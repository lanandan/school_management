package com.gr_infotech.localdatabase;
import java.util.ArrayList;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class SavingMessagesDataBaseAdapter {
	static DatabaseAdapter dba;
	static SQLiteDatabase db;
	public static String TNAME="allmsgs";
	static ContentValues cv;
    public static String GOTMSG="message"; 
	static String sql="CREATE TABLE IF NOT EXISTS "+TNAME+" ( message text )";
   
	public static void onCreate(SQLiteDatabase db) {
	      Log.i("test","its calling in oncreate of messagedatabaseadapter method");
		  db.execSQL(sql);       
	}

	public static long insertingData(SavingMessagesBean bean,Context context)
	{
		dba=new DatabaseAdapter(context);
		Log.i("test","its inserting data in SavingeMessageDataBaseAdapter");
		db=dba.getWritableDatabase();
        cv=new ContentValues();
        cv.put(GOTMSG,bean.getMessages());       
        long returnvalues=db.insertOrThrow(TNAME, null, cv);
		Log.i("test","the values are inserted in savingmessagedbadapter"+returnvalues);
		return returnvalues;
		
	}
	public static ArrayList<SavingMessagesBean> getAllData(Context context)
	{
		Log.i("test","calling cursor get all data in android");
		String query1="SELECT * FROM "+TNAME;
		ArrayList<SavingMessagesBean> list=new ArrayList<SavingMessagesBean>();
		dba=new DatabaseAdapter(context);
		db=dba.getReadableDatabase();
	    Cursor c=db.rawQuery(query1, null);
	    if(c.moveToFirst())
		{
	    do{
	    	SavingMessagesBean smb=new SavingMessagesBean();
	    	smb.setMessages(c.getString(0));
	    	list.add(smb);
	    }while(c.moveToNext());
	    db.close();
	    return list;   
	}
	return null;	
	    
	    	}
}
