package com.gr_infotech.localdatabase;

public class SavingMessagesBean {
String messages;
String position;
SavingMessagesBean()
{
	
}
SavingMessagesBean(String message)
{
	this.messages=message;
}
SavingMessagesBean(String message,String position)
{
	this.messages=message;
	this.position=position;
}
public String isPosition() {
	return position;
}
public void setPosition(String position) {
	this.position = position;
}
public String getMessages() {
	return messages;
}

public void setMessages(String messages) {
	this.messages = messages;
            
 }

}
