package com.gr_infotech.localdatabase;

import android.util.Log;

public class SavingUserBean  {
String name;
String sender_imei;
String sender_regid;
String message;


public SavingUserBean(String name,String sender_imei,String sender_regid,String message)
{
	this.name=name;
	this.sender_imei=sender_imei;
	this.sender_regid=sender_regid;
	this.message=message;
	Log.i("test","the values in saving userbean are"+name+" "+sender_imei+" "+sender_regid+" "+message);
}
public SavingUserBean()
{
	
}


public String getName() {

	return name;
}
public void setName(String name) {
	this.name = name;
	Log.i("test","the values in saving userbean name are"+this.name);

}
public String getSender_imei() {
	return sender_imei;
}
public void setSender_imei(String sender_imei) {
	this.sender_imei = sender_imei;
	Log.i("test","the values in saving userbean senderimei are"+this.sender_imei);
	
}
public String getSender_regid() {
	return sender_regid;
}
public void setSender_regid(String sender_regid) {
	this.sender_regid = sender_regid;
	Log.i("test","the values in saving userbean sending_regid are"+this.sender_regid);

}
public String getMessage() {
	return message;
}
public void setMessage(String message) {
	this.message = message;
	Log.i("test","the values in saving userbean message are"+this.message);

}

}
