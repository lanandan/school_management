package com.gr_infotech.localdatabase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseAdapter extends SQLiteOpenHelper {
	static String DATABASENAME="userdatamsging.db";
	static String sql; 
	static String TNAME;
	Context context;
	SQLiteDatabase db;
	static int VERSION=1;
	public DatabaseAdapter(Context context) {
		super(context, DATABASENAME, null, VERSION);
	 Log.e("Context is",""+context);
		
	}

	
	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		try
		{
			Log.e("test"+getClass().getName(), " database executed - ");
			MessageDataBaseAdapter.onCreate(db);
			SavingMessagesDataBaseAdapter.onCreate(db);
			SavingSentMessagesDatabaseAdapter.onCreate(db);
		}
		catch(Exception e)
		{
			Log.e("the database is not created",""+e);
			e.printStackTrace();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		try
		{
		
		db.execSQL("DROP TABLE IF EXISTS "+TNAME);
	    onCreate(db);
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			Log.e("the table is not upgraded",""+e);
		}
	}

}
