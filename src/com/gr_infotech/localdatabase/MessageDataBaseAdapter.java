package com.gr_infotech.localdatabase;
import java.util.ArrayList;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class MessageDataBaseAdapter {
	static DatabaseAdapter dba;
	static SQLiteDatabase db;
	public static String TNAME="msgsofuser";
	static ContentValues cv;
    public static String NAME="name";
    public static String ID="id";
	public static String REGID="regid";
	public static String SENDER_IMEI="sender_imei";
	public static String SENDER_MSG="sender_msg";
    static String sql="CREATE TABLE IF NOT EXISTS "+TNAME+" ( name text ,regid text ,sender_imei text ,sender_msg text )";
	
    public static void onCreate(SQLiteDatabase db) {
	      Log.i("test","its calling in oncreate of messagedatabaseadapter method");
		db.execSQL(sql);
	       
	}

	public static long insertData(SavingUserBean bean,Context context)
	{
		dba=new DatabaseAdapter(context);
		Log.i("test","its inserting data in messagedatabaseadapter");
		db=dba.getWritableDatabase();
        cv=new ContentValues();
        cv.put(NAME, bean.getName());
        cv.put(REGID, bean.getSender_regid());
        cv.put(SENDER_IMEI, bean.getSender_imei());
        cv.put(SENDER_MSG, bean.getMessage());
		long returnvalues=db.insertOrThrow(TNAME, null, cv);
		Log.i("test","the values are inserted in service"+returnvalues);
		return returnvalues;
		
	}
	public static ArrayList<SavingUserBean> getAllData(Context context)
	{
		Log.i("test","calling cursor get all data in android");
		String query1="SELECT * FROM "+TNAME;
		ArrayList<SavingUserBean> list=new ArrayList<SavingUserBean>();
		dba=new DatabaseAdapter(context);
		db=dba.getReadableDatabase();
	    Cursor c=db.rawQuery(query1, null);
	    if(c.moveToFirst())
		{
	    do{
	    	SavingUserBean sub=new SavingUserBean();
	    	sub.setName(c.getString(0));
	    	sub.setMessage(c.getString(3));
	    	list.add(sub);
	    }while(c.moveToNext());
	    db.close();
	    return list;   
	}
	return null;	
	    
	    	}
}
