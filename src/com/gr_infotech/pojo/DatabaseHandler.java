package com.gr_infotech.pojo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

public class DatabaseHandler extends SQLiteOpenHelper 
{
	private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "chat";
    private static final String Chat_Table = "chat_table";
	SimpleDateFormat day,month,year;
	String strday,strmonth,stryear;
	Calendar cal;
	String date;

	static class notify 
	{
		static String SEND_OR_RECIEVE = "sendorrecieve";
		static String MESSAGE = "message";
		static String type="type";
		static String classes="class";
		static String section="section";
		static String subject="subject";
		static String date="date";
	}

    
    
    
    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
	
	
	
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		
		String CREATE_TABLE_CHAT = "CREATE TABLE " + Chat_Table + "("+ notify.SEND_OR_RECIEVE + " TEXT," + notify.MESSAGE + " TEXT," + notify.type + " TEXT," + notify.classes + " TEXT," + notify.section + " TEXT,"+ notify.subject + " TEXT,"+ notify.date + " TEXT"+")";
		db.execSQL(CREATE_TABLE_CHAT);

	}
		
	

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	
	
		db.execSQL("DROP TABLE IF EXISTS " + Chat_Table);
		onCreate(db);

		
	}
	
	
public void addData(Chat_setter_getter val) {
		
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(notify.SEND_OR_RECIEVE, val.getsendorreceive());
		values.put(notify.MESSAGE, val.getcontent());
		values.put(notify.type, val.gettype());
		values.put(notify.classes,val.getclass());
		values.put(notify.section,val.getsection());
		values.put(notify.subject,val.getsubject());
		
		
		day = new SimpleDateFormat("dd", Locale.getDefault());
		month = new SimpleDateFormat("MM", Locale.getDefault());
		year = new SimpleDateFormat("yyyy", Locale.getDefault());
		strday = day.format(cal.getTime());
		strmonth = month.format(cal.getTime());
		stryear = year.format(cal.getTime());
		date=strday+"-"+strmonth+"-"+stryear;
        values.put(notify.date,date);
		
        
        Log.e("Data base Message :","Content :"+val.getcontent()+"Type:,"+val.gettype()+"classes,"+val.getclass()+"section,"+val.getsection());
		db.insert(Chat_Table, null, values);
		db.close();
		
	}

	
	
	
	 public List<Chat_setter_getter> getAllchat(String type,String class_name,String section,String subject) {
		    List<Chat_setter_getter> chatList = new ArrayList<Chat_setter_getter>();
	        String selectQuery = "SELECT * FROM "+Chat_Table+" where "+notify.type+"=\'"+type+"\'"+ "and "+notify.classes+"=\'"+class_name+"\'"+ "and "+notify.section+"=\'"+section+"\'"+ "and "+notify.subject+"=\'"+subject+"\'";
	        SQLiteDatabase db = this.getWritableDatabase();
	        Cursor cursor = db.rawQuery(selectQuery, null);
	       
	        // looping through all rows and adding to list
	        if (cursor.moveToFirst()) {
	            do {
	            	 Chat_setter_getter chat = new Chat_setter_getter();
	                chat.setcontent(cursor.getString(1));
	                chat.setsendorreceive(cursor.getString(0));	   
	                chat.settype(cursor.getString(2));
	                chat.setclass(cursor.getString(3));
	                chat.setsection(cursor.getString(4));
	                chat.setsubject(cursor.getString(5));
	                // Adding contact to list
	                chatList.add(chat);
	            } while (cursor.moveToNext());
	        }	 
	        // return contact list
	      
	        db.close();
	        return chatList;
	    }
	

}
