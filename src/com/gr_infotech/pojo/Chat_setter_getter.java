package com.gr_infotech.pojo;

public class Chat_setter_getter {
    
    //private variables
    String content;
    String sendorreceive;
    String type;
    String classes;
    String section;
    String subject;
     
    // Empty constructor
    public Chat_setter_getter(){
         
    }
    // constructor
    public Chat_setter_getter(String sendorreceive,String content,String type,String classes,String section,String subject)
    {
        this.sendorreceive=sendorreceive;
    	this.content=content;
    	this.type=type;
    	this.classes=classes;
    	this.section=section;
    	this.subject=subject;
    }
     
    public void setcontent(String content) 
	{
    	this.content=content;    	
    }
    
    
    public void setsendorreceive(String sendorreceive) 
	{
    	this.sendorreceive=sendorreceive;    	
    }
    
    
    public void settype(String type) 
	{
    	this.type=type;    	
    }
    
    
    public void setclass(String classes) 
    {
    	this.classes=classes;	
	}
    
    public void setsection(String section)
    {
    	this.section=section;
    }
    
    public void setsubject(String subject)
    {
    	this.subject=subject;
    }
    
    
    public String getsendorreceive()
    {
    	return sendorreceive;
    }
    
    
    public String getcontent()
    {
    	return content;
    }   
    
    public String gettype()
    {
    	return type;
    }
    
    public String getsubject()
    {
    	return subject;    	
    }
 
    public String getclass()
    {
    	return classes;
    }
    
    public String getsection()
    {
    	return section;
    }
    
    
    
    
}