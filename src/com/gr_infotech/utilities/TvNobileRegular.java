package com.gr_infotech.utilities;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TvNobileRegular extends TextView 
{

	 public TvNobileRegular (Context context, AttributeSet attrs, int defStyle) 
	    {
	        super(context, attrs, defStyle);
	        init();
	    }

	    public TvNobileRegular (Context context, AttributeSet attrs) 
	    {
	        super(context, attrs);
	        init();
	    }

	    public TvNobileRegular (Context context) 
	    {
	        super(context);
	        init();
	    }

	    private void init() 
	    {
	        if (!isInEditMode()) 
	        {
	            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Nobile-Regular.ttf");
	            setTypeface(tf);
	        }
	    }
	    
}
