package com.gr_infotech.utilities;

import java.util.ArrayList;
import java.util.List;



import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.school_management.LoginActivity;
import com.example.school_management.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class Logout_Common 
{
	private static Context contextActivity;	
//	private static final String JSON_LogoutUrl = Parser_Network.wsUrl + Parser_Network.wsLogout;		
	
	public static void setLogout(Activity activity)
	{
		contextActivity = activity;		
		exitLogout();
		showToast(activity, "Logged Out");
		
//		 new AtaskCommonLogout().execute(JSON_LogoutUrl);		 
	}
	
	private static void showMessage(String msg)
	{
		Toast.makeText(contextActivity, msg, Toast.LENGTH_SHORT).show();
	}
	
	static class AtaskCommonLogout extends AsyncTask<String, String, JSONObject>
	{
		ProgressDialog pDialog;

		@Override
		protected void onPreExecute() 
		{
			super.onPreExecute();

			pDialog = new ProgressDialog(contextActivity);
			pDialog.setMessage("Logging out...");
			pDialog.setCancelable(false);
			pDialog.show();
		}
		
		@Override
		protected JSONObject doInBackground(String... params) 
		{	
			JSONObject job =new JSONObject();
			try 
			{
				Log.d("URL : ", params[0].toString());
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
			    nameValuePairs.add(new BasicNameValuePair("parentNumber", Network_checking.sUserName));
				job = Network_checking.doProcessPost(params[0], nameValuePairs);
				Log.d("Json Object : ", job.toString());
			} 
			catch (Exception e) 
			{
				pDialog.dismiss();
				Log.d("Do in Background : ", e.toString() + "\n Do in Background");
			}
			return job;
		}
		
		@Override
		protected void onPostExecute(JSONObject result)
		{			
			try 
			{
				JSONArray ja=result.getJSONArray("logoutParent");				
				int len = ja.length();
								
				if (len > 0)
				{
					JSONObject jo = ja.getJSONObject(0);				
					
					if(jo.getString("Status").equals("Updated"))
					{					
//						((Activity)contextActivity).finish();
						
						Intent in = new Intent(contextActivity, LoginActivity.class);
						contextActivity.startActivity(in);
						
						showMessage("Logged out : " + jo.getString("Status"));
					}
					else
						showMessage("Logout Failed");
					
				}
				
			} 			
			catch (JSONException e) 
			{
				showMessage("Failure");								
			}
			
			if (pDialog != null && pDialog.isShowing()) 
			{
				pDialog.dismiss();
			}
				
		}
	}
	
	public static void exitLogout()
	{
		Intent in = new Intent(contextActivity,LoginActivity.class);
		contextActivity.startActivity(in);
		((Activity) contextActivity).overridePendingTransition(R.anim.left_in, R.anim.right_out);
	}
	
	public static void showToast(Activity activity, String msg)
	{
		Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
	}

}
