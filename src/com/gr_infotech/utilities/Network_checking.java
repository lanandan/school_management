package com.gr_infotech.utilities;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class Network_checking 
{
		
	public static String wsValidateLogin = "validateLogin";	
	public static String wsLogout = "logoutParent";
	public static String wsForgotPassword = "ForgotPassword";
	public static String wsChangePassword = "ChangePassword";
	public static String sUserName = null;
	public static String reqMobileNo = null;
	public static String reqTicketID = null;
	public static String sUID = null;
	private static StringBuilder sb;

	public static boolean isNetworkAvailable(Activity activity) 
	{
		ConnectivityManager connectivity = (ConnectivityManager) activity
				.getSystemService(Context.CONNECTIVITY_SERVICE);
			
			if (connectivity == null) 
			{
				return false;
			} 
			else 
			{
				NetworkInfo[] info = connectivity.getAllNetworkInfo();
				if (info != null) 
				{
					for (int i = 0; i < info.length; i++) 
					{
						if (info[i].getState() == NetworkInfo.State.CONNECTED)
						{
							return true;
						}
					}
				}
			}
			return false;
		}
	
	public static JSONObject doProcessPost(String url,  List<NameValuePair> nameValuePairs) throws Exception
	{
		HttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(url);
		
		try 
		{
		    httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));		        
			HttpResponse httpResponse = httpClient.execute(httpPost);
			
			Log.d("Http Response : ", ""+httpResponse);
			StatusLine sl = httpResponse.getStatusLine();
			sb = new StringBuilder();
			int stcode = sl.getStatusCode();
			if (stcode == 200) 
			{
				Log.d("Do Post Method Process : ", "Page Found");
				HttpEntity httpEntity = httpResponse.getEntity();
				InputStream is = httpEntity.getContent();
				BufferedReader br = new BufferedReader(
						new InputStreamReader(is));
			
				String line;
				while ((line = br.readLine()) != null) 
				{
					sb.append(line);
				}
			} 
			else 
			{
				Log.d("Do Post Method Process : ", "Page Not Found");
			}
		} 
		catch (Exception e)
		{
			Log.d("Do Post Method Process : ", e.toString() + "Do Process");
		}
		
		
		Log.d("Do Post Method Process : ", "SUCCESS : "+sb.toString());
		
		JSONObject json = new JSONObject(sb.toString());
		Log.d("Parser_Network : ", json.toString());
		return json;		
	}
}
