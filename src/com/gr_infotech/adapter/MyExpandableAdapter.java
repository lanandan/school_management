package com.gr_infotech.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List; 

import com.example.school_management.R;

import android.R.integer;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
public class MyExpandableAdapter extends BaseExpandableListAdapter 
{
    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, List<String>> _listDataChild;
  
    
    Typeface nobileMedium, nobileRegular, nobileBoldItalic, nobileMediumItalic;
    
    public MyExpandableAdapter(Context context, List<String> listDataHeader,HashMap<String, List<String>> listChildData)
    {
    	nobileMedium = Typeface.createFromAsset(context.getAssets(), "fonts/Nobile-Medium.ttf");
		nobileRegular = Typeface.createFromAsset(context.getAssets(), "fonts/Nobile-Regular.ttf");
		nobileBoldItalic = Typeface.createFromAsset(context.getAssets(), "fonts/Nobile-BoldItalic.ttf");
		nobileMediumItalic = Typeface.createFromAsset(context.getAssets(), "fonts/Nobile-MediumItalic.ttf");
    	this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;  
    }
 
    @Override
    public Object getChild(int groupPosition, int childPosititon) 
    {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }
 
    @Override
    public long getChildId(int groupPosition, int childPosition) 
    {
        return childPosition;
    }
 
    @Override
    public View getChildView(int groupPosition, final int childPosition,boolean isLastChild, View convertView, ViewGroup parent) 
    {
         final String childText = (String) getChild(groupPosition, childPosition);
 
         Log.e("Data for attendance",childText);
         
        if (convertView == null) 
        {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.row, null);
        }
 
        TextView txtListChild = (TextView) convertView.findViewById(R.id.lblListItem);
        TextView text = (TextView) convertView.findViewById(R.id.text);
        
                      
        String stringtosplit = childText;
        String part1,part2;
        
        if(stringtosplit.contains(":"))
        {
        	String[] parts = stringtosplit.split(":");
            part1 = parts[0]; // 004
            part2 = parts[1]; // 034556
            txtListChild.setTypeface(nobileRegular);       
            txtListChild.setHint(part1);
            text.setGravity(Gravity.RIGHT);
            text.setText(part2);
            
        }
        else
        {
        	String[] parts = stringtosplit.split("-");
            part1 = parts[0]; // 004
            part2 = parts[1]; // 034556
            txtListChild.setTypeface(nobileRegular);       
            txtListChild.setHint(part1);
            text.setGravity(Gravity.LEFT);
            text.setText(part2);        	
        }
   
        return convertView;
    }
 
    @Override
    public int getChildrenCount(int groupPosition) 
    {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).size();
    }
 
    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }
 
    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }
 
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }
 
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
            View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.group, null);
        }
 
        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);
 
        return convertView;
    }
 
    @Override
    public boolean hasStableIds() {
        return false;
    }
 
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}