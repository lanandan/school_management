package com.gr_infotech.adapter;
import java.util.ArrayList;
import android.content.Context;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.school_management.R;
import com.gr_infotech.fragments.Attendance_Fragment;

class ViewHolder
{
	TextView tv1;
	TextView tv2;
	CheckBox cb1;
}

public class CustomAttendanceListAdpater extends BaseAdapter
{

	int reso;
	ArrayList<TextView> name,reg;
	ArrayList<CheckBox> cb;
	Context cxt;
	public static boolean checkingstatus[];
	
		
	public CustomAttendanceListAdpater(Context context, int res, ArrayList<TextView> attnd_stud_no, ArrayList<TextView> attnd_stud_name, ArrayList<CheckBox> cb){
		reso = res;
		name=attnd_stud_name;
		reg=attnd_stud_no;
		this.cb = cb;
	    System.out.println(cb.size());
		checkingstatus=new boolean[attnd_stud_name.size()];
		int i=0;
		while(i<attnd_stud_name.size())
		{
			
			checkingstatus[i]=true;
		i++;
		}
		cxt = context;		
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return name.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		
		final ViewHolder holder;
		
		if(convertView==null)
		{
		    LayoutInflater inflater = (LayoutInflater) cxt.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(reso,null,false);
			holder = new ViewHolder();			
			holder.tv1 = (TextView)convertView.findViewById(R.id.tv_1);
			holder.tv2 = (TextView)convertView.findViewById(R.id.tv_2);
			holder.cb1 = (CheckBox)convertView.findViewById(R.id.cb_1);
			convertView.setTag(holder);
		}
		else
		{
			holder=(ViewHolder)convertView.getTag();
		}
		holder.tv1.setText(name.get(position).getText());
		holder.tv1.setFilters( new InputFilter[] { new InputFilter.LengthFilter(15) } );
		holder.tv2.setText(reg.get(position).getText());
		holder.tv2.setPadding(0, 0, 15, 0);
		holder.cb1.setButtonDrawable(R.drawable.checboxcustom);
		holder.cb1.setChecked(true);		
		holder.cb1.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                	@Override
                	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                		holder.cb1.setChecked(isChecked);
                		int positions=position;                		
                		checkingstatus[position]=isChecked;
                		System.out.print(checkingstatus[position]+","+position);                		
                		if(checkingstatus[position]==true){
                		Attendance_Fragment.total_no++;
                		}else{
                			Attendance_Fragment.total_no--;
                		}
                		
                		Attendance_Fragment.totalNumSelectedUpdate();
                    }
                });
		
		
		
		return convertView;
	}

}
