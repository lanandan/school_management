package com.gr_infotech.adapter;

import com.example.school_management.R;

import android.app.Activity;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

public class CustomList extends ArrayAdapter<String>
{
private final Activity context;
private final String[] web;
private final Integer[] imageId;

public CustomList(Activity context,String[] web, Integer[] imageId) 
{
super(context, R.layout.listsingle, web);
this.context = context;
this.web = web;
this.imageId = imageId;
}

@Override
public View getView(int position, View view, ViewGroup parent) 
{
LayoutInflater inflater = context.getLayoutInflater();
View rowView= inflater.inflate(R.layout.listsingle, null, true);
TableRow tr=(TableRow)rowView.findViewById(R.id.tableRow1);
TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);
ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
txtTitle.setText(web[position]);
imageView.setImageResource(imageId[position]);
if(txtTitle.getText().equals("Parent")||txtTitle.getText().equals("Teacher"))
{
	txtTitle.setBackgroundColor(Color.rgb(255,97,3));
	imageView.setBackgroundColor(Color.rgb(255,97,3));
	tr.setBackgroundColor(Color.rgb(255,97,3));
	tr.setGravity(Gravity.CENTER);
}

if(txtTitle.getText().equals(""))
{
	
	tr.setGravity(Gravity.LEFT);
}

return rowView;
}
}