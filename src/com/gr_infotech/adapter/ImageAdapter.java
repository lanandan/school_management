package com.gr_infotech.adapter;

import com.example.school_management.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ImageAdapter extends BaseAdapter{
    private Context mContext;
    private final String[] web;
    private final int[] Imageid;
      public ImageAdapter(Context c,String[] web,int[] Imageid ) 
      {
          mContext = c;
          this.Imageid = Imageid;
          this.web = web;
      }
    @Override
    public int getCount() {
      // TODO Auto-generated method stub
      return web.length;
    }
    @Override
    public Object getItem(int position) {
      // TODO Auto-generated method stub
      return null;
    }
    @Override
    public long getItemId(int position) {
      // TODO Auto-generated method stub
      return 0;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      // TODO Auto-generated method stub
      View grid;
      LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
          if (convertView == null) {
            grid = new View(mContext);
            grid = inflater.inflate(R.layout.gridsingle, null);
            TextView textView = (TextView) grid.findViewById(R.id.grid_text);
            ImageView imageView = (ImageView)grid.findViewById(R.id.grid_image);
            textView.setText(web[position]);
            imageView.setImageResource(Imageid[position]);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
           // imageView.setLayoutParams(new GridView.LayoutParams(100,100));            
          } else {
            grid = (View) convertView;
          }
      return grid;
    }
}
