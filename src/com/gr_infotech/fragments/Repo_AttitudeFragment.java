package com.gr_infotech.fragments;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.example.school_management.LoginActivity;
import com.example.school_management.R;
import com.gr_infotech.adapter.MyExpandableAdapter;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

public class Repo_AttitudeFragment extends Fragment
{
	Calendar cal;
	String date;	
	ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    HttpClient httpclient;
	HttpPost httppost;
	ArrayList<NameValuePair> nameValuePairs;
	InputStream is;
	Button addstudent;
	SharedPreferences shared,shared_uid,shared_gallery_name;
	String uid,subtype,type,phno;
	JSONObject parent_email;		
	String class_name,section;
	String name,classes,sections,no;
	private static final String SH_UIDS = "Tripuid";	
	private static final String UID = "UniqueID";
	private static final String TYPE = "Type";
	private static final String PHNO = "phone";
	TextView names,nos,txtdate,txttime,txtstudname;	
	SimpleDateFormat day,month,year;
	String strday,strmonth,stryear;
	String Month[]={"Jan","Feb","March","April","May","June","July","Aug","Sep","Oct","Nov","Dec"};	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) 
	{
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);       
		View view = inflater.inflate(R.layout.report_attitude,container, false);         	        
        //to get list view
		expListView = (ExpandableListView)view.findViewById(R.id.report_attitude_expandable);
		// To get Uid
        shared = getActivity().getSharedPreferences(SH_UIDS,Context.MODE_PRIVATE);
		uid=(shared.getString(UID,null));
		type=(shared.getString(TYPE,null));
		phno=(shared.getString(PHNO,null));
		// to get selected Student name and class and section
		shared_gallery_name=getActivity().getSharedPreferences("gallery_name",Context.MODE_PRIVATE);
		name=shared_gallery_name.getString("name", null);
		classes=shared_gallery_name.getString("class", null);
		sections=shared_gallery_name.getString("sec", null);
		no=shared_gallery_name.getString("no",null);
				
		
		cal= Calendar.getInstance(TimeZone.getDefault());
		
		
		 	txtdate=(TextView)view.findViewById(R.id.txtdate);
	        txttime=(TextView)view.findViewById(R.id.txttime);
	        txtstudname=(TextView)view.findViewById(R.id.stud_name);
	        
	        
	         Date time = new Date();
	         String strDateFormat = "hh:mm:ss a";
	         SimpleDateFormat sdf = new SimpleDateFormat(strDateFormat);        
	         txttime.setText(sdf.format(time));         
	        
	        day = new SimpleDateFormat("dd", Locale.getDefault());
			month = new SimpleDateFormat("MM", Locale.getDefault());
			year = new SimpleDateFormat("yyyy", Locale.getDefault());
			strday = day.format(cal.getTime());
			strmonth = month.format(cal.getTime());
			stryear = year.format(cal.getTime());
			date=strday+"-"+strmonth+"-"+stryear;
	        
	        txtdate.setText(date);
	        txtstudname.setText(name);	
	
		
		//Data to fetch details from database
				String line;
		    	String result="";
		    	String email="";
		    	String email_content_extendible_list_view="";
		    	String json_lbl_name="",json_lbl_email="";        	
		    	String jsonchild_report_month="MONTH";
		    	String jsonchild_report_year="YEAR";
		    	String jsonchild_report_subjectname="SUBJECT_NAME";		    	
		    	String jsonchild_report_obedience="OBEDIENCE";
		    	String jsonchild_report_discipline="DISCIPLINE";
		    	String jsonchild_report_punctuality="PUNCTUALITY";
		    	String jsonchild_report_personality="PERSONALITY";
		    	String jsonchild_report_total="TOTAL";
		    	String jsonchild_report_month_label="LMONTH";
		    	String jsonchild_report_year_label="LYEAR";
		    	String jsonchild_report_subject_label="LSUBJECT_NAME";		    	
		    	String jsonchild_report_obedience_label="LOBEDIENCE";
		    	String jsonchild_report_discipline_label="LDISCIPLINE";
		    	String jsonchild_report_punctuality_label="LPUNCTUALITY";
		    	String jsonchild_report_personality_label="LPERSONALITY";
		    	String jsonchild_report_total_label="LTOTAL";
		    	if(type.equals("Parent"))
				{
					listDataHeader = new ArrayList<String>();
			        listDataChild = new HashMap<String, List<String>>();
			        httpclient = new DefaultHttpClient();
			    	httppost = new HttpPost("http://"+LoginActivity.URL+"/schoolmanagement/designs/webservice/attitude_report.php");
			    	//data to be send to web server to fetch data about the particulr class
			    	nameValuePairs= new ArrayList<NameValuePair>();
					nameValuePairs.add(new BasicNameValuePair("no",no));
					nameValuePairs.add(new BasicNameValuePair("classes",classes));
					nameValuePairs.add(new BasicNameValuePair("sections",sections));
					Log.e("Name Value Pairs",nameValuePairs+"");
			     	// details to be displayed in Extensible list view	
		        try
				{
					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
					HttpResponse response = httpclient.execute(httppost);
					HttpEntity entity = response.getEntity();					
					is = entity.getContent();
					BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
				    StringBuilder sb = new StringBuilder();
				    while ((line = reader.readLine()) != null)
		            {
		                sb.append(line + "\n");
		            }
		            is.close();
		            result = sb.toString();			
		   
		            JSONObject json_data = new JSONObject(result);
		    	    Log.e("Json Data", json_data+"");	     
		            
		    	    JSONObject data = json_data.getJSONObject("DATA"); // get data object
		            String code = data.getString("CODE"); // get the name from data.
		            String message =data.getString("MESSAGE");
		            if(code.equals("200"))
		            {		            
		            JSONArray json_month=data.getJSONArray("MONTH");		            
		            JSONArray json_year=data.getJSONArray("YEAR");		            
		            JSONArray json_subject_name=data.getJSONArray("SUBJECT_NAME");
		            JSONArray json_obedience=data.getJSONArray("OBEDIENCE");
		            JSONArray json_discipline=data.getJSONArray("DISCIPLINE");
		            JSONArray json_punctuality=data.getJSONArray("PUNCTUALITY");
		            JSONArray json_personality=data.getJSONArray("PERSONALITY");
		            JSONArray json_total=data.getJSONArray("TOTAL");
		            
		            int i=0;
		            for(i=0;i<json_month.length();i++)
		            {
		            	
listDataHeader.add(json_subject_name.getString(i)+" in "+Month[Integer.parseInt(json_month.getString(i))-1]+" , "+json_year.getString(i));		            		
		            	
		            		List<String> reports = new ArrayList<String>(); 
		            		reports.clear();		            		
			            	reports.add("Obedience:"+json_obedience.getString(i));            	
			            	reports.add("Discipline:"+json_discipline.getString(i));
			            	reports.add("punctuality:"+json_punctuality.getString(i));
			            	reports.add("Personality:"+json_personality.getString(i));
			            	reports.add("Total:"+json_total.getString(i));
			            listDataChild.put(listDataHeader.get(i),reports);
			        }      
		            }		            
		            else
		            {
		            	Log.e("error","error in data Transfer");
		            	
		            }
		        }
				catch(Exception e)
				{
					Log.e("Exception",e+"");
				}
		        
		        listAdapter = new MyExpandableAdapter(getActivity().getApplicationContext(), listDataHeader, listDataChild);
		        // setting list adapter
		        expListView.setAdapter(listAdapter);
				}
				else
				{
					Toast.makeText(getActivity().getApplicationContext(),"Sorry this is Parent View",Toast.LENGTH_LONG).show();
				}
		
		
		// TODO Auto-generated method stub
		return view;
		
	}

}
