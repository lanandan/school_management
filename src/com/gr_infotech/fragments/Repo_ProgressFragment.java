package com.gr_infotech.fragments;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.achartengine.ChartFactory;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.example.school_management.LoginActivity;
import com.example.school_management.R;
import com.gr_infotech.adapter.MyExpandableAdapter;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Repo_ProgressFragment extends Fragment implements OnClickListener 
{
	LayoutInflater factory;
	View textEntryView,mapaddressview,sltmapaddressview;
	Calendar cal;
	ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    HttpClient httpclient;
	HttpPost httppost;
	ArrayList<NameValuePair> nameValuePairs;
	InputStream is;
	Button addstudent;
	SharedPreferences shared,shared_uid,shared_gallery_name;
	String uid,subtype,type,phno;
	JSONObject parent_email;		
	String class_name,section;
	String name,classes,sections,no;
	private static final String SH_UIDS = "Tripuid";	
	private static final String UID = "UniqueID";
	private static final String TYPE = "Type";
	private static final String PHNO = "phone";
	TextView names,nos,txtdate,txttime,txtstudname;
	SimpleDateFormat day,month,year,months;
	String strday,strmonth,stryear,date,strchartmonth,strchartyear;
	String Month[]={"Jan","Feb","March","April","May","June","July","Aug","Sep","Oct","Nov","Dec"};
	Button chart;
	int strtday,strtmonth,strtyear;
	final Calendar c = Calendar.getInstance();
	TextView txtmonth,txtyear;
	String popmonth,popyear;
	int x[],marks[];
	String subject_name[];
	String monthforprogreport;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) 
	{
		// TODO Auto-generated method stub
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);       
		View view = inflater.inflate(R.layout.repo_progress,container, false);
		
		
		
				//to get list view
				expListView = (ExpandableListView)view.findViewById(R.id.report_progress_expandable);
				chart=(Button)view.findViewById(R.id.chart);
				chart.setOnClickListener(this);
				// To get Uid
		        shared = getActivity().getSharedPreferences(SH_UIDS,Context.MODE_PRIVATE);
				uid=(shared.getString(UID,null));
				type=(shared.getString(TYPE,null));
				phno=(shared.getString(PHNO,null));
				// to get selected Student name and class and section
				shared_gallery_name=getActivity().getSharedPreferences("gallery_name",Context.MODE_PRIVATE);
				name=shared_gallery_name.getString("name", null);
				classes=shared_gallery_name.getString("class", null);
				sections=shared_gallery_name.getString("sec", null);
				no=shared_gallery_name.getString("no",null);
				
				
				
				
				cal= Calendar.getInstance(TimeZone.getDefault());
				
				
				 txtdate=(TextView)view.findViewById(R.id.txtdate);
			        txttime=(TextView)view.findViewById(R.id.txttime);
			        txtstudname=(TextView)view.findViewById(R.id.stud_name);
			        
			        
			         Date time = new Date();
			         String strDateFormat = "hh:mm:ss a";
			         SimpleDateFormat sdf = new SimpleDateFormat(strDateFormat);        
			         txttime.setText(sdf.format(time));         
			        
			        day = new SimpleDateFormat("dd", Locale.getDefault());
					month = new SimpleDateFormat("MM", Locale.getDefault());
					year = new SimpleDateFormat("yyyy", Locale.getDefault());
					months = new SimpleDateFormat("M", Locale.getDefault());
					
					strday = day.format(cal.getTime());
					strmonth = month.format(cal.getTime());
					stryear = year.format(cal.getTime());
					date=strday+"-"+strmonth+"-"+stryear;
			        
			        txtdate.setText(date);
			        txtstudname.setText(name);	
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				Log.e("Checking",no+","+classes+","+sections);
				
				//Data to fetch details from database
				String line;
		    	String result="";
		    	String email="";
		    	String email_content_extendible_list_view="";
		    	String json_lbl_name="",json_lbl_email="";        	
		    	String jsonchild_report_month="MONTH";
		    	String jsonchild_report_year="YEAR";
		    	String jsonchild_report_subjectname="SUBJECT_NAME";	
		    	String jsonchild_report_outofmarks="OUT_OF_MARKS";
		    	String jsonchild_report_marks="MARKS";
		    	
		    	String jsonchild_report_month_label="LMONTH";
		    	String jsonchild_report_year_label="LYEAR";
		    	String jsonchild_report_subject_label="LSUBJECT_NAME";		    	
		    	String jsonchild_report_out_of_marks_label="LOUT_OF_MARKS";
		    	String jsonchild_report_marks_label="LMARKS";
		    	
		    	
		 
		    	if(type.equals("Parent"))
				{
					listDataHeader = new ArrayList<String>();
			        listDataChild = new HashMap<String, List<String>>();
			        httpclient = new DefaultHttpClient();
			    	httppost = new HttpPost("http://"+LoginActivity.URL+"/schoolmanagement/designs/webservice/progress_report.php");
			    	//data to be send to web server to fetch data about the particulr class
			    	nameValuePairs= new ArrayList<NameValuePair>();
					nameValuePairs.add(new BasicNameValuePair("no",no));
					nameValuePairs.add(new BasicNameValuePair("classes",classes));
					nameValuePairs.add(new BasicNameValuePair("sections",sections));
			     	// details to be displayed in Extensible list view	
		        try
				{
					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
					HttpResponse response = httpclient.execute(httppost);
					HttpEntity entity = response.getEntity();					
					is = entity.getContent();
					BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
				    StringBuilder sb = new StringBuilder();
				    while ((line = reader.readLine()) != null)
		            {
		                sb.append(line + "\n");
		            }
		            is.close();
		            result = sb.toString();			
		   
		            JSONObject json_data = new JSONObject(result);
		    	    Log.e("Json Data", json_data+"");	     
		            
		    	    JSONObject data = json_data.getJSONObject("DATA"); // get data object
		            String code = data.getString("CODE"); // get the name from data.
		            String message =data.getString("MESSAGE");
		            if(code.equals("200"))
		            {		            
		            	
		            	
		            JSONArray json_month=data.getJSONArray("MONTH");
		            JSONArray json_year=data.getJSONArray("YEAR");
		            JSONArray json_subject_name=data.getJSONArray("SUBJECT_NAME");
		            JSONArray json_out_of_marks=data.getJSONArray("OUT_OF_MARKS");
		            JSONArray json_marks=data.getJSONArray("MARKS");
		            int i=0;
		            for(i=0;i<json_month.length();i++)
		            {
listDataHeader.add(json_subject_name.getString(i)+" in "+Month[Integer.parseInt(json_month.getString(i))-1]+" , "+json_year.getString(i));
		            		List<String> reports = new ArrayList<String>(); 
		            		reports.clear();         		
			            	//reports.add("Marks out of:"+json_out_of_marks.getString(jsonchild_report_out_of_marks_label+i));
			            	reports.add("Marks:"+json_marks.getString(i)+"  Out of "+json_out_of_marks.getString(i));
			            	listDataChild.put(listDataHeader.get(i),reports);
			        }      
		            }		            
		            else
		            {
		            	Log.e("error","error in data Transfer");
		            	
		            }
		        }
				catch(Exception e)
				{
					Log.e("Exception",e+"");
				}
		        
		        listAdapter = new MyExpandableAdapter(getActivity().getApplicationContext(), listDataHeader, listDataChild);
		        // setting list adapter
		        expListView.setAdapter(listAdapter);
				}
				else
				{
					Toast.makeText(getActivity().getApplicationContext(),"Sorry this is Parent View",Toast.LENGTH_LONG).show();
				}
		
		    	
		
		return view;
	}
	@Override
	public void onClick(View v) 
	{
		if(v.getId()==R.id.chart)
		{
			LayoutInflater li = LayoutInflater.from(getActivity());
			View promptsView = li.inflate(R.layout.chart_list, null);
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
			alertDialogBuilder.setView(promptsView);
			final LinearLayout layout=(LinearLayout) promptsView.findViewById(R.id.layout);
			final TextView month = (TextView) promptsView.findViewById(R.id.month);
			final TextView txtyear = (TextView) promptsView.findViewById(R.id.year);
			
			layout.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
				
			
					final Calendar c = Calendar.getInstance();
			         int Year = c.get(Calendar.YEAR);
			         int Month = c.get(Calendar.MONTH);
			         int Day = c.get(Calendar.DAY_OF_MONTH);        
			         DatePickerDialog dpd1 = new DatePickerDialog(getActivity(),
			                 new DatePickerDialog.OnDateSetListener() {

			                     @Override
			                     public void onDateSet(DatePicker view, int year,int monthOfYear, int dayOfMonth) {
			                         // Display Selected date in textbox
			                         //enddate.setText((monthOfYear + 1)+"-"+dayOfMonth +"-"+year);

			                     	 strtday=dayOfMonth;
			                         strtmonth=monthOfYear+1;
			                         monthforprogreport=monthOfYear+1+"";
			                         strtyear=year;             	
			                         
			                         popmonth=strtmonth+"";
			                         popyear=strtyear+"";
			                         
			                         month.setText(Repo_ProgressFragment.this.Month[strtmonth-1]);
			                         txtyear.setText(strtyear+"");
			                         
			                        			                         
			                      }
			                     }, Year, Month, Day);
			        
			         ((ViewGroup) dpd1.getDatePicker()).findViewById(Resources.getSystem().getIdentifier("day", "id", "android")).setVisibility(View.GONE);
			         dpd1.show();			      
				}
			});
			
			alertDialogBuilder
			.setCancelable(false)
			.setPositiveButton("OK",
			  new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog,int id) {	
			    	
			    	String line,result;			    	
			    	httpclient = new DefaultHttpClient();
			    	httppost = new HttpPost("http://"+LoginActivity.URL+"/schoolmanagement/designs/webservice/getmarks.php");
			    	//data to be send to web server to fetch data about the particulr class
			    	nameValuePairs= new ArrayList<NameValuePair>();
					nameValuePairs.add(new BasicNameValuePair("no",no));
					nameValuePairs.add(new BasicNameValuePair("classes",classes));
					nameValuePairs.add(new BasicNameValuePair("sections",sections));
					nameValuePairs.add(new BasicNameValuePair("month",monthforprogreport+""));
					nameValuePairs.add(new BasicNameValuePair("year",strtyear+""));
					Log.e("Month and Year",monthforprogreport+","+strtyear);
					
			     		
		        try
				{
					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
					HttpResponse response = httpclient.execute(httppost);
					HttpEntity entity = response.getEntity();					
					is = entity.getContent();
					BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
				    StringBuilder sb = new StringBuilder();
				    while ((line = reader.readLine()) != null)
		            {
		                sb.append(line + "\n");
		            }
		            is.close();
		            result = sb.toString();			
		            Log.e("Result",result);
		   
		            JSONObject json_data = new JSONObject(result);
		    	    Log.e("Json Data", json_data+"");	     
		            
		    	    JSONObject data = json_data.getJSONObject("DATA"); // get data object
		            String code = data.getString("CODE"); // get the name from data.
		            String message =data.getString("MESSAGE");
		            if(code.equals("200"))
		            {       
		            JSONArray json_subject_name=data.getJSONArray("SUBJECT");
		            JSONArray json_marks=data.getJSONArray("MARKS");
		            int i=0;
		            x=new int[json_subject_name.length()];
		            marks=new int[json_subject_name.length()];
		            subject_name=new String[json_subject_name.length()];
		            
		            for(i=0;i<json_subject_name.length();i++)
		            {
		            	x[i]=i+1;
		            	marks[i]=Integer.parseInt(json_marks.getString(i));
		            	subject_name[i]=json_subject_name.getString(i);
		            }     
		            
		            
		            XYSeries markSeries = new XYSeries("Marks");
					for(i=0;i<x.length;i++)
					{
						markSeries.add(x[i],marks[i]);
					}
					
					XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();
			        dataset.addSeries(markSeries);
			        XYSeriesRenderer attendencerender=new XYSeriesRenderer();
			       /* attendencerender.setColor(Color.BLUE);*/
			        attendencerender.setColor(Color.parseColor("#ffa500"));
			    	attendencerender.setPointStyle(PointStyle.CIRCLE);
			    	attendencerender.setFillPoints(true);
			    	attendencerender.setLineWidth((float) 10.5d);
			    	attendencerender.setDisplayChartValues(true);
			        
			    	XYMultipleSeriesRenderer multiRenderer = new XYMultipleSeriesRenderer();
			    	multiRenderer.setXLabels(0);
			    	/*multiRenderer.setChartTitle("Attendence of a student");*/
			    	multiRenderer.setChartTitle("Progress of student");
			    	multiRenderer.setXTitle("Subjects");
			    	multiRenderer.setBarSpacing(0.5);
			    
			    /*	multiRenderer.setYTitle("Percentage of Attendence");*/
			    	multiRenderer.setYTitle("Percentage of Marks");
			    	multiRenderer.setZoomButtonsVisible(true);    	    
			    	multiRenderer.setAxesColor(Color.YELLOW);
			    	multiRenderer.setLabelsColor(Color.YELLOW);
			   /*  multiRenderer.setChartValuesTextSize(BIND_ABOVE_CLIENT);*/
			    	multiRenderer.setBackgroundColor(Color.DKGRAY);
			    	
			    	//String[] mMonth ={"Tamil","English","Maths","Science","social"};
			    	for(i=0;i<x.length;i++){
			    		
			    		multiRenderer.addXTextLabel(i+1, subject_name[i]);    		
			    	}
			    	multiRenderer.addSeriesRenderer(attendencerender);
			    	Log.i("here","click");
			    	//Intent intent = ChartFactory.getLineChartIntent(getBaseContext(), dataset, multiRenderer);
			    	Intent intent = ChartFactory.getBarChartIntent(getActivity().getApplicationContext(), dataset, multiRenderer, org.achartengine.chart.BarChart.Type.DEFAULT);
			    	startActivity(intent);
		          
		            
		            }		            
		            if(code.equals("500"))
		            {
		            	Toast.makeText(getActivity().getApplicationContext(),message+"",Toast.LENGTH_LONG).show();
		            	
		            }
		        }
				catch(Exception e)
				{
					Log.e("Exception",e+"");
				}
			    	
			       
			    }
			  })
			.setNegativeButton("Cancel",
			  new DialogInterface.OnClickListener() {
			    public void onClick(DialogInterface dialog,int id) {
				dialog.cancel();
			    }
			  });			
			// create alert dialog
			AlertDialog alertDialog = alertDialogBuilder.create();

			// show it			
			alertDialog.setTitle("Select Month and year");
			alertDialog.show();
			
			
		}		
	}
}
