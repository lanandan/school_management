package com.gr_infotech.fragments;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.example.school_management.GcmIntentService;
import com.example.school_management.LoginActivity;
import com.example.school_management.R;
import com.gr_infotech.adapter.ChatArrayAdapter;
import com.gr_infotech.adapter.ChatMessage;
import com.gr_infotech.pojo.Chat_setter_getter;
import com.gr_infotech.pojo.DatabaseHandler;

public class Send_Classnotice_Fragment extends Fragment 
{
	Handler hand;
	Button send;
	EditText message;
	TableLayout msgcontents;
	HttpClient httpclient;
	HttpPost httppost;
	ArrayList<NameValuePair> nameValuePairs;
	InputStream is;
	String uid,code;
	String line="";
	String result="";
	String subtype="";
	String message_tbl_layt[];
	SharedPreferences shared,shared_gallery_name;	
	String type,phno,class_name,section;
	String subject;	
	    int flag;	
	    private static final String TAG = "ChatActivity";
	    private ChatArrayAdapter chatArrayAdapter;
	    private ListView listView;
	    private EditText chatText;
	    private ImageButton buttonSend;
	    private boolean side = false;    	    
	    private static final String SH_UIDS = "Tripuid";	
		private static final String UID = "UniqueID";
		private static final String TYPE = "Type";
		private static final String PHNO = "phone";
		
		String textToCopy="";
		String textToCut="";
		int position_cut = 0;
		private ActionMode mActionMode;
	
	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle args) {
        View view = inflater.inflate(R.layout.sendclassnoticefragment,container, false);
      
        chatText = (EditText) view.findViewById(R.id.msg);
        buttonSend = (ImageButton)view.findViewById(R.id.send);
        listView = (ListView)view.findViewById(R.id.msgview);  
        
    
        //To get uid and phone no to get parent registration ID        
        shared = getActivity().getSharedPreferences(SH_UIDS,Context.MODE_PRIVATE);
		uid=(shared.getString(UID,null));
		type=(shared.getString(TYPE,null));
	    phno=(shared.getString(PHNO,null));
        //to get selected Student name and class and section
	    shared=getActivity().getSharedPreferences("gallery_name",Context.MODE_PRIVATE);
        class_name=shared.getString("name",null);
        section=shared.getString("sec",null);
        shared =getActivity().getSharedPreferences("GCM",Context.MODE_PRIVATE);
        
        
        
    hand = new Handler(); 
	Runnable run = new Runnable()
	{
		@Override
		public void run()
		{
			Log.e("Welcome","this is hands");
			if(GcmIntentService.flags==1)
			{
				 
				 String notifytype=(shared.getString("notifytype",null));
				 if(notifytype!=null)
				 {				 
				 if(notifytype.equals("Send Class Notice"))
				 {
					 getchatMessage();	 
				 }
				 }
			}
			hand.postDelayed(this,1000);
		}
	};
		hand.postDelayed(run,1000);
        
      
        
        chatArrayAdapter =new ChatArrayAdapter(getActivity().getApplicationContext(),R.layout.activity_chat_singlemessage);
      
         listView.setAdapter(chatArrayAdapter);
        
        DatabaseHandler db = new DatabaseHandler(getActivity().getApplicationContext());
		List<Chat_setter_getter> chat=new ArrayList<Chat_setter_getter>();
		chat = db.getAllchat("Send Class Notice",class_name,section,"Notifications");
		
		Log.e("Welcome",chat.size()+"");
		
		for (Chat_setter_getter cn : chat) 
		{
			Log.e("Message",cn.getsendorreceive());
			if(cn.getsendorreceive().equals("receive".trim()))
			{
				 Log.e("Send or receive ",cn.getcontent());
				 Log.e("Send or receive ",cn.getsendorreceive());				 
				 side=true;
				 chatArrayAdapter.add(new ChatMessage(side,cn.getcontent()));
				 chatArrayAdapter.notifyDataSetChanged();
				 listView.setAdapter(chatArrayAdapter);				 
				
			}
			
			if(cn.getsendorreceive().equals("send".trim()))
			 {
				Log.e("Send or receive ",cn.getcontent());
				 Log.e("Send or receive ",cn.getsendorreceive());
				  
				side=false;
				 chatArrayAdapter.add(new ChatMessage(side,cn.getcontent()));
				chatArrayAdapter.notifyDataSetChanged();
				listView.setAdapter(chatArrayAdapter);
			 }
        }
        

        chatText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    
                	//return sendChatMessage();
                }
                return false;
            }
        });
        
        
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
            	
            	Log.e("entered","Entered");
            	
            	if(!chatText.getText().toString().equals(""))
        		{
            		//if(sendChatMessagetodatabase())
                	//{
                		sendChatMessage();	
                	//}                	
        		}
        		else
        		{
        			Toast.makeText(getActivity().getApplicationContext(),"Messages should be filled", Toast.LENGTH_LONG).show();
        		}
            	
            }
        });

        
        listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        listView.setAdapter(chatArrayAdapter);
        
        
        
        chatArrayAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                
                //chatArrayAdapter.notifyDataSetChanged();
                //listView.setSelection(chatArrayAdapter.getCount() - 1);
            }
        });
        
        
        if(GcmIntentService.flags==1)
        {   
        	 shared =getActivity().getSharedPreferences("GCM",Context.MODE_PRIVATE);
			 String notifytype=(shared.getString("notifytype",null));
			 if(notifytype.equals("Send Class Notice"))
			 {
				 getchatMessage();	 
			 }        
        }
           
        

		listView.setOnItemLongClickListener(new OnItemLongClickListener(){

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				//textToCopy = (String) listView.getItemAtPosition(position);
				//((TextView)v.findViewById(R.id.singleMessage)).setCustomSelectionActionModeCallback();
				position_cut = position;
				openAlert(view);
					
				// mActionMode = getActivity().startActionMode(new ActionBarCallBack());
				
				return false;
			}
			
		});
        
        
 	    return view;
    }
	

	
	void copy(View v){
		
		textToCopy = "";
		textToCopy = ((TextView)v. findViewById(R.id.singleMessage)).getText().toString();     
		Log.e("test",textToCopy);
		getActivity();
		ClipboardManager clipMan = (ClipboardManager)getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
		ClipData clip = ClipData.newPlainText("label", textToCopy);
		
		clipMan.setPrimaryClip(clip);
	}
	
	void deleteText(View v){
		
		View new_view = ((TextView)v. findViewById(R.id.singleMessage));
		((LinearLayout)new_view.getParent()).removeView(new_view);
	}
	
	void cutAndPaste(View v){
		textToCut = "";
		textToCut = ((TextView)v. findViewById(R.id.singleMessage)).getText().toString();  
		Log.e("test",textToCut);
		
getActivity();
		View new_view1 = ((TextView)v. findViewById(R.id.singleMessage));
		((LinearLayout)new_view1.getParent()).removeView(new_view1);
		ClipboardManager clipMan_cut = (ClipboardManager)getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
		ClipData clip_cut = ClipData.newPlainText("label1", textToCut);
		clipMan_cut.setPrimaryClip(clip_cut);
		
	}
	
	View vw_new;
	private void openAlert(View v) {
		// TODO Auto-generated method stub
		String alrt_array[] = {"Copy", "Delete", "Cut"};
		vw_new = v;
		AlertDialog.Builder alrt = new AlertDialog.Builder(getActivity());
		
		alrt.setTitle("Message options");
		//alrt.setMessage("Are you sure to Send?");
		alrt.setItems(alrt_array,new DialogInterface.OnClickListener(){
			public void onClick(DialogInterface dialog, int id) {
				
				switch(id){
				
				case 0:
					
					copy(vw_new);
					break;
				
				case 1:
					
					deleteText(vw_new);
					break;
				case 2:
					
					cutAndPaste(vw_new);
					break;
				}
					
				
				
			}
		});			

		
		AlertDialog alertDialog = alrt.create();
		// show alert
		alertDialog.show();

	}
	
	private boolean getchatMessage()
	{
		if(GcmIntentService.flags==1)
		{	
			 shared =getActivity().getSharedPreferences("GCM",Context.MODE_PRIVATE);
			 String notifytype=(shared.getString("notifytype",null));
			 if(notifytype.equals("Send Class Notice"))
			 {			
			String message=(shared.getString("gcmintent_message",null));	 			
			chatArrayAdapter.add(new ChatMessage(side,message));
			chatArrayAdapter.notifyDataSetChanged();
			listView.setAdapter(chatArrayAdapter);			
			DatabaseHandler db = new DatabaseHandler(getActivity().getApplicationContext());
			//db.addData(new Chat_setter_getter("receive",message,"Send Class Notice"));   
			List<Chat_setter_getter> chat=new ArrayList<Chat_setter_getter>();				
						
			chat = db.getAllchat("Send Class Notice",class_name,section,"Notifications");
			
			for (Chat_setter_getter cn : chat) 
			{
				Log.e("Message",cn.getsendorreceive());
				if(cn.getsendorreceive().equals("receive"))
				{
					 Log.e("Send or receive ",cn.getcontent());
					 flag=1;
					 side=true;
					 chatArrayAdapter.add(new ChatMessage(side,cn.getcontent()));
					 chatArrayAdapter.notifyDataSetChanged();
					 listView.setAdapter(chatArrayAdapter);
				}
				
				if(cn.getsendorreceive().equals("send"))
				 {
					 side=false;					  
					 chatArrayAdapter.add(new ChatMessage(side,cn.getcontent()));
					 chatArrayAdapter.notifyDataSetChanged();
					 listView.setAdapter(chatArrayAdapter);
				 }
	     	}	
			 GcmIntentService.flags=0;	
		 }
		}		
		return true;
	}		
	
	
	private boolean sendChatMessage() 
	{		
		Log.e("Sending","Sending");
		//DatabaseHandler db = new DatabaseHandler(getActivity().getApplicationContext());
		//db.addData(new Chat_setter_getter("send",chatText.getText().toString(),"Send Class Notice",class_name,section,"SendClassNotices")); 
		//db.close();       
		side=false;
		chatArrayAdapter.add(new ChatMessage(side,chatText.getText().toString()));	
		chatArrayAdapter.notifyDataSetChanged();
		listView.setAdapter(chatArrayAdapter);
		String webServiceInfo;
        String class_notice=chatText.getText().toString();
	        try {
	        	httpclient = new DefaultHttpClient();        	
	 		    HttpParams httpParameters = new BasicHttpParams();
	 		    HttpClient client = new DefaultHttpClient(httpParameters); 		    
	 		    httppost = new HttpPost("http://"+LoginActivity.URL+"/schoolmanagement/designs/webservice/GCM.php");
				nameValuePairs= new ArrayList<NameValuePair>();				
				nameValuePairs.add(new BasicNameValuePair("type","Teacher"));
				nameValuePairs.add(new BasicNameValuePair("message","Notification~"+chatText.getText().toString()+"~"+class_name+"~"+section+"~"+"SendClassNotices"));								
				nameValuePairs.add(new BasicNameValuePair("class_name",class_name));
				nameValuePairs.add(new BasicNameValuePair("section",section));
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));				
				Log.e("testing",nameValuePairs+"");
				HttpResponse response = httpclient.execute(httppost); 
				HttpEntity entity = response.getEntity();			
				BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
	 		    while ((webServiceInfo = rd.readLine()) != null) {
		 		     Log.d("****Status Log***", "Webservice: " + webServiceInfo);
		 		    } 		    
	 		   } catch (Exception e) {
	 		    e.printStackTrace();
	 		   }        
	     chatText.setText("");
        return true;
    }	 

	
	
	
	public boolean sendChatMessagetodatabase()
	{
		boolean returns=false;		
		shared = getActivity().getSharedPreferences(SH_UIDS,Context.MODE_PRIVATE);
		uid=(shared.getString(UID,null));
		type=(shared.getString(TYPE,null));
	    phno=(shared.getString(PHNO,null));        
	    shared=getActivity().getSharedPreferences("gallery_name",Context.MODE_PRIVATE);
        class_name=shared.getString("name",null);
        section=shared.getString("sec",null);       
		
		try
		{
			httpclient = new DefaultHttpClient();
			String httpid = "http://"+LoginActivity.URL+"/schoolmanagement/designs/webservice/GCM.php";
			httppost = new HttpPost(httpid);
			nameValuePairs= new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("uid",uid));
			nameValuePairs.add(new BasicNameValuePair("type",type));
			nameValuePairs.add(new BasicNameValuePair("phno",phno));
			nameValuePairs.add(new BasicNameValuePair("class_name",class_name));
			nameValuePairs.add(new BasicNameValuePair("section",section));
			nameValuePairs.add(new BasicNameValuePair("message",chatText.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("label","send_class_notice"));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost); 
			HttpEntity entity = response.getEntity();		
			is = entity.getContent();			
				    BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
				    StringBuilder sb = new StringBuilder();
				    while ((line = reader.readLine()) != null)
		            {					    	
		                sb.append(line + "\n");
		               
		            }
		            is.close();
		            result = sb.toString();
		            Log.e("message", result);
		         JSONObject json_data = new JSONObject(result);
			     JSONObject data = json_data.getJSONObject("DATA");		         
			     String code = data.getString("CODE");
			     String messages =data.getString("MESSAGE");         
		         
			    if(code.equals("200"))
		         {
		        	  returns=true;
		         }
		         if(code.equals("500"))
		         {
		        	 returns=false;
		         }        
		               					
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return returns;
		
	}
	
}

