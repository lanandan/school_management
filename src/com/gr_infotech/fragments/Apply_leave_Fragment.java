package com.gr_infotech.fragments;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import com.example.school_management.LoginActivity;
import com.example.school_management.R;
import com.example.school_management.SettingsScreenActivity;
import com.example.school_management.LoginActivity.Login_process;
import com.gr_infotech.utilities.Network_checking;

import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Apply_leave_Fragment extends Fragment implements OnClickListener 
{
	EditText reason;
	TextView start_date,end_date,name;
	Button send;
	Calendar cal;
	int day,month,year;	
	int strtday,strtmonth,strtyear;
	int endday,endmonth,endyear;
	Date date1,date2;
	String stud_name,stud_class,stud_sec,stud_no;
	ProgressDialog pd;
	HttpClient httpclient;
	HttpPost httppost;
	ArrayList<NameValuePair> nameValuePairs;
	InputStream is;
	String line,result,uid,type,phno,message;	
	SharedPreferences shared;
	String class_name,section;	
	SimpleDateFormat sfday,sfmonth,sfyear;
	String strday,strmonth,stryear,current_date,current_time;
	
	 	private static final String SH_UIDS = "Tripuid";	
		private static final String UID = "UniqueID";
		private static final String TYPE = "Type";
		private static final String PHNO = "phone";
	
	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle args)
	{
        View view = inflater.inflate(R.layout.apply_leave,container, false);        
        name=(TextView)view.findViewById(R.id.leave_request_name);
        reason=(EditText)view.findViewById(R.id.reason);
        start_date=(TextView)view.findViewById(R.id.start_date);
        end_date=(TextView)view.findViewById(R.id.end_date);
        send=(Button)view.findViewById(R.id.btn_send);
        send.setOnClickListener(this);
        start_date.setOnClickListener(this);
        end_date.setOnClickListener(this);        
        cal = Calendar.getInstance();
        day = cal.get(Calendar.DAY_OF_MONTH);
        month = cal.get(Calendar.MONTH);
        year = cal.get(Calendar.YEAR);
        shared = getActivity().getSharedPreferences(SH_UIDS,Context.MODE_PRIVATE);
		uid=(shared.getString(UID,null));
		type=(shared.getString(TYPE,null));
	    phno=(shared.getString(PHNO,null));
	    
	    shared=getActivity().getSharedPreferences("gallery_name",Context.MODE_PRIVATE);
        stud_name=shared.getString("name",null);
        stud_class=shared.getString("class",null);
        stud_sec=shared.getString("sec",null);
	    stud_no=shared.getString("no",null);
	    name.setText(stud_name);
	    
	 
	    Date time = new Date();
        String strDateFormat = "hh:mm:ss a";
        SimpleDateFormat sdf = new SimpleDateFormat(strDateFormat);        
        current_time=sdf.format(time);
        
	    sfday = new SimpleDateFormat("dd", Locale.getDefault());
		sfmonth = new SimpleDateFormat("MMM", Locale.getDefault());
		sfyear = new SimpleDateFormat("yyyy", Locale.getDefault());
		strday = sfday.format(cal.getTime());
		strmonth = sfmonth.format(cal.getTime());
		stryear = sfyear.format(cal.getTime());
		current_date=strday+" / "+strmonth+" / "+stryear;
        
        return view;        
	}

	@Override
	public void onClick(View v) 
	{

		if(v.getId()==R.id.start_date)
     {
         // Process to get Current Date
         final Calendar c = Calendar.getInstance();
         int Year = c.get(Calendar.YEAR);
         int Month = c.get(Calendar.MONTH);
         int Day = c.get(Calendar.DAY_OF_MONTH);

         // Launch Date Picker Dialog
         DatePickerDialog dpd = new DatePickerDialog(getActivity(),
                 new DatePickerDialog.OnDateSetListener() {

                     @Override
                     public void onDateSet(DatePicker view, int year,int monthOfYear, int dayOfMonth) {
                         // Display Selected date in textbox
                        // strtdate.setText((monthOfYear + 1)+"-"+dayOfMonth +"-"+year);

                     	endday=dayOfMonth;
                     	endmonth=monthOfYear+1;
                     	endyear=year; 
                     	
                     	  try{

           	                SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
           	                date1 = sdf.parse(strtmonth+"-"+strtday+"-"+strtyear);
           	                date2 = sdf.parse(endmonth+"-"+endday+"-"+endyear);

           	                if(date1.after(date2)){
           	                   start_date.setText("");
           	                   end_date.setText("");
           	                   Toast.makeText(getActivity().getApplicationContext(), "Invalid date selected", Toast.LENGTH_LONG).show();
           	                }

           	                if(date1.before(date2)||date1.equals(date2))
           	                {
           	                	start_date.setText(sdf.format(date1));
           	                	end_date.setText(sdf.format(date2));           	                
           	               }

           	                /*
           	                if(date1.equals(date2))
           	                {
           	                    start_date.setText("");
            	                end_date.setText("");
            	                Toast.makeText(getActivity().getApplicationContext(), "Invalid date selected", Toast.LENGTH_LONG).show();
           	                }*/

           	            }catch(Exception ex){
           	            	ex.printStackTrace();
           	            }
                     }
                     }, Year, Month, Day);
         dpd.setTitle("End Date");
         dpd.show();
         
         
         
         
      // Launch Date Picker Dialog
         DatePickerDialog dpd1 = new DatePickerDialog(getActivity(),
                 new DatePickerDialog.OnDateSetListener() {

                     @Override
                     public void onDateSet(DatePicker view, int year,int monthOfYear, int dayOfMonth) {
                         // Display Selected date in textbox
                         //enddate.setText((monthOfYear + 1)+"-"+dayOfMonth +"-"+year);

                     	strtday=dayOfMonth;
                         strtmonth=monthOfYear+1;
                         strtyear=year;
                     	
                      }
                     }, Year, Month, Day);
         dpd1.setTitle("Start Date");
         dpd1.show();
       }

	if(v.getId()==R.id.btn_send)
	{
		
		if(leaveValidation())
		{
		
			if(Network_checking.isNetworkAvailable(getActivity()))
			{
				Apply_leave_process leave=new Apply_leave_process();
				leave.execute();				
			}
			else
			{
				showToast("Please check your network connection");
			}
		}			
		
	}
	
	}
	
	public boolean leaveValidation()
	{
		String strstart_date=start_date.getText().toString();
		String strend_date=end_date.getText().toString();
		String strreason=reason.getText().toString();
		
		
		Drawable dr = getResources().getDrawable(R.drawable.star_boxed_full); 
        //add an error icon to yur drawable files
		dr.setBounds(0, 0, dr.getIntrinsicWidth(), dr.getIntrinsicHeight());
		
		
		if (strstart_date.equals("".trim()) || strend_date.equals("".trim()) || strreason.equals("".trim()))
		{
		
			if(start_date.getText().toString().trim().equalsIgnoreCase(""))
			{
				start_date.setError("Start date should be selected",dr);
			}
			if(end_date.getText().toString().trim().equalsIgnoreCase(""))
			{
				end_date.setError("End date should be selected",dr);
			}
			if(reason.getText().toString().trim().equalsIgnoreCase(""))
			{
				reason.setError("Reasons should be given",dr);
			}
			
			
			return false;
		}
		
		else
		{
		
		
			return true;
		}
	}
	
	
	private void showToast(String msg)
	{
		Toast.makeText(getActivity().getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
	}	
	
	
	
	public class Apply_leave_process extends AsyncTask<Void, Void, ArrayList<String>>
	{	
		int flag=0;
		
		
		@Override
		protected ArrayList<String> doInBackground(Void... params) {					
			
			if(leave_request())
			{
				flag=1;
			}
			else
			{
				flag=2;
			}
			return null;
		}		
		@Override
	    protected void onPreExecute() {
	            // TODO Auto-generated method stub
	            super.onPreExecute();                     
	            pd = new ProgressDialog(getActivity(),R.style.MyTheme1);
				pd.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progressbar_custom));
				pd.setCancelable(false);
				pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
				pd.show();	            
	            //pd = ProgressDialog.show(LoginActivity.this, "Logging in","Please Wait");
	    }		
		@Override
	    protected void onPostExecute(ArrayList<String> contacts) {
	        super.onPostExecute(contacts);
	        pd.cancel();		        
	        if(flag==1)
	        {
	        	Toast.makeText(getActivity().getApplicationContext(),"Leave Request Updated successfuly", Toast.LENGTH_SHORT).show();
	        }
	        if(flag==2)
	        {
	        	Toast.makeText(getActivity().getApplicationContext(),message, Toast.LENGTH_SHORT).show();
	        }	        
	    }
		
		public boolean leave_request()
		{
			boolean returns = false;
			try
			{
				httpclient = new DefaultHttpClient();
				String httpid = "http://"+LoginActivity.URL+"/schoolmanagement/designs/webservice/leave_request.php";
				httppost = new HttpPost(httpid);
				nameValuePairs= new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("name",name.getText().toString()));
				nameValuePairs.add(new BasicNameValuePair("phoneno",phno));
				nameValuePairs.add(new BasicNameValuePair("uid",uid));
				nameValuePairs.add(new BasicNameValuePair("type",type));				
				nameValuePairs.add(new BasicNameValuePair("reason",reason.getText().toString()));
				nameValuePairs.add(new BasicNameValuePair("start_date",start_date.getText().toString()));
				nameValuePairs.add(new BasicNameValuePair("end_date",end_date.getText().toString()));
				nameValuePairs.add(new BasicNameValuePair("class",stud_class));
				nameValuePairs.add(new BasicNameValuePair("sec",stud_sec));
				nameValuePairs.add(new BasicNameValuePair("register_no",stud_no));
				nameValuePairs.add(new BasicNameValuePair("current_date",current_date));
				nameValuePairs.add(new BasicNameValuePair("current_time",current_time));
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				HttpResponse response = httpclient.execute(httppost); 
				HttpEntity entity = response.getEntity();				
				is = entity.getContent();			
				Log.e("Name Value Pairs",nameValuePairs+"");
				
						BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
					    StringBuilder sb = new StringBuilder();
					    while ((line = reader.readLine()) != null)
			            {					    	
			                sb.append(line + "\n");
			            }
			            is.close();
			            result = sb.toString();
			          
			         JSONObject json_data = new JSONObject(result);
				     JSONObject data = json_data.getJSONObject("DATA");		         
				     String code = data.getString("CODE");
				     message =data.getString("MESSAGE");		         
			         if(code.equals("200"))
			         {
			        	  returns=true;
			         }
			         if(code.equals("500"))
			         {
			        	 returns=false;
			         }       
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			return returns;	
		}		
	}	
}