package com.gr_infotech.fragments;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import com.example.school_management.AddChildActivity;
import com.example.school_management.LoginActivity;
import com.example.school_management.R;
import com.example.school_management.RegisterActivity;
import com.example.school_management.RegisterActivity.Registration_process;
import com.example.school_management.SettingsScreenActivity;
import com.gr_infotech.utilities.Network_checking;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.InputFilter;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class Edit_account_fragment extends Fragment implements OnClickListener 
{
	EditText name,mobile_no,passwd,answer;
	Button update;
	TextView type;
	Spinner sec_question;	
	String[] spinner_type = {"Select Type","Teacher","Parent"};
	String[] question={"Security Question","what is your school name?","What is your pet’s name?","What is your favorite game?","What month were you born?","What is your mother’s birthday?"};
	ArrayAdapter<String> adapter_type,adapter_question;
	String strname,strmobile_no,strpwd,stranswer;
	HttpClient httpclient;
	HttpPost httppost;
	ArrayList<NameValuePair> nameValuePairs;
	InputStream is;
	String line,result;
	String uid,str_type,phno;
	SharedPreferences shared;
	Typeface nobileMedium, nobileRegular, nobileBoldItalic, nobileMediumItalic;
	String sec_questions,types;
	Resources res;	
	ProgressDialog pd;
	private static final String SH_UIDS = "Tripuid";	
	private static final String UID = "UniqueID";
	private static final String TYPE = "Type";
	private static final String PHNO = "phone";	
    String message;
    String dbphno,dbpwd,dbtype,dbsecquestion,dbanswer,dbuid,dbname;    
    
	
	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle args) 
	{
        View view = inflater.inflate(R.layout.edit_account_fragment,container, false);
	    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);		
		nobileMedium = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nobile-Medium.ttf");
		nobileRegular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nobile-Regular.ttf");
		nobileBoldItalic = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nobile-BoldItalic.ttf");
		nobileMediumItalic = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Nobile-MediumItalic.ttf");
		
		name=(EditText)view.findViewById(R.id.regis_edt_name);
		mobile_no=(EditText)view.findViewById(R.id.regis_edt_mobileno);
		passwd=(EditText)view.findViewById(R.id.regs_edt_pwd);
		answer=(EditText)view.findViewById(R.id.regs_edt_ans);
		update=(Button)view.findViewById(R.id.regs_btn_update);
		update.setOnClickListener(this);		
		update.setTypeface(nobileBoldItalic);		
		name.setTypeface(nobileRegular);
		mobile_no.setTypeface(nobileRegular);
		answer.setTypeface(nobileRegular);
		
		shared = getActivity().getSharedPreferences(SH_UIDS,Context.MODE_PRIVATE);
		uid=(shared.getString(UID,null));
		str_type=(shared.getString(TYPE,null));
	    phno=(shared.getString(PHNO,null));	
	    
	    
		type=(TextView)view.findViewById(R.id.regs_select_type);
		sec_question=(Spinner)view.findViewById(R.id.regs_secquestion);	
		
		adapter_question = new ArrayAdapter(getActivity(),android.R.layout.simple_spinner_item,question);
		adapter_question.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sec_question.setAdapter(adapter_question);
		
		if(get_editaccount_data(uid,phno,str_type))
		{
			name.setText(dbname);
			mobile_no.setText(dbphno);
			passwd.setText(dbpwd);
			answer.setText(dbanswer);
			type.setText(dbtype);	
		}
		else
		{
			showToast("Data is not Available for your ID");
		}
		return view;        
	}
	
	
	
	
	public boolean get_editaccount_data(String uid,String phno,String str_type)
	{
		boolean returns=false;
		
		try
		{
			httpclient = new DefaultHttpClient();
			httppost = new HttpPost("http://"+LoginActivity.URL+"/schoolmanagement/designs/webservice/get_editaccount_data.php");
			nameValuePairs= new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("uid",uid));
			nameValuePairs.add(new BasicNameValuePair("type",str_type));
			nameValuePairs.add(new BasicNameValuePair("phno",phno));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost); 
			HttpEntity entity = response.getEntity();			
			is = entity.getContent();			
				    BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
				    StringBuilder sb = new StringBuilder();
				    while ((line = reader.readLine()) != null)
		            {				    	
		                sb.append(line + "\n");		               
		            }
		            is.close();
		            result = sb.toString();
		            Log.e("message", result);
		         JSONObject json_data = new JSONObject(result);
			     JSONObject data = json_data.getJSONObject("DATA");		         
			     String code = data.getString("CODE");
			     
			     message =data.getString("MESSAGE");	
			     dbname=data.getString("NAME");
			     dbuid=data.getString("UID");
			     dbphno=data.getString("PHNO");
			     dbpwd=data.getString("PASSWORD");
			     dbtype=data.getString("TYPE");
			     dbsecquestion=data.getString("SEC_QUESTION");
			     dbanswer=data.getString("ANSWER");   
			     
			     Log.e("Type",dbtype);
			     
			     if(code.equals("200"))
		         {
		        	 returns=true;
		         }
		         if(code.equals("500"))
		         {		       	 
		        	 returns=false;
		         }  
		         
		         if(code.equals("501"))
		         {		       	 
		        	 returns=false;
		         }  
		         
		         
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return returns;	
	}

	
	


	@Override
	public void onClick(View v) 
	{	
		if(v.getId()==R.id.regs_btn_update)
		{
			
			
			if(update_registration_validation())
			{
			
				if(Network_checking.isNetworkAvailable(getActivity()))
				{
					
					update_account_process register=new update_account_process();
					register.execute();					
				}
				else
				{
					showToast("Please check your network connection");
				}
			}			
		}		
	}
	
	
	private void showToast(String msg)
	{
		Toast.makeText(getActivity().getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
	}	
	
	
	public boolean update_registration()
	{
		boolean returns=false;
		try
		{
			httpclient = new DefaultHttpClient();
			httppost = new HttpPost("http://"+LoginActivity.URL+"/schoolmanagement/designs/webservice/update_register.php");
			nameValuePairs= new ArrayList<NameValuePair>();	
			
			nameValuePairs.add(new BasicNameValuePair("uid",uid));
			nameValuePairs.add(new BasicNameValuePair("name",name.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("phno",mobile_no.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("password",passwd.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("answer",answer.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("type",type.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("security_question",sec_question.getSelectedItem().toString()));					
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));			
			HttpResponse response = httpclient.execute(httppost);
			Log.e("entered","entered");
			HttpEntity entity = response.getEntity();
			Log.e("entered","entered");
			is = entity.getContent();	
			Log.e("entered","entered");
			
				    BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
				    StringBuilder sb = new StringBuilder();
				    while ((line = reader.readLine()) != null)
		            {
		                sb.append(line + "\n");
		                Log.e("entered",line);		                
		            }
		            is.close();
		            Log.e("entered","entered1");
		            result = sb.toString();		
		            Log.e("entered","entered2");
		            Log.e("Result",result);
		        
		         JSONObject json_data = new JSONObject(result);
			     JSONObject data = json_data.getJSONObject("DATA");		         
			     String code = data.getString("CODE");
		         message =data.getString("MESSAGE");		         
		         if(code.equals("200"))
		         {
		        	 
		        	  returns=true;
		         }
		         if(code.equals("500"))
		         {		        	 
		        	 returns=false;
		         }		         
		              					
		}
		catch(Exception e)
		{
			Log.e("Exception",e+"");
		}
		return returns;	
	}
	
	
	public boolean update_registration_validation()
	{
		strname = name.getText().toString().trim();
		strmobile_no = mobile_no.getText().toString().trim();
		strpwd=passwd.getText().toString().trim();
		stranswer=answer.getText().toString().trim();
		sec_questions=sec_question.getSelectedItem().toString();
				
		
		if (strname.equals("".trim()) || strmobile_no.equals("".trim())||strpwd.equals("".trim())||stranswer.equals("".trim())||sec_questions.equals("Security Question"))
		{
			Drawable dr = getResources().getDrawable(R.drawable.star_boxed_full); 
            dr.setBounds(0, 0, dr.getIntrinsicWidth(), dr.getIntrinsicHeight());
			
			if(name.getText().toString().trim().equalsIgnoreCase(""))
			{
				name.setError("Name field should be filled",dr);
				
			}
			if(mobile_no.getText().toString().trim().equalsIgnoreCase(""))
			{
				mobile_no.setError("Mobile No field should be filled",dr);
				
			}
			if(passwd.getText().toString().trim().equalsIgnoreCase(""))
			{
				passwd.setError("Password field should be filled",dr);
				
			}
			if(answer.getText().toString().trim().equalsIgnoreCase(""))
			{
				answer.setError("Answer field should be filled",dr);
				
			}
			if(sec_questions.equals("Security Question"))
			{
				((TextView)sec_question.getChildAt(0)).setError("Select Security Question",dr);
				
				//showToast("Security Question should be Selected");			
			}
	        
			showToast("Please Fill all the fields");
			return false;
		}
		else
		{
			Drawable dr = getResources().getDrawable(R.drawable.star_boxed_full); 
            dr.setBounds(0, 0, dr.getIntrinsicWidth(), dr.getIntrinsicHeight());
			if (mobile_no.length() == 10)
			{
				if (strpwd.length() > 3)
					return true;
				else
				{
					showToast("Password Should be Minimum 4 Digits");
					passwd.setText("");		
					passwd.setError("Password Should be Minimum 4 Digits",dr);
					passwd.requestFocus();
					return false;
				}
			}				
			{
				showToast("Invalid Mobile Number");
				mobile_no.setText("");
				mobile_no.setError("Invalid Mobile Number",dr);
				mobile_no.requestFocus();
				return false;
			}
		}
		
		
	}
	
	
	
	
	public class update_account_process extends AsyncTask<Void, Void, ArrayList<String>>
	{	
		int flag=0;
		
		
		@Override
		protected ArrayList<String> doInBackground(Void... params) {					
			
			if(update_registration())
			{
				flag=1;
			}
			else
			{
				flag=2;
			}
			return null;
		}		
		@Override
	    protected void onPreExecute() {
	            // TODO Auto-generated method stub
	            super.onPreExecute();                     
	            pd = new ProgressDialog(getActivity(),R.style.MyTheme1);
				pd.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progressbar_custom));
				pd.setCancelable(false);
				pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
				pd.show();	            
	            //pd = ProgressDialog.show(LoginActivity.this, "Logging in","Please Wait");
	    }		
		@Override
	    protected void onPostExecute(ArrayList<String> contacts) {
	        super.onPostExecute(contacts);
	        pd.cancel();		        
	        if(flag==1)
	        {
	        	Toast.makeText(getActivity().getApplicationContext(),"Updated Successfully", Toast.LENGTH_SHORT).show();
	        }
	        if(flag==2)
	        {
	        	Toast.makeText(getActivity().getApplicationContext(),message, Toast.LENGTH_SHORT).show();
	        }	        
	    }
	
	}
	
	
	
}