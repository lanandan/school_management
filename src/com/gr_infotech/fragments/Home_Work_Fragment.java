package com.gr_infotech.fragments;
import android.app.DatePickerDialog;
import android.app.Fragment;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;
import com.example.school_management.GcmIntentService;
import com.example.school_management.LoginActivity;
import com.example.school_management.R;
import com.gr_infotech.adapter.ChatArrayAdapter;
import com.gr_infotech.adapter.ChatMessage;
import com.gr_infotech.pojo.Chat_setter_getter;
import com.gr_infotech.pojo.DatabaseHandler;
import android.annotation.TargetApi;
import android.app.ActionBar.LayoutParams;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.style.StrikethroughSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class Home_Work_Fragment extends Fragment implements OnClickListener
{
		Handler hand;
		Button send;
		EditText message,subj;
		TableLayout msgcontents;
		HttpClient httpclient;
		HttpPost httppost;
		ArrayList<NameValuePair> nameValuePairs;
		InputStream is;
		String uid,code;
		String line="";
		String result="";
		String subtype="";
		String message_tbl_layt[];
		SharedPreferences shared,shared_gallery_name;	
		String type,phno,class_name,section;
		ImageButton get_homework;
		int strtday,strtmonth,strtyear;
		Date date1;
		String date;
	    int flag;	
	    private static final String TAG = "ChatActivity";
	    private ChatArrayAdapter chatArrayAdapter;
	    private ListView listView;
	    private EditText chatText;
	    private ImageButton buttonSend;
	    private boolean side = false;    	    
	    private static final String SH_UIDS = "Tripuid";	
		private static final String UID = "UniqueID";
		private static final String TYPE = "Type";
		private static final String PHNO = "phone";
	
	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle args) {
        View view = inflater.inflate(R.layout.home_work_fragment,container, false);
        
        chatText = (EditText) view.findViewById(R.id.msg);
        subj=(EditText)view.findViewById(R.id.subject);
        buttonSend = (ImageButton)view.findViewById(R.id.send);
        listView = (ListView)view.findViewById(R.id.msgview);
        get_homework=(ImageButton)view.findViewById(R.id.views);
        get_homework.setOnClickListener(this);
        
        shared = getActivity().getSharedPreferences(SH_UIDS,Context.MODE_PRIVATE);
		uid=(shared.getString(UID,null));
		type=(shared.getString(TYPE,null));
	    phno=(shared.getString(PHNO,null));    		
        
        shared=getActivity().getSharedPreferences("gallery_name",Context.MODE_PRIVATE);
        class_name=shared.getString("name",null);
        section=shared.getString("sec",null);
        shared =getActivity().getSharedPreferences("GCM",Context.MODE_PRIVATE);
        
    hand = new Handler(); 
	Runnable run = new Runnable()
	{
		@Override
		public void run()
		{
			Log.e("Welcome","this is hands");
			if(GcmIntentService.flags==1)
			{
				 
				 String notifytype=(shared.getString("notifytype",null));
				 if(notifytype!=null)
				 {
				 if(notifytype.equals("Home Work"))
				 {
					 getchatMessage();	 
				 }
				 }
			}
			hand.postDelayed(this,1000);
		}
	};
		hand.postDelayed(run,1000);
        
        chatArrayAdapter =new ChatArrayAdapter(getActivity().getApplicationContext(),R.layout.activity_chat_singlemessage);
        listView.setAdapter(chatArrayAdapter);
        
        
        try
    	{	   		 
    		
    		httpclient = new DefaultHttpClient();
    		httppost = new HttpPost("http://"+LoginActivity.URL+"/schoolmanagement/designs/webservice/get_teacher_subject.php");
    		nameValuePairs= new ArrayList<NameValuePair>();		
    		nameValuePairs.add(new BasicNameValuePair("class_name",class_name));
    		nameValuePairs.add(new BasicNameValuePair("section",section));
    		nameValuePairs.add(new BasicNameValuePair("phno",phno));    		    		
    		httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
    		HttpResponse response = httpclient.execute(httppost); 
    		HttpEntity entity = response.getEntity();			
    		is = entity.getContent();			
    			    BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
    			    StringBuilder sb = new StringBuilder();
    			    while ((line = reader.readLine()) != null)
    	            {			    	
    	                sb.append(line + "\n");	               
    	            }
    	            is.close();
    	            result = sb.toString();	        
    	            Log.e("Result",result);
    	         JSONObject json_data = new JSONObject(result);
    		     JSONObject data = json_data.getJSONObject("DATA");		         
    		     String code = data.getString("CODE");
    	         String url_message =data.getString("MESSAGE");       
    	         if(code.equals("200"))
    	         {		        	 
    	        	 subj.setText(data.getString("SUBJECT"));   
    	        	  
    	         }
    	         if(code.equals("500"))
    	         {
    	        	 subj.setText("Sorry No data");		        
    	         }        
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
        
        
        
        DatabaseHandler db = new DatabaseHandler(getActivity().getApplicationContext());
		List<Chat_setter_getter> chat=new ArrayList<Chat_setter_getter>();
		chat = db.getAllchat("Home Work",class_name,section,subj.getText().toString());
		for (Chat_setter_getter cn : chat) 
		{
			Log.e("Message",cn.getsendorreceive());
			if(cn.getsendorreceive().equals("receive".trim()))
			{
				 side=true;
				 chatArrayAdapter.add(new ChatMessage(side,cn.getcontent()));
				 chatArrayAdapter.notifyDataSetChanged();
				 listView.setAdapter(chatArrayAdapter);				 
				
			}
			
			if(cn.getsendorreceive().equals("send".trim()))
			 {
				side=false;
				chatArrayAdapter.add(new ChatMessage(side,cn.getcontent()));
				chatArrayAdapter.notifyDataSetChanged();
				listView.setAdapter(chatArrayAdapter);
			 }
        }
        
        
        
        chatText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    
                	return sendChatMessage();
                }
                return false;
            }
        });
        
        
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
            	
            	
            	if(!chatText.getText().toString().equals(""))
        		{
            		//if(sendChatMessagetodatabase())
                	//{
                		sendChatMessage();	
                	//}                	
        		}
        		else
        		{
        			Toast.makeText(getActivity().getApplicationContext(),"Messages should be filled", Toast.LENGTH_LONG).show();
        		}
            	
            }
        });

        
        listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        listView.setAdapter(chatArrayAdapter);  
      
        
        /*
        if(GcmIntentService.flags==1)
        {   
        	 shared =getActivity().getSharedPreferences("GCM",Context.MODE_PRIVATE);
			 String notifytype=(shared.getString("notifytype",null));
			 if(notifytype.equals("Home Work"))
			 {
				 getchatMessage();	 
			 }        
        }
        */
        //To get the subjects handling by teacher
        
      
        
 	    return view;
    }

	private boolean getchatMessage()
	{
		if(GcmIntentService.flags==1)
		{	
			 shared =getActivity().getSharedPreferences("GCM",Context.MODE_PRIVATE);
			 String notifytype=(shared.getString("notifytype",null));
			 
			 if(notifytype.equals("Home Work"))
			 {			
			String message=(shared.getString("gcmintent_message",null));	 			
			chatArrayAdapter.add(new ChatMessage(side,message));
			chatArrayAdapter.notifyDataSetChanged();
			listView.setAdapter(chatArrayAdapter);						
			List<Chat_setter_getter> chat=new ArrayList<Chat_setter_getter>();				
			DatabaseHandler db = new DatabaseHandler(getActivity().getApplicationContext());
	        chat = db.getAllchat("Home Work",class_name,section,subj.getText().toString());
	        	
			for (Chat_setter_getter cn : chat) 
			{
				
				if(cn.getsendorreceive().equals("receive"))
				{
					 flag=1;
					 side=true;
					 chatArrayAdapter.add(new ChatMessage(side,cn.getcontent()));
					 chatArrayAdapter.notifyDataSetChanged();
					 listView.setAdapter(chatArrayAdapter);
				}
				
				if(cn.getsendorreceive().equals("send"))
				 {
					 side=false;					  
					 chatArrayAdapter.add(new ChatMessage(side,cn.getcontent()));
					 chatArrayAdapter.notifyDataSetChanged();
					 listView.setAdapter(chatArrayAdapter);
				 }
	     	}	
			 GcmIntentService.flags=0;	
		 }
		}		
		return true;
	}		
	
	
	private boolean sendChatMessage() 
	{		
		//DatabaseHandler db = new DatabaseHandler(getActivity().getApplicationContext());
		//db.addData(new Chat_setter_getter("send",chatText.getText().toString(),"Home Work",class_name,section,subj.getText().toString())); 
		//db.close();       
		side=false;
		chatArrayAdapter.add(new ChatMessage(side,chatText.getText().toString()));	
		chatArrayAdapter.notifyDataSetChanged();
		listView.setAdapter(chatArrayAdapter);
		String webServiceInfo;
     
	        String class_notice=chatText.getText().toString();
	    
	        try {
	        	httpclient = new DefaultHttpClient();        	
	 		    HttpParams httpParameters = new BasicHttpParams();
	 		    HttpClient client = new DefaultHttpClient(httpParameters); 		    
	 		    httppost = new HttpPost("http://"+LoginActivity.URL+"/schoolmanagement/designs/webservice/GCM.php");	 		    
				nameValuePairs= new ArrayList<NameValuePair>();				
				nameValuePairs.add(new BasicNameValuePair("type","Teacher"));
				nameValuePairs.add(new BasicNameValuePair("message","Home Works~"+chatText.getText().toString()+"~"+class_name+"~"+section+"~"+subj.getText().toString()));				
				nameValuePairs.add(new BasicNameValuePair("class_name",class_name));
				nameValuePairs.add(new BasicNameValuePair("section",section));
				nameValuePairs.add(new BasicNameValuePair("subject",subj.getText().toString()));				
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));				
				HttpResponse response = httpclient.execute(httppost); 
				Log.e("Home Work",nameValuePairs+"");
				HttpEntity entity = response.getEntity();			
				BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
	 		    while ((webServiceInfo = rd.readLine()) != null) {
		 		     Log.d("****Status Log***", "Webservice: " + webServiceInfo);
		 		    } 		    
	 		   } catch (Exception e) {
	 		    e.printStackTrace();
	 		   }	
 		   
        chatText.setText("");
        return true;
    }



	@Override
	public void onClick(View v) 
	{
		if(v.getId()==R.id.views)
		{
			if(subj.getText().toString().trim().equals(""))
			{
				Toast.makeText(getActivity().getApplicationContext(),"Sorry fill the Subject",Toast.LENGTH_LONG).show();				
			}
			else
			{				
				
			try
			{
				 final Calendar c = Calendar.getInstance();
		         int Year = c.get(Calendar.YEAR);
		         int Month = c.get(Calendar.MONTH);
		         int Day = c.get(Calendar.DAY_OF_MONTH);
				
				 // Launch Date Picker Dialog
		         DatePickerDialog dpd1 = new DatePickerDialog(getActivity(),
		                 new DatePickerDialog.OnDateSetListener() {

		                     @Override
		                     public void onDateSet(DatePicker view, int year,int monthOfYear, int dayOfMonth) {
		                         
		                     	 strtday=dayOfMonth;
		                         strtmonth=monthOfYear+1;
		                         strtyear=year;
		                     	
		                         date=strtday+"-"+strtmonth+"-"+strtyear;
		                         Log.e("date",date);
		                         
		                         shared=getActivity().getSharedPreferences("gallery_name",Context.MODE_PRIVATE);
		         	 	        class_name=shared.getString("name",null);
		         	 	        section=shared.getString("sec",null);				
		         				
		         	 	        
		         	 	        try
		         	 	        {
		         	 	        
		         	 	        
		         				httpclient = new DefaultHttpClient();
		         				httppost = new HttpPost("http://"+LoginActivity.URL+"/schoolmanagement/designs/webservice/get_home_work1.php");
		         				nameValuePairs= new ArrayList<NameValuePair>();	
		         				nameValuePairs.add(new BasicNameValuePair("subject",subj.getText().toString().trim()));
		         				nameValuePairs.add(new BasicNameValuePair("date",date));
		         				nameValuePairs.add(new BasicNameValuePair("class_name",class_name));
		         				nameValuePairs.add(new BasicNameValuePair("section",section));
		         				//nameValuePairs.add(new BasicNameValuePair("content",chatText.getText().toString()));
		         				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		         				HttpResponse response = httpclient.execute(httppost); 
		         				HttpEntity entity = response.getEntity();			
		         				
		         				Log.e("send",nameValuePairs+"");
		         				is = entity.getContent();			
		         					    BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
		         					    StringBuilder sb = new StringBuilder();
		         					    while ((line = reader.readLine()) != null)
		         			            {
		         					    	
		         			                sb.append(line + "\n");
		         			               
		         			            }
		         			            is.close();
		         			            result = sb.toString();	        
		         			            Log.e("Result",result);
		         			         JSONObject json_data = new JSONObject(result);
		         				     JSONObject data = json_data.getJSONObject("DATA");		         
		         				     String code = data.getString("CODE");
		         			         String url_message =data.getString("MESSAGE");       
		         			        
		         			        
		         			         if(code.equals("200"))
		         			         {		        	 
		         			        	 chatText.setText(data.getString("HOME_WORK_CONTENT"));		        
		         			        	  
		         			         }
		         			         if(code.equals("500"))
		         			         {
		         			        	 chatText.setText("Sorry No data");		        
		         			         }     
		         			         
		         	 	        }
		         	 	        catch(Exception e)
		         	 	        {
		         	 	        	Log.e("Exception",e+"");
		         	 	        }
		         			    
		                      }
		                     }, Year, Month, Day);
		         
		         dpd1.setTitle("Date of Home Work ");
		         dpd1.show();
				
				
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			}
			
		}
	}	 
	
	
	
	
	
	
	
	public boolean sendChatMessagetodatabase()
	{
		boolean returns=false;
		
		shared = getActivity().getSharedPreferences(SH_UIDS,Context.MODE_PRIVATE);
		uid=(shared.getString(UID,null));
		type=(shared.getString(TYPE,null));
	    phno=(shared.getString(PHNO,null));
        
	    shared=getActivity().getSharedPreferences("gallery_name",Context.MODE_PRIVATE);
        class_name=shared.getString("name",null);
        section=shared.getString("sec",null);
        
		
		try
		{
			httpclient = new DefaultHttpClient();
			String httpid = "http://"+LoginActivity.URL+"/schoolmanagement/designs/webservice/GCM.php";
			httppost = new HttpPost(httpid);
			nameValuePairs= new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("uid",uid));
			nameValuePairs.add(new BasicNameValuePair("type",type));
			nameValuePairs.add(new BasicNameValuePair("phno",phno));
			nameValuePairs.add(new BasicNameValuePair("class_name",class_name));
			nameValuePairs.add(new BasicNameValuePair("section",section));
			nameValuePairs.add(new BasicNameValuePair("message",chatText.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("label","home_work"));
			nameValuePairs.add(new BasicNameValuePair("subject",subj.getText().toString()));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost); 
			HttpEntity entity = response.getEntity();		
			is = entity.getContent();			
				    BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
				    StringBuilder sb = new StringBuilder();
				    while ((line = reader.readLine()) != null)
		            {					    	
		                sb.append(line + "\n");
		               
		            }
		            is.close();
		            result = sb.toString();
		            Log.e("message", result);
		         JSONObject json_data = new JSONObject(result);
			     JSONObject data = json_data.getJSONObject("DATA");		         
			     String code = data.getString("CODE");
			     String messages =data.getString("MESSAGE");         
		         
			    if(code.equals("200"))
		         {
		        	  returns=true;
		         }
		         if(code.equals("500"))
		         {
		        	 returns=false;
		         }        
		               					
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	
	return returns;
	}
}

