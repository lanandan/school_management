package com.gr_infotech.fragments;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.school_management.Head_setting_screen_fragment;
import com.example.school_management.LoginActivity;
import com.example.school_management.R;
import com.example.school_management.SettingsScreenActivity;

public class Subject_fragment extends Fragment 
{
	ListView subjectlist;
	HttpClient httpclient;
	HttpPost httppost;
	ArrayList<NameValuePair> nameValuePairs;
	InputStream is;
	SharedPreferences shared_gallery_name;
	String line,result,message;
	SharedPreferences shared;
	String code;
	String strsubj[];//={"Tamil","English","Maths","Science","Social"};
	
		
	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle args) 
	{
        View view = inflater.inflate(R.layout.subject_fragment,container, false);
        subjectlist=(ListView)view.findViewById(R.id.subject_list);
        
        try
		{
        	shared_gallery_name=getActivity().getSharedPreferences("gallery_name",Context.MODE_PRIVATE);
			String class_name=shared_gallery_name.getString("class", null);
			String section=shared_gallery_name.getString("sec", null);
        	
			httpclient = new DefaultHttpClient();
			String httpid = "http://"+LoginActivity.URL+"/schoolmanagement/designs/webservice/get_subject.php";
			httppost = new HttpPost(httpid);
			nameValuePairs= new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("class_name",class_name));							
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost); 
			HttpEntity entity = response.getEntity();		
			is = entity.getContent();			
				    BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
				    StringBuilder sb = new StringBuilder();
				    while ((line = reader.readLine()) != null)
		            {					    	
		                sb.append(line + "\n");
		               
		            }
		            is.close();
		            result = sb.toString();		            
		         Log.e("Result",result);
		         JSONObject json_data = new JSONObject(result);
			     JSONObject data = json_data.getJSONObject("DATA");		         
			     code = data.getString("CODE");			     
			     message =data.getString("MESSAGE");	
			     
		         if(code.equals("200"))
		         {
		        	 Log.e("Message","entered");
		        	 JSONArray msg = (JSONArray)data.getJSONArray("SUBJECT");
		        	
		        	 
		        	 int i=0;
		        	  Log.e("Message",msg+"");
		        	  strsubj=new String[msg.length()];
		        	  
		        	  for(i=0;i<msg.length();i++)
		        	  {
		        		  Log.e("Message",msg.getString(i).toString());		        		  
		        		  strsubj[i]=msg.getString(i).toString();  
		        	  }
		        	  
		        	  ArrayAdapter adapter = new ArrayAdapter<String>(getActivity().getApplicationContext(),R.layout.list_subject,strsubj);
		        	 subjectlist.setAdapter(adapter);	        	 
		        	  subjectlist.setOnItemClickListener(new OnItemClickListener() 
		        	  {
		        		  
		                  @Override
		                  public void onItemClick(AdapterView<?> parent, View view,int position, long id) 
		                  {
		                   // ListView Clicked item index
		                	  int itemPosition = position;
		                     shared=getActivity().getSharedPreferences("subject_name",Context.MODE_PRIVATE);
				           	 Editor editor = shared.edit();
				             editor.putString("subject",strsubj[itemPosition]);
				             editor.commit();		                   
				           // ListView Clicked item value
		                   String  itemValue = (String) subjectlist.getItemAtPosition(position);	
		                   
		                   
		                   setTitle(subjectlist.getItemAtPosition(position)+"");
		                  Fragment fragment=new Home_Works_Fragment();
		 				  FragmentManager fragmentManager = getFragmentManager();
		 			      fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
		                   // Show Alert 
		                   //Toast.makeText(getActivity().getApplicationContext(),"Position :"+itemPosition+"  ListItem : " +itemValue , Toast.LENGTH_LONG).show();	                 
		                  }
		             }); 
		        	 
		         }
		         if(code.equals("500"))
		         {
		        	Toast.makeText(getActivity().getApplicationContext(),"No subject has been assigned for this class",Toast.LENGTH_LONG).show();
		         }        	              					
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}      
        
        return view;
	}
	
	
	public void setTitle(CharSequence title) {	     
	     
		 ((SettingsScreenActivity) getActivity()).getSupportActionBar().setTitle(title);
		 
	 }	
}
