package com.gr_infotech.fragments;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import com.example.school_management.LoginActivity;
import com.example.school_management.R;
import com.example.school_management.SettingsScreenActivity;
import com.example.school_management.LoginActivity.Login_process;
import com.gr_infotech.utilities.Network_checking;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class Contact_us_Fragment extends Fragment implements OnClickListener 
{	
	ImageButton call,visit_webpage;	
	EditText queries;
	Button send;
	HttpClient httpclient;
	HttpPost httppost;
	ArrayList<NameValuePair> nameValuePairs;
	InputStream is;
	ProgressDialog pd;
	String line,result,message;
	SharedPreferences shared;
	private static final String SH_UIDS = "Tripuid";	
	private static final String UID = "UniqueID";
	private static final String TYPE = "Type";
	private static final String PHNO = "phone";	
	String uid,str_type,phno;
	
	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle args) {
        View view = inflater.inflate(R.layout.contact_us,container, false);        
        call=(ImageButton)view.findViewById(R.id.call_now);
        visit_webpage=(ImageButton)view.findViewById(R.id.web_site);
        call.setOnClickListener(this);
        visit_webpage.setOnClickListener(this);        
        queries=(EditText)view.findViewById(R.id.contact_us_queries);
        send=(Button)view.findViewById(R.id.btn_contact_send);
        send.setOnClickListener(this); 
        shared = getActivity().getSharedPreferences(SH_UIDS,Context.MODE_PRIVATE);
		uid=(shared.getString(UID,null));
		str_type=(shared.getString(TYPE,null));
	    phno=(shared.getString(PHNO,null));	
        return view;
}

	@Override
	public void onClick(View v) 
	{
		if(v.getId()==R.id.call_now)
		{
			    String PhoneNum="9787846453";
				Intent callIntent = new Intent(Intent.ACTION_CALL);
		        callIntent.setData(Uri.parse("tel:"+Uri.encode(PhoneNum.trim())));
		        //callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		        startActivity(callIntent);  
		}
		if (v.getId()==R.id.web_site)
		{
			Fragment fragment = new visit_webpage_fragment();
	  		FragmentManager fragmentManager = getFragmentManager();
	        fragmentManager.beginTransaction().replace(R.id.content_frame,fragment).commit(); 
		}
		
		
		if(v.getId()==R.id.btn_contact_send)
		{
			if(contact_us_Validation())
			{
				if(Network_checking.isNetworkAvailable(getActivity()))
				{
					Login_process login=new Login_process();
					login.execute();				
				}
				else
				{
					showToast("Please check your network connection");
				}
			}
			
		}		
	}
	
	
	private void showToast(String msg)
	{
		Toast.makeText(getActivity().getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
	}	
	
	public boolean contact_us_Validation()
	{
				
			Drawable dr = getResources().getDrawable(R.drawable.star_boxed_full); 
            //add an error icon to yur drawable files
			dr.setBounds(0, 0, dr.getIntrinsicWidth(), dr.getIntrinsicHeight());
			if (queries.getText().toString().equals("".trim()))
			{
			
				if(queries.getText().toString().trim().equalsIgnoreCase(""))
				{
					queries.setError("Queries should be filled",dr);
				}
				
				//showToast("Fill All Feilds");
				return false;
			}
			else
			{
				
				return true;
				
			}
	}
	
	
	
	public class Login_process extends AsyncTask<Void, Void, ArrayList<String>>
	{	
		int flag=0;
		
		
		@Override
		protected ArrayList<String> doInBackground(Void... params) {					
			
			if(checkingvaliduser())
			{
				flag=1;
			}
			else
			{
				flag=2;
			}
			return null;
		}		
		@Override
	    protected void onPreExecute() {
	            // TODO Auto-generated method stub
	            super.onPreExecute();                     
	            pd = new ProgressDialog(getActivity(),R.style.MyTheme1);
				pd.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progressbar_custom));
				pd.setCancelable(false);
				pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
				pd.show();	            
	            //pd = ProgressDialog.show(LoginActivity.this, "Logging in","Please Wait");
	    }		
		@Override
	    protected void onPostExecute(ArrayList<String> contacts) {
	        super.onPostExecute(contacts);
	        pd.cancel();		        
	        if(flag==1)
	        {
	        	Toast.makeText(getActivity().getApplicationContext(),message, Toast.LENGTH_SHORT).show();
	        }
	        if(flag==2)
	        {
	        	Toast.makeText(getActivity().getApplicationContext(),message, Toast.LENGTH_SHORT).show();
	        }	        
	    }
			
		public boolean checkingvaliduser()
		{
			boolean returns = false;
			try
			{
				httpclient = new DefaultHttpClient();
				String httpid = "http://"+LoginActivity.URL+"/schoolmanagement/designs/webservice/Contact_us_queries.php";
				httppost = new HttpPost(httpid);
				nameValuePairs= new ArrayList<NameValuePair>();				
				nameValuePairs.add(new BasicNameValuePair("phno",phno));
				nameValuePairs.add(new BasicNameValuePair("uid",uid));
				nameValuePairs.add(new BasicNameValuePair("type",uid));
				nameValuePairs.add(new BasicNameValuePair("queries",queries.getText().toString()));
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				HttpResponse response = httpclient.execute(httppost); 
				HttpEntity entity = response.getEntity();					
				is = entity.getContent();			
					    BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
					    StringBuilder sb = new StringBuilder();
					    while ((line = reader.readLine()) != null)
			            {					    	
			                sb.append(line + "\n");
			               
			            }
			            is.close();
			            result = sb.toString();
			            Log.e("message", result);
			         JSONObject json_data = new JSONObject(result);
				     JSONObject data = json_data.getJSONObject("DATA");		         
				     String code = data.getString("CODE");
				     message =data.getString("MESSAGE");		         
			         
			         if(code.equals("200"))
			         {
			        	  returns=true;
			         }
			         if(code.equals("500"))
			         {
			        	 returns=false;
			         }        
			         			       					
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			return returns;	
		}		
	}	
}
