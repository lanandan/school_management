package com.gr_infotech.fragments;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.example.school_management.LoginActivity;
import com.example.school_management.R;
import com.gr_infotech.adapter.ChatMessage;
import com.gr_infotech.adapter.CustomAttendanceListAdpater;
import com.gr_infotech.pojo.Chat_setter_getter;
import com.gr_infotech.pojo.DatabaseHandler;

public class Attendance_Fragment extends Fragment implements OnClickListener {

	public static int total_no;
	
	View view;
	int i = 0;
	TableRow row;
	ArrayList<String> stu_name;
	ArrayList<String> stu_no;
	ArrayList<String> nametosendtowebservice;
	ArrayList<String> notosendtowebservice;
	TextView names, nos, txtdate, txttime, txtclassec;
	CheckBox chk, check;
	List<String> na, num;
	Button send;
	ListView attendance;
	ArrayList<TextView> attnd_stud_no;
	ArrayList<TextView> attnd_stud_name;
	ArrayList<CheckBox> attnd_chek;	
	HttpClient httpclient;
	HttpPost httppost;
	ArrayList<NameValuePair> nameValuePairs;
	InputStream is;
	SharedPreferences shared, share_uid;
	String class_name, section, line, result, message;
	SimpleDateFormat day, month, year;
	String strday, strmonth, stryear;
	Calendar cal;
	String date, uid, phonenumber, type;
	CustomAttendanceListAdpater cala;
	static TextView totalNumPresent;
	
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle args) {
		view = inflater.inflate(R.layout.attendance_fragment, container, false);
		send = (Button) view.findViewById(R.id.btnsend);

		attendance = (ListView) view.findViewById(R.id.attnd_tbl_layout);
		
		totalNumPresent = (TextView) view.findViewById(R.id.total_no_present);

		send.setOnClickListener(this);

		attnd_chek = new ArrayList<CheckBox>();
		attnd_stud_no = new ArrayList<TextView>();
		attnd_stud_name = new ArrayList<TextView>();

		cal = Calendar.getInstance(TimeZone.getDefault());
		txtdate = (TextView) view.findViewById(R.id.txtdate);
		txttime = (TextView) view.findViewById(R.id.txttime);
		txtclassec = (TextView) view.findViewById(R.id.class_sec);

		Date time = new Date();
		String strDateFormat = "hh:mm:ss a";
		SimpleDateFormat sdf = new SimpleDateFormat(strDateFormat);
		txttime.setText(sdf.format(time));

		day = new SimpleDateFormat("dd", Locale.getDefault());
		month = new SimpleDateFormat("MM", Locale.getDefault());
		year = new SimpleDateFormat("yyyy", Locale.getDefault());
		strday = day.format(cal.getTime());
		strmonth = month.format(cal.getTime());
		stryear = year.format(cal.getTime());
		date = strday + " - " + strmonth + " - " + stryear;

		txtdate.setText(date);

		// Data from web service to get the student name and number
		shared = getActivity().getSharedPreferences("gallery_name",	Context.MODE_PRIVATE);
		class_name = shared.getString("name", null);
		section = shared.getString("sec", null);
		txtclassec.setText(class_name + " " + section);

		share_uid = getActivity().getSharedPreferences("Tripuid",Context.MODE_PRIVATE);
		uid = share_uid.getString("UniqueID", null);
		phonenumber = share_uid.getString("phone", null);
		type = share_uid.getString("Type", null);

		if (get_attendance(class_name, section)) {
			// Toast.makeText(getActivity().getApplicationContext(),message+"",
			// Toast.LENGTH_LONG).show();
		} else {
			Toast.makeText(getActivity().getApplicationContext(), message + "",Toast.LENGTH_LONG).show();
		}
		
		
		attendance.setAdapter(cala);

		totalNumSelected();
		return view;
	}
	
	public static void totalNumSelectedUpdate(){
			totalNumPresent.setText("Total Number Present is " +total_no);	
	}
	
	
	public void totalNumSelected(){
			total_no = attnd_chek.size();
			totalNumPresent.setText("Total Number Present is " +total_no);	
	}

	public boolean get_attendance(String class_name, String section_name) {
		boolean returns = false;
		String name = "NAME";
		String no = "NO";
		try {
			httpclient = new DefaultHttpClient();
			httppost = new HttpPost("http://192.168.1.117/schoolmanagement/designs/webservice/get_attendance_data.php");
			nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("class_name", class_name));
			nameValuePairs.add(new BasicNameValuePair("section", section_name));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
			Log.e("message", result);
			JSONObject json_data = new JSONObject(result);
			JSONObject data = json_data.getJSONObject("DATA");
			String code = data.getString("CODE");
			message = data.getString("MESSAGE");

			if (code.equals("200")) {
				
				i = 0;
				JSONArray studentname = (JSONArray)data.getJSONArray("STUDENT NAME");
				JSONArray studentno = (JSONArray)data.getJSONArray("STUDENT NO");
							
				attnd_stud_name.clear();
				attnd_stud_no.clear();
				attnd_chek.clear();
				

				while (i < studentname.length()) 
				{
					names = new TextView(getActivity().getApplicationContext());
					attnd_stud_name.add(names);
					attnd_stud_name.get(i).setText(studentname.getString(i));
					nos = new TextView(getActivity().getApplicationContext());
					attnd_stud_no.add(nos);
					attnd_stud_no.get(i).setText(studentno.getString(i));
					chk = new CheckBox(getActivity().getApplicationContext());
					chk.setButtonDrawable(R.drawable.checboxcustom);
					attnd_chek.add(chk);
					i++;
				}
				cala = new CustomAttendanceListAdpater(this.getActivity(),R.layout.new_attendance_list_frag, attnd_stud_no,attnd_stud_name, attnd_chek);
				returns = true;
			}
			if (code.equals("500")) {
				returns = false;
			}

			if (code.equals("501")) {
				returns = false;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return returns;
	}

	@Override
	public void onClick(View v) {

		if (v.getId() == R.id.btnsend) {

			openAlert(v);

			}
	}

	private void openAlert(View v) {
		// TODO Auto-generated method stub

		AlertDialog.Builder alrt = new AlertDialog.Builder(getActivity());

		alrt.setTitle("Sending decision");
		alrt.setMessage("Are you sure to Send?");
		
		
		alrt.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {

				if (send_attendance(class_name, section)) {
					dialog.dismiss();
					Toast.makeText(getActivity().getApplicationContext(),
							"" + message, Toast.LENGTH_LONG).show();
				} else {
					dialog.dismiss();
					Toast.makeText(getActivity().getApplicationContext(),
							"" + message, Toast.LENGTH_LONG).show();
				}

			}

		});

		alrt.setNegativeButton("No", new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int id) {

				dialog.dismiss();

			}

		});

		AlertDialog alertDialog = alrt.create();
		// show alert
		alertDialog.show();

	}

	public boolean send_attendance(String class_name, String section) {
		nametosendtowebservice = new ArrayList<String>();
		notosendtowebservice = new ArrayList<String>();
		
		boolean returns = false;
		String name = "NAME";
		String no = "NO";

		try {
			i = 0;
			int length = attnd_chek.size();
			Log.e("Sizesss", length + "");
			
			while (i < length) {
				
				
				Log.e("Attendance checking",CustomAttendanceListAdpater.checkingstatus[i]+"");
				
				if (CustomAttendanceListAdpater.checkingstatus[i] == true) {
					Log.e("Class name true", attnd_stud_name.get(i).getText().toString());
					Log.e("Class no true", attnd_stud_no.get(i).getText().toString());					
					
					
				} else {
					Log.e("Name to Web service in attendance false false",attnd_stud_name.get(i).getText().toString());
					nametosendtowebservice.add(attnd_stud_name.get(i).getText().toString());
					notosendtowebservice.add(attnd_stud_no.get(i).getText().toString());
				}
				i++;
			}
			httpclient = new DefaultHttpClient();
			httppost = new HttpPost("http://"+LoginActivity.URL+"/schoolmanagement/designs/webservice/send_attendance_data.php");
			nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("class_name", class_name));
			nameValuePairs.add(new BasicNameValuePair("section", section));
			nameValuePairs.add(new BasicNameValuePair("date", txtdate.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("time", txttime.getText().toString()));
			nameValuePairs.add(new BasicNameValuePair("day", strday));
			nameValuePairs.add(new BasicNameValuePair("month", strmonth));
			nameValuePairs.add(new BasicNameValuePair("year", stryear));
			nameValuePairs.add(new BasicNameValuePair("uid", uid));
			nameValuePairs.add(new BasicNameValuePair("phonenumber",phonenumber));

			String[] nameArr = new String[nametosendtowebservice.size()];
			nameArr = nametosendtowebservice.toArray(nameArr);
			String strnamearr = Arrays.toString(nameArr);
			String[] noArr = new String[notosendtowebservice.size()];
			noArr = notosendtowebservice.toArray(noArr);
			String strnoarr = Arrays.toString(noArr);

			strnoarr = strnoarr.replace("[", "").replace("]", "");
			strnamearr = strnamearr.replace("[", "").replace("]", "");

			Log.e("String name", strnamearr);
			Log.e("String number", strnoarr);

			nameValuePairs.add(new BasicNameValuePair("stud_name", strnamearr));
			nameValuePairs.add(new BasicNameValuePair("stud_no", strnoarr));

			Log.e("size", nametosendtowebservice.size() + "");

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			Log.e("ul format", nameValuePairs + "");

			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
			Log.e("message", result);

			JSONObject json_data = new JSONObject(result);
			JSONObject data = json_data.getJSONObject("DATA");
			String code = data.getString("CODE");
			message = data.getString("MESSAGE");
			if (code.equals("200")) 
			{
				
			Log.e("Chat message","Chat Message");	
				sendChatMessage();
				returns = true;
			}
			if (code.equals("500")) {

				returns = false;
			}

			if (code.equals("501")) {
				returns = false;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return returns;
	}
	
	
	
	
	private boolean sendChatMessage() 
	{		
		Log.e("Sending","Sending");
		//DatabaseHandler db = new DatabaseHandler(getActivity().getApplicationContext());
		//db.addData(new Chat_setter_getter("send","Your Children is absent","Send Class Notice",class_name,section,"SendClassNotices")); 
		//db.close();       
		String webServiceInfo;          
        
		try {
	        	httpclient = new DefaultHttpClient();        	
	 		    HttpParams httpParameters = new BasicHttpParams();
	 		    HttpClient client = new DefaultHttpClient(httpParameters); 		    
	 		    httppost = new HttpPost("http://"+LoginActivity.URL+"/schoolmanagement/designs/webservice/GCM.php");
				nameValuePairs= new ArrayList<NameValuePair>();				
				nameValuePairs.add(new BasicNameValuePair("type","absent"));
				nameValuePairs.add(new BasicNameValuePair("message","Notification~"+"Your Child is absent"+"~"+class_name+"~"+section+"~"+"SendClassNotices"));								
				nameValuePairs.add(new BasicNameValuePair("class_name",class_name));
				nameValuePairs.add(new BasicNameValuePair("section",section));
				
				String[] nameArr = new String[nametosendtowebservice.size()];
				nameArr = nametosendtowebservice.toArray(nameArr);
				String strnamearr = Arrays.toString(nameArr);
				String[] noArr = new String[notosendtowebservice.size()];
				noArr = notosendtowebservice.toArray(noArr);
				String strnoarr = Arrays.toString(noArr);
				
				strnoarr = strnoarr.replace("[", "").replace("]", "");
				strnamearr = strnamearr.replace("[", "").replace("]", "");
				
				nameValuePairs.add(new BasicNameValuePair("stud_name", strnamearr));
				nameValuePairs.add(new BasicNameValuePair("stud_no", strnoarr));
				
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));				
				HttpResponse response = httpclient.execute(httppost); 
				HttpEntity entity = response.getEntity();			
				BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
	 		    while ((webServiceInfo = rd.readLine()) != null) {
		 		     Log.d("****Status Log***", "Webservice: " + webServiceInfo);
		 		    } 		    
	 		   } catch (Exception e) {
	 		    e.printStackTrace();
	 		   }        
	        
	     
        return true;
    }	
}

