package com.gr_infotech.fragments;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import com.example.school_management.LoginActivity;
import com.example.school_management.R;
import com.gr_infotech.adapter.MyExpandableAdapter;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

public class Students_Fragment extends Fragment 
{
	Calendar cal;
	 	ExpandableListAdapter listAdapter;
	    ExpandableListView expListView;
	    List<String> listDataHeader;
	    HashMap<String, List<String>> listDataChild;
	    HttpClient httpclient;
		HttpPost httppost;
		ArrayList<NameValuePair> nameValuePairs;
		InputStream is;
		Button addstudent;
		SharedPreferences sharedpreferences,shared;
		String uid,subtype;
		JSONObject parent_email;
		String class_name,section;
		TextView names,nos,txtdate,txttime,txtstudname;
		SimpleDateFormat day,month,year;
		String strday,strmonth,stryear,date;
	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle args) 
	{
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);       
		View view = inflater.inflate(R.layout.student_information, container, false);         	        
         // get the listview
        expListView = (ExpandableListView)view.findViewById(R.id.expandable_list_view);
        // preparing list data              
        shared=getActivity().getSharedPreferences("gallery_name",Context.MODE_PRIVATE);
        class_name=shared.getString("name",null);
        section=shared.getString("sec",null);
        Log.e("Class", class_name+" : "+section);
        
        
        String line;
    	String result="";
    	String name="";
    	String email="";
    	String email_content_extendible_list_view="";
    	String json_lbl_name="",json_lbl_email="";        
    	String jsonstud_id="STUDENTID";
    	String jsonstud_name="STUDENTNAME";
    	String jsonstud_phno="STUDENTPHNO";
    	String jsonstud_email="STUDENTEMAIL";
    	String jsonstud_class="STUDENTCLASS";
    	String jsonstud_sec="STUDENTSECTION";
    	String jsonstud_id_label="STUDID";
    	String jsonstud_name_label="STUDNAME";
    	String jsonstud_phone_label="STUDPHNO";
    	String jsonstud_email_label="STUDEMAIL";
    	String jsonstud_class_label="STUDCLASS";
    	String jsonstud_sec_label="STUDSEC";
    	
    	
    	cal= Calendar.getInstance(TimeZone.getDefault());
		
		
		 txtdate=(TextView)view.findViewById(R.id.txtdate);
	        txttime=(TextView)view.findViewById(R.id.txttime);
	        txtstudname=(TextView)view.findViewById(R.id.stud_name);
	        
	        
	         Date time = new Date();
	         String strDateFormat = "hh:mm:ss a";
	         SimpleDateFormat sdf = new SimpleDateFormat(strDateFormat);        
	         txttime.setText(sdf.format(time));         
	        
	        day = new SimpleDateFormat("dd", Locale.getDefault());
			month = new SimpleDateFormat("MMM", Locale.getDefault());
			year = new SimpleDateFormat("yyyy", Locale.getDefault());
			strday = day.format(cal.getTime());
			strmonth = month.format(cal.getTime());
			stryear = year.format(cal.getTime());
			date=strday+" / "+strmonth+" / "+stryear;
	        
	        txtdate.setText(date);
	        txtstudname.setText(class_name+" "+section);	
		
    	
    	
    	
    	try
        {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();    	
    	httpclient = new DefaultHttpClient();
    	httppost = new HttpPost("http://"+LoginActivity.URL+"/schoolmanagement/designs/webservice/student_info.php");
		nameValuePairs= new ArrayList<NameValuePair>();
		nameValuePairs.add(new BasicNameValuePair("class_name",class_name));	
		nameValuePairs.add(new BasicNameValuePair("section",section));
		Log.e("Class Name",class_name);
		Log.e("Section",section);
		httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		HttpResponse response = httpclient.execute(httppost);
		HttpEntity entity = response.getEntity();					
		is = entity.getContent();
		BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
	    StringBuilder sb = new StringBuilder();				    
	    while ((line = reader.readLine()) != null)
	            {
	                sb.append(line + "\n");
	            }
	            is.close();
	            result = sb.toString();			
	            
	            
	    		JSONObject json_data = new JSONObject(result);
	    	    Log.e("Json Data", json_data+"");	     
	            JSONObject data = json_data.getJSONObject("DATA"); // get data object
	            String code = data.getString("CODE"); // get the name from data.
	            String message =data.getString("MESSAGE");
	            
	            JSONArray json_id=data.getJSONArray("STUDENTID");
	            JSONArray json_name=data.getJSONArray("STUDENTNAME");
	            JSONArray json_phno=data.getJSONArray("STUDENTPHNO");
	            JSONArray json_email=data.getJSONArray("STUDENTEMAIL");
	            JSONArray json_class=data.getJSONArray("STUDENTCLASS");
	            JSONArray json_sec=data.getJSONArray("STUDENTSECTION");
	         	
	            int i=0;
	            for(i=0;i<json_name.length();i++)
	            {
	            	listDataHeader.add(json_name.getString(i));
	            	List<String> student_information = new ArrayList<String>();	            	
	            	student_information.add("No-"+json_id.getString(i));	            	
	            	student_information.add("Email-"+json_email.getString(i));
	            	student_information.add("Phno-"+json_phno.getString(i));
	            	student_information.add("Class-"+json_class.getString(i));
	            	student_information.add("Section-"+json_sec.getString(i));
	            	listDataChild.put(listDataHeader.get(i),student_information);
	            }                 
        }
        catch(Exception e)
        {
        	Log.e("Exception",e+"");
        }
	   listAdapter = new MyExpandableAdapter(getActivity().getApplicationContext(), listDataHeader, listDataChild);
        // setting list adapter
        expListView.setAdapter(listAdapter);
        return view;
    }	

}
