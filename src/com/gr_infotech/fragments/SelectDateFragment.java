package com.gr_infotech.fragments;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;


public class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Calendar c = Calendar.getInstance();

        DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, c.get(Calendar.YEAR),
                c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));

        dialog.getDatePicker().setSpinnersShown(true);

        // hiding calendarview and daySpinner in datePicker
            dialog.getDatePicker().setCalendarViewShown(false);

            LinearLayout pickerParentLayout = (LinearLayout) dialog.getDatePicker().getChildAt(0);

            LinearLayout pickerSpinnersHolder = (LinearLayout) pickerParentLayout.getChildAt(0);

            pickerSpinnersHolder.getChildAt(0).setVisibility(View.GONE);


        dialog.setTitle("Pick a date");
        return dialog;
    }

    public void onDateSet(DatePicker view, int yy, int mm, int dd) {

        Calendar result = Calendar.getInstance();

        result.set(Calendar.YEAR, yy);
        result.set(Calendar.MONDAY, mm);
        result.set(Calendar.DAY_OF_MONTH, dd);

       /* if (currentWeek != -1) {
            updateDateWeek(result.getTimeInMillis());
        } else {
            updateDate(result.getTimeInMillis());
        }*/
    }

	
}