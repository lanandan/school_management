package com.gr_infotech.fragments;
import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import com.example.school_management.R;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class visit_webpage_fragment extends Fragment
{
	WebView webView;
	String url="http://www.grinfotech.com";
	
	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle args) {
        View view = inflater.inflate(R.layout.visit_website,container, false);
        webView = (WebView)view.findViewById(R.id.website);
        webView.setWebViewClient(new MyBrowser());
        
        return view;
}
	
	
	 public void open(View view){
	      String url = "http://www.grinfotech.com";
	      webView.getSettings().setLoadsImagesAutomatically(true);
	      webView.getSettings().setJavaScriptEnabled(true);
	      webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
	      webView.loadUrl(url);

	   }
	   private class MyBrowser extends WebViewClient {
	      @Override
	      public boolean shouldOverrideUrlLoading(WebView view, String url) {
	         view.loadUrl(url);
	         return true;
	      }
	   }
/*
	   Usernameadmin
	   Password52ef341a24
	   Note that Username and Password carefully! It is a random password that was generated just for you.
*/
}
