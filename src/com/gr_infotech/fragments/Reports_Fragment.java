package com.gr_infotech.fragments;

import com.example.school_management.R;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class Reports_Fragment extends Fragment {
	Button attendence,attitude,progress;
	android.app.FragmentManager fm;
	public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle args) {
        View view = inflater.inflate(R.layout.reports_fragment,container, false);     
        attendence=(Button)view.findViewById(R.id.button_attendence);
        attitude=(Button)view.findViewById(R.id.btnattitude);
        progress=(Button)view.findViewById(R.id.btn_progress);
        attendence.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				fm=getFragmentManager();
				FragmentTransaction ft=fm.beginTransaction();
				Repo_AttendenceFragment attendence=new Repo_AttendenceFragment();
				ft.replace(R.id.addedframe,attendence);
				ft.commit();
				
			}
		});
        attitude.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				fm=getFragmentManager();
				FragmentTransaction ft=fm.beginTransaction();
				Repo_AttitudeFragment ra=new Repo_AttitudeFragment();
				ft.replace(R.id.addedframe,ra);
				ft.commit();
			}
		});
        progress.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				fm=getFragmentManager();
				FragmentTransaction ft=fm.beginTransaction();
			    Repo_ProgressFragment rp=new Repo_ProgressFragment();			   
			    ft.replace(R.id.addedframe,rp);
				ft.commit();
			}
		});
   
        if(fm==null)
        {
        android.app.FragmentManager fm=getFragmentManager();
		FragmentTransaction ft=fm.beginTransaction();
		Repo_AttendenceFragment attendence=new Repo_AttendenceFragment();
		ft.add(R.id.addedframe,attendence);
		ft.commit();
        }
        return view;        
	}
}


