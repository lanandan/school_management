package com.gr_infotech.fragments;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Fragment;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.school_management.LoginActivity;
import com.example.school_management.R;
import com.example.school_management.SettingsScreenActivity;
import com.gr_infotech.adapter.CustomAttendanceListAdpater;


public class Show_ContactsList_Fragment extends Fragment implements OnClickListener 
{

	ArrayList<String> nametosendtowebservice,notosendtowebservice;
	ArrayList<String> contact_name,contact_no,contactList;
	
	
	ArrayList<CheckBox> cb,check;
	ListView lst;
	ImageButton btSend;
	String n, pn;
	TextView tv1;
	CheckBox chb1;
	Context context;
	String arr[];
	int a;
	EditText group_name;
	public boolean checkingstatus[];
	HttpClient httpclient;
	HttpPost httppost;
	ArrayList<NameValuePair> nameValuePairs;
	InputStream is;
	String class_name,section,line,result,message,uid,phonenumber,type;
	SharedPreferences shared,share_uid;
	
	
	
	
	
	public Show_ContactsList_Fragment(){
		
	}
	
	public Show_ContactsList_Fragment(ArrayList<CheckBox> cb){
		cb = new ArrayList<CheckBox>();
		this.cb=cb;
		this.cb.addAll(cb);
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle args) 
	{
		View view = inflater.inflate(R.layout.contact_list_fragment, container,false);
		lst = (ListView) view.findViewById(R.id.cnt_lst);
		btSend = (ImageButton) view.findViewById(R.id.bt_send_json);
		btSend.setOnClickListener(this);
		group_name=(EditText)view.findViewById(R.id.group_name);
		contact_name = new ArrayList<String>();
		contact_no=new ArrayList<String>();
		nametosendtowebservice=new ArrayList<String>();
		notosendtowebservice=new ArrayList<String>();
		String Groupname;
		check=new ArrayList<CheckBox>();
		
		shared=getActivity().getSharedPreferences("gallery_name",Context.MODE_PRIVATE);
	    class_name=shared.getString("name",null);
	    section=shared.getString("sec",null);        
	    
	    
	    share_uid=getActivity().getSharedPreferences("Tripuid",Context.MODE_PRIVATE);
	  	uid=share_uid.getString("UniqueID",null);
	  	phonenumber=share_uid.getString("phone",null);
	  	type=share_uid.getString("Type",null);
		
		
		
		
		
		

		try {
			fetchContacts();
			GroupAdapter adap = new GroupAdapter(this.getActivity(),R.layout.row_for_grouping_contacts, contact_name,contact_no,check);
			lst.setAdapter(adap);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return view;
	}

	public class Person {
		String myName = "";
		String myNumber = "";

		public String getName() {
			return myName;
		}

		public void setName(String name) {
			myName = name;
		}

		public String getPhoneNum() {
			return myNumber;
		}

		public void setPhoneNum(String number) {
			myNumber = number;
		}
	}

	public void fetchContacts() 
	{
		contactList = new ArrayList<String>();
		Person p = new Person();
		String phoneNumber = null;
		Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
		String _ID = ContactsContract.Contacts._ID;
		String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
		String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;
		Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
		String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
		String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
		ContentResolver contentResolver = getActivity().getContentResolver();
		Cursor cursor = contentResolver.query(CONTENT_URI, null, null, null,null);


		if (cursor.getCount() > 0) {

			while (cursor.moveToNext()) 
			{
				String contact_id = cursor.getString(cursor.getColumnIndex(_ID));
				String name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));
				int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER)));
				if (hasPhoneNumber > 0) 
				{
					p.setName(name);
					n = p.getName();
					Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?",new String[] { contact_id }, null);
					while (phoneCursor.moveToNext()) 
					{
						contact_name.add(name);
						phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
						contact_no.add(phoneNumber);
						p.setPhoneNum(phoneNumber);
						pn = p.getPhoneNum();						
					}
					phoneCursor.close();

				}

			}

		}
	}

	
	public class ViewHolder{
		TextView txtname,txtno;
		CheckBox check;
	}
	
	public class GroupAdapter extends BaseAdapter {

		Context cxt;
		int resourc;
		ArrayList<String> contact_name,contact_no;		
		ArrayList<CheckBox> CB;
		Show_ContactsList_Fragment scf;
		
		int pos;

		public GroupAdapter(Context context, int res,ArrayList<String> contact_name,ArrayList<String> contact_no, ArrayList<CheckBox> cb) 
		{
			cxt = context;
			resourc = res;
			this.contact_name=contact_name;
			this.contact_no=contact_no;			
			CB = cb;
			checkingstatus=new boolean[contact_name.size()];
			int i=0;
			while(i<contact_name.size())
			{
				checkingstatus[i]=false;
			i++;
			}
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return contact_name.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return contact_name.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return contact_name.indexOf(getItem(position));
		}

		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
		final ViewHolder holder;
			
			if(convertView==null)
			{
			    LayoutInflater inflater = (LayoutInflater) cxt.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				convertView = inflater.inflate(resourc,null,false);
				holder = new ViewHolder();			
				holder.txtname = (TextView) convertView.findViewById(R.id.contact_name);
				holder.txtno = (TextView) convertView.findViewById(R.id.contact_no);
				holder.check = (CheckBox) convertView.findViewById(R.id.chk_box);
				convertView.setTag(holder);
			}
			else
			{
				holder=(ViewHolder)convertView.getTag();
			}
			
			holder.txtname.setText(contact_name.get(position));
			holder.txtno.setText(contact_no.get(position));
			holder.check.setButtonDrawable(R.drawable.checboxcustom);
			holder.check.setChecked(false);
			holder.check.setOnCheckedChangeListener(
	                new CompoundButton.OnCheckedChangeListener() {
	                	@Override
	                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
	                		holder.check.setChecked(isChecked);
	                		int positions=position;
	                		checkingstatus[position]=isChecked;                		                		
	                    }
	                });
			
			
			return convertView;
			
	
		}

	}

	@Override
	public void onClick(View v) 
	{
	
		if(v.getId()==R.id.bt_send_json)
		{
			
			if(group_name.getText().equals(""))
			{
			Toast.makeText(getActivity().getApplicationContext(),"group name has not been Entered",Toast.LENGTH_LONG).show();	
			
		   }
			else
			{
			
				int i=0;
				while(i<contact_name.size())
				{				
					if(checkingstatus[i]==true)
					{
						nametosendtowebservice.add(contact_name.get(i));
						notosendtowebservice.add(contact_no.get(i));
					}					
					i++;
				}
				
				
				httpclient = new DefaultHttpClient();
				httppost = new HttpPost("http://"+LoginActivity.URL+"/schoolmanagement/designs/webservice/create_group.php");
				nameValuePairs= new ArrayList<NameValuePair>();
				String[] nameArr = new String[nametosendtowebservice.size()];
				nameArr = nametosendtowebservice.toArray(nameArr);
				String strnamearr = Arrays.toString(nameArr);
				String[] noArr = new String[notosendtowebservice.size()];
				noArr = notosendtowebservice.toArray(noArr);
				String strnoarr = Arrays.toString(noArr);			
				strnoarr = strnoarr.replace("[", "").replace("]", "");
				strnamearr=strnamearr.replace("[", "").replace("]", "");
				Log.e("String name",strnamearr);
				Log.e("String number",strnoarr);
				
				nameValuePairs.add(new BasicNameValuePair("uid",uid));
				nameValuePairs.add(new BasicNameValuePair("phone",phonenumber));
	            nameValuePairs.add(new BasicNameValuePair("type",type));
	            
				nameValuePairs.add(new BasicNameValuePair("group_name",group_name.getText().toString()));
				nameValuePairs.add(new BasicNameValuePair("contact_name",strnamearr));
	            nameValuePairs.add(new BasicNameValuePair("contact_no",strnoarr));
							
				Log.e("size",nametosendtowebservice.size()+"");			
				
			try
			{
				
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				Log.e("ul format",nameValuePairs+"");
				
				HttpResponse response = httpclient.execute(httppost); 
				HttpEntity entity = response.getEntity();			
				is = entity.getContent();			
					    BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
					    StringBuilder sb = new StringBuilder();
					    while ((line = reader.readLine()) != null)
			            {				    	
			                sb.append(line + "\n");		               
			            }
			            is.close();
			            result = sb.toString();		           
			            Log.e("message", result);            
			           
			         JSONObject json_data = new JSONObject(result);
				     JSONObject data = json_data.getJSONObject("DATA");		         
				     String code = data.getString("CODE");
				     message =data.getString("MESSAGE");		         
			         if(code.equals("200"))
			         {		        	 
			            Toast.makeText(getActivity().getApplicationContext(),"Group Created Succesfully",Toast.LENGTH_LONG).show();
			         }
			         if(code.equals("500"))
			         {	
			        	 Toast.makeText(getActivity().getApplicationContext(),"Problem in creating group",Toast.LENGTH_LONG).show();			       
			         }  
			}
			catch(Exception e)
			{
				Log.e("Exception",e+"");
			}
		
		
		
	}

}
}
}
