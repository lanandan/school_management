package com.example.school_management;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import com.example.school_management.LoginActivity.Login_process;
import com.gr_infotech.utilities.Network_checking;
import android.support.v7.app.ActionBarActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Forgot_pwd_activity extends ActionBarActivity implements OnClickListener
{

	String[] question={"Security Question","what is your school name?","What is your pet’s name?","What is your favorite game?","What month were you born?","What is your mother’s birthday?"};
	ArrayAdapter<String> adapter_question;
	Spinner spin_question;
	SharedPreferences shared;
	String phoneno,types,uid;
	Button forget_pwd;
	String security_question,sec_answer;
	EditText edt_sec_answer,forgot_type;	
	HttpClient httpclient;
	HttpPost httppost;
	ArrayList<NameValuePair> nameValuePairs;
	InputStream is;
	String line,result,type,pwdfrom_service;
	TextView password,login;
	String message,childorclass;
	Typeface nobileMedium, nobileRegular, nobileBoldItalic, nobileMediumItalic;
	ProgressDialog pd;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_forgot_pwd_activity);		
		getSupportActionBar().hide();		
		
		shared = getSharedPreferences("Mobile_No", MODE_PRIVATE);
		phoneno = shared.getString("Phoneno", null);
		types=shared.getString("type", null);
		uid=shared.getString("uid",null);
		forgot_type=(EditText)findViewById(R.id.forgot_type);
		login=(TextView)findViewById(R.id.txt_Login);
		login.setOnClickListener(this);
		Log.e("session",phoneno+","+types+","+uid);
		
		nobileMedium = Typeface.createFromAsset(getAssets(), "fonts/Nobile-Medium.ttf");
		nobileRegular = Typeface.createFromAsset(getAssets(), "fonts/Nobile-Regular.ttf");
		nobileBoldItalic = Typeface.createFromAsset(getAssets(), "fonts/Nobile-BoldItalic.ttf");
		nobileMediumItalic = Typeface.createFromAsset(getAssets(), "fonts/Nobile-MediumItalic.ttf");
		
		
		password=(TextView)findViewById(R.id.get_pwd);
		spin_question=(Spinner)findViewById(R.id.forgot_secquestion);
		adapter_question = new ArrayAdapter(this,android.R.layout.simple_spinner_item,question);
		adapter_question.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spin_question.setAdapter(adapter_question);		
		edt_sec_answer=(EditText)findViewById(R.id.forgot_pass_answer);		
		
		forget_pwd=(Button)findViewById(R.id.get_forget_pwd);
		forget_pwd.setOnClickListener(this);
		
		
		forget_pwd.setTypeface(nobileBoldItalic);		
		edt_sec_answer.setTypeface(nobileRegular);
		forgot_type.setTypeface(nobileRegular);
		
		
		
		
		if(types.equals("Teacher"))
		{
			forgot_type.setHint("Enter one class name");
		}
		if(types.equals("Parent"))
		{
		
			forgot_type.setHint("Enter one child name");
		}
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.forgot_pwd_activity, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onBackPressed() 
	{
		//onDestroy();
		//moveTaskToBack(true);
		Intent i=new Intent(getApplicationContext(),LoginActivity.class);	          	
		startActivity(i);	
		overridePendingTransition(R.anim.right_in, R.anim.left_out);
		
	}
	
	@Override
	public void onClick(View v) 
	{
		
		if(v.getId()==R.id.txt_Login)
		{		
			Intent i=new Intent(getApplicationContext(),LoginActivity.class);	          	
			startActivity(i);	
			overridePendingTransition(R.anim.right_in, R.anim.left_out);
		}
		
		if(v.getId()==R.id.get_forget_pwd)
		{
			
			
				Log.e("Entered","Entered into Teacher");
				if(forgot_pwd_validation())
				{					
					Log.e("Entered","Entered into Teacher step 2");
					if(Network_checking.isNetworkAvailable(Forgot_pwd_activity.this))
					{
						forgotpwd_process forgot=new forgotpwd_process();
						forgot.execute();				
					}
					else
					{
						showToast("Please check your network connection");	
					}					
				}
			
			
				
		}
	}
	
	private void showToast(String msg)
	{
		Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
	}	
	
	public boolean forgot_pwd_validation()
	{
		
		security_question=spin_question.getSelectedItem().toString();
		sec_answer=edt_sec_answer.getText().toString();		
		childorclass=forgot_type.getText().toString();
			
		Drawable dr = getResources().getDrawable(R.drawable.star_boxed_full); 
        dr.setBounds(0, 0, dr.getIntrinsicWidth(), dr.getIntrinsicHeight());
		
			if (security_question.equals("Security Question".trim())||sec_answer.equals("".trim())||childorclass.equals("".trim()))				
			{
				
				if(security_question.equals("Security Question".trim()))
				{
					((TextView)spin_question.getChildAt(0)).setError("Security Question",dr);
						
				}
				if(edt_sec_answer.getText().toString().trim().equalsIgnoreCase(""))
				{
					edt_sec_answer.setText("");
					edt_sec_answer.setError("Answer should be filled",dr);
				}
				if(forgot_type.getText().toString().trim().equalsIgnoreCase(""))
				{
					edt_sec_answer.setText("");
					forgot_type.setError("need to be filled",dr);
				}
				return false;
			}			
			else
			{
				return true;
			}
		
	}
	

	
	
	public class forgotpwd_process extends AsyncTask<Void, Void, ArrayList<String>>
	{	
		int flag=0;
		
		
		@Override
		protected ArrayList<String> doInBackground(Void... params) {					
			
			if(types.equals("Teacher"))
			{			
			if(checking_type_teacher_user())
			{
				flag=1;
			}
			else
			{
				flag=2;
			}
			}
		
			
			if(types.equals("Parent"))
			{			
			if(checking_type_parent_user())
			{
				flag=1;
			}
			else
			{
				flag=2;
			}
			
			
			}
			return null;
		}		
		@Override
	    protected void onPreExecute() {
	            // TODO Auto-generated method stub
	            super.onPreExecute();                     
	            pd = new ProgressDialog(Forgot_pwd_activity.this,R.style.MyTheme1);
				pd.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progressbar_custom));
				pd.setCancelable(false);
				pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
				pd.show();	            
	            //pd = ProgressDialog.show(LoginActivity.this, "Logging in","Please Wait");
	    }		
		@Override
	    protected void onPostExecute(ArrayList<String> contacts) {
	        super.onPostExecute(contacts);
	        pd.cancel();		        
	        if(flag==1)
	        {
	        	password.setText(pwdfrom_service);	
	        }
	        if(flag==2)
	        {
	        
	        	Toast.makeText(getApplicationContext(),"Not Valid User", Toast.LENGTH_SHORT).show();
	        }	        
	    }
	
		
		public boolean checking_type_parent_user()
		{
			boolean returns=false;
			try
			{
				httpclient = new DefaultHttpClient();
				httppost = new HttpPost("http://"+LoginActivity.URL+"/schoolmanagement/designs/webservice/checking_pwd_parent.php");
				nameValuePairs= new ArrayList<NameValuePair>();		
				nameValuePairs.add(new BasicNameValuePair("phoneno",phoneno));
				nameValuePairs.add(new BasicNameValuePair("uid",uid));
				nameValuePairs.add(new BasicNameValuePair("type",type));
				nameValuePairs.add(new BasicNameValuePair("child",forgot_type.getText().toString()));
				nameValuePairs.add(new BasicNameValuePair("sec_question",spin_question.getSelectedItem().toString()));
				nameValuePairs.add(new BasicNameValuePair("sec_answer",edt_sec_answer.getText().toString().trim()));				
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				HttpResponse response = httpclient.execute(httppost); 
				HttpEntity entity = response.getEntity();			
				
				is = entity.getContent();			
					    BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
					    StringBuilder sb = new StringBuilder();
					    while ((line = reader.readLine()) != null)
			            {
					    	
			                sb.append(line + "\n");
			               
			            }
			            is.close();
			            result = sb.toString();	            
			         JSONObject json_data = new JSONObject(result);
				     JSONObject data = json_data.getJSONObject("DATA");		         
				     String code = data.getString("CODE");
				     message=data.getString("MESSAGE");
				     pwdfrom_service=data.getString("password");	 
			         if(code.equals("200"))
			         {
			        	  returns=true;
			         }
			         if(code.equals("500"))
			         {
			        	 returns=false;
			         }        
			         		       					
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			return returns;	
	}	
		
		
		public boolean checking_type_teacher_user()
		{
			boolean returns=false;
			try
			{
				httpclient = new DefaultHttpClient();
				httppost = new HttpPost("http://"+LoginActivity.URL+"/schoolmanagement/designs/webservice/checking_pwd_teacher.php");
				nameValuePairs= new ArrayList<NameValuePair>();		
				nameValuePairs.add(new BasicNameValuePair("phoneno",phoneno));
				nameValuePairs.add(new BasicNameValuePair("uid",uid));
				nameValuePairs.add(new BasicNameValuePair("type",type));
				nameValuePairs.add(new BasicNameValuePair("class",forgot_type.getText().toString()));
				nameValuePairs.add(new BasicNameValuePair("sec_question",spin_question.getSelectedItem().toString()));
				nameValuePairs.add(new BasicNameValuePair("sec_answer",edt_sec_answer.getText().toString().trim()));				
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				HttpResponse response = httpclient.execute(httppost); 
				HttpEntity entity = response.getEntity();			
				
				is = entity.getContent();			
					    BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
					    StringBuilder sb = new StringBuilder();
					    while ((line = reader.readLine()) != null)
			            {
					    	
			                sb.append(line + "\n");
			               
			            }
			            is.close();
			            result = sb.toString();	 
			            Log.e("Result",result);
			         JSONObject json_data = new JSONObject(result);
				     JSONObject data = json_data.getJSONObject("DATA");		         
				     String code = data.getString("CODE");
				     message=data.getString("MESSAGE");
				     pwdfrom_service=data.getString("password"); 
			         //uid and type to store in sessions using shared preference
			         if(code.equals("200"))
			         {
			        	  returns=true;
			         }
			         if(code.equals("500"))
			         {
			        	 returns=false;
			         }        
			         		       					
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			return returns;	
	}	
		
	}	
}
