package com.example.school_management;

import com.gr_infotech.adapter.ImageAdapter;
import com.gr_infotech.fragments.Apply_leave_Fragment;
import com.gr_infotech.fragments.Attendance_Fragment;
import com.gr_infotech.fragments.Home_Work_Fragment;
import com.gr_infotech.fragments.Home_Works_Fragment;
import com.gr_infotech.fragments.Notifications_Fragment;
import com.gr_infotech.fragments.Send_Classnotice_Fragment;
import com.gr_infotech.fragments.Reports_Fragment;
import com.gr_infotech.fragments.Students_Fragment;
import com.gr_infotech.fragments.Subject_fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class Head_setting_screen_fragment extends Fragment 
{
	TextView text,image_txt,txt_lbl;
	GridView grid,grid_settings;  
	Gallery galleryView,galleryviews;
	View view;
	String code,uid;
	SharedPreferences sharedpreferences,shares;
	
	String type;
	String label_for_grid_identification;	
	SharedPreferences shared;
	String grid_label[]=new String[4];
	int grid_image[]=new int[4];
	char dyn_letter;
	TextView txt_head;
	String gallery_name,gallery_sec;
	 Editor editor;
	 String ff_ll;
	private static final String SH_UIDS = "Tripuid";	
	private static final String UID = "UniqueID";
	private static final String TYPE = "Type";

	 public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle args) 
	    {    	
	        view = inflater.inflate(R.layout.head_setting_screen_fragment, container, false);                 
	        grid=(GridView)view.findViewById(R.id.grid_view);   		
	        txt_lbl=(TextView)view.findViewById(R.id.txt_label);     
	        txt_head = (TextView)view.findViewById(R.id.imagetext);	        
	        shared = getActivity().getSharedPreferences(SH_UIDS,Context.MODE_PRIVATE);
			uid=(shared.getString(UID,null));
			type=(shared.getString(TYPE,null));
			
			
			if(type.equals("Teacher"))
			{
				txt_lbl.setText("Class");
				grid_label[0]="Students";
				grid_label[1]="Send class notice";
				grid_label[2]="Attendance";
				grid_label[3]="Home Work";  	        	
				grid_image[0]=R.drawable.settings_student;
				grid_image[1]=R.drawable.sendclsnotice;
				grid_image[2]=R.drawable.take_attendance;
				grid_image[3]=R.drawable.information;        	
			
				
				gallery_name=SettingsScreenActivity.dynamic_header_name[0];
				gallery_sec=SettingsScreenActivity.dynamic_header_sec[0];
				//gallery_name=sharedfirst_letter.getString("firstname",null); 
				Log.e("First Name",gallery_name);
				dyn_letter  =gallery_name.charAt(0);
	         	ff_ll = " "+dyn_letter+" ";
	         	txt_head.setText(ff_ll);  
	         	
	         	galleryviews = (Gallery)view.findViewById(R.id.galleryid);
	            galleryviews.setAdapter(new GalleryAdapter(getActivity().getApplicationContext()));        
	            galleryviews.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent, View view,int position, long id) {
						
						Toast.makeText(getActivity().getApplicationContext(),SettingsScreenActivity.dynamic_header_name[position]+"-"+SettingsScreenActivity.dynamic_header_sec[position]+"Selected",Toast.LENGTH_LONG).show();
						   
						
						   shares=getActivity().getSharedPreferences("gallery_name",Context.MODE_PRIVATE);
						   editor = shares.edit();
		          		   editor.clear();
		          		   editor.putString("name",SettingsScreenActivity.dynamic_header_name[position]);					      
					       editor.putString("sec",SettingsScreenActivity.dynamic_header_sec[position]);
					       editor.commit();   	
					       gallery_name=SettingsScreenActivity.dynamic_header_name[position];          		   
						   dyn_letter  =gallery_name.charAt(0);
				           ff_ll = " "+dyn_letter+" ";
				           txt_head.setText(ff_ll);  							
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
						
						
						
					}
				});        
		
			}
			if(type.equals("Parent"))
			{
				txt_lbl.setText("Child");
				grid_label[0]="Apply Leave";
				grid_label[1]="Notifications";
				grid_label[2]="Report";
				grid_label[3]="Home Works";  	        	
				grid_image[0]=R.drawable.settings_student;
				grid_image[1]=R.drawable.sendclsnotice;
				grid_image[2]=R.drawable.take_attendance;
				grid_image[3]=R.drawable.information;        	
			
			
				
				gallery_name=SettingsScreenActivity.dynamic_header_name[0];
				
				//gallery_name=sharedfirst_letter.getString("firstname",null); 
				Log.e("First Name",gallery_name);
				dyn_letter  =gallery_name.charAt(0);
	         	ff_ll = " "+dyn_letter+" ";
	         	txt_head.setText(ff_ll);  
	         	
	         	galleryView = (Gallery)view.findViewById(R.id.galleryid);
	            galleryView.setAdapter(new GalleryAdapter(getActivity().getApplicationContext()));        
	            galleryView.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent, View view,int position, long id) {
						
						Toast.makeText(getActivity().getApplicationContext(),SettingsScreenActivity.dynamic_header_name[position]+" Selected",Toast.LENGTH_LONG).show();
						   
						
						   // To Store data to fetch data in fragment using class and section
						
						   shares=getActivity().getSharedPreferences("gallery_name",Context.MODE_PRIVATE);
						   editor = shares.edit();
		          		   editor.clear();
		          		   editor.putString("name",SettingsScreenActivity.dynamic_header_name[position]);		
					       editor.putString("class",SettingsScreenActivity.dynamic_header_class[position]);//class
					       editor.putString("sec",SettingsScreenActivity.dynamic_header_sec[position]);
					       editor.putString("no", SettingsScreenActivity.dynamic_header_no[position]);
					       editor.commit();   
					       
					       
					       gallery_name=SettingsScreenActivity.dynamic_header_name[position];          		   
						   dyn_letter  =gallery_name.charAt(0);
				           ff_ll = " "+dyn_letter+" ";
				           txt_head.setText(ff_ll);  							
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
						
						
						
					}
				});	            
	
			}      
	        
       	 ImageAdapter adapters=new ImageAdapter(getActivity().getApplicationContext(), grid_label,grid_image);
       	 grid.setAdapter(adapters);
       	 
       	 grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
       		    @Override
       		    public void onItemClick(AdapterView<?> parent, View view,int position, long id) {

       		    	Log.d("Inside >> ImageAdapter Click :: web "," "+grid_label+" ; "+grid_image);
       		    	
       		    	if(grid_label[position].equals("Students"))
       		     	{  
       		    		setTitle("Students Information");
       		    		// write code to make generic for all the three domains          		
       		    		Fragment fragment = new Students_Fragment();
       		     		FragmentManager fragmentManager = getFragmentManager();
       		            fragmentManager.beginTransaction().replace(R.id.content_frame,fragment).commit();       		            	                              	    
       		     	}
       		     	if(grid_label[position].equals("Send class notice"))
       		     	{                 		
       		     		// write code to make generic for all the three domains
       		     	    setTitle("Send class notice");
       		     		Fragment fragment = new Send_Classnotice_Fragment();
       		     		FragmentManager fragmentManager = getFragmentManager();
       		            fragmentManager.beginTransaction().replace(R.id.content_frame,fragment).commit();                   		
       		     	}
       		     	if(grid_label[position].equals("Attendance"))
       		     	{     		     		
       		     	setTitle("Attendance");
       		     	Fragment fragment = new Attendance_Fragment();
   		     		FragmentManager fragmentManager = getFragmentManager();
   		            fragmentManager.beginTransaction().replace(R.id.content_frame,fragment).commit();
       		     	
       		     	}
       		     	if(grid_label[position].equals("Home Work"))
       		     	{
       		     		//Log.e("Entered","Take Attendance");
       		     		// write code to make generic for all the three domains
       		     	    setTitle("Home work");
       		     		Fragment fragment = new Home_Work_Fragment();
       		     		FragmentManager fragmentManager = getFragmentManager();
       		            fragmentManager.beginTransaction().replace(R.id.content_frame,fragment).commit();
       		     	}  
       		     	
       		        if(grid_label[position].equals("Home Works"))
    		     	{
    		     		//Log.e("Entered","Take Attendance");
    		     		// write code to make generic for all the three domains
       		        	setTitle("Subjects List");    		     		
       		        	Fragment fragment=new Subject_fragment();
       		        	FragmentManager fragmentManager = getFragmentManager();
       		        	fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
       		        	/*Fragment fragment = new Home_Works_Fragment();
    		     		FragmentManager fragmentManager = getFragmentManager();
    		            fragmentManager.beginTransaction().replace(R.id.content_frame,fragment).commit();*/
    		     	}  
       		     	
       		        
       		     if(grid_label[position].equals("Apply Leave"))
 		     	{
       		    	setTitle("Apply Leave");
 		     		Fragment fragment = new Apply_leave_Fragment();
 		     		FragmentManager fragmentManager = getFragmentManager();
 		            fragmentManager.beginTransaction().replace(R.id.content_frame,fragment).commit();
 		     	}  
       		     	
       		   
       		     if(grid_label[position].equals("Notifications"))
 		     	{
 		     		//Log.e("Entered","Take Attendance");
 		     		// write code to make generic for all the three domains
       		    	setTitle("Notifications");
 		     		Fragment fragment = new Notifications_Fragment();
 		     		FragmentManager fragmentManager = getFragmentManager();
 		            fragmentManager.beginTransaction().replace(R.id.content_frame,fragment).commit();
 		     	}  
       		     	
       		  
       		     	
       		 if(grid_label[position].equals("Report"))
		     	{
		     		//Log.e("Entered","Take Attendance");
		     		// write code to make generic for all the three domains
       			    setTitle("Report");
		     		Fragment fragment = new Reports_Fragment();
		     		FragmentManager fragmentManager = getFragmentManager();
		            fragmentManager.beginTransaction().replace(R.id.content_frame,fragment).commit();
		     	}   		     	
       		    }
       		});               
         
			 return view;	    
	    }	 
	
	
	
	 public class GalleryAdapter extends BaseAdapter {

	  	  private Context context;
	  	  private int itemBackground;
	  	  private Activity activity;
	  	  private LayoutInflater inflater=null;
	  	

	  	  public GalleryAdapter(Context c)
	  	  {
	  	  context = c;
	  	  inflater = (LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	  	  }
	  	
		public int getCount() {
			
	  	  return SettingsScreenActivity.dynamic_header_name.length;
	  	//dynamic_header_name
	  	//name.length;
	  	  }
	  	  // returns the ID of an item
	  	  public Object getItem(int position) {
	  	  return position;
	  	  }
	  	  // returns the ID of an item
	  	  public long getItemId(int position) {
	  	  return position;
	  	  }
	  	  // returns an ImageView view
	  	  public View getView(int position, View convertView, ViewGroup parent) {
	  	  TextView classes=new TextView(context);
	  	  classes.setPadding(40,40,40,40);
	  	  classes.setTextColor(Color.WHITE);	
	  	  
	  	  if(type.equals("Teacher"))
	  	  {
	  		classes.setText(SettingsScreenActivity.dynamic_header_name[position]+" "+SettingsScreenActivity.dynamic_header_sec[position]);  
	  	  }
	  	if(type.equals("Parent"))
	  	  {
	  		classes.setText(SettingsScreenActivity.dynamic_header_name[position]);  
	  	  }
	  	  
	  	   //classes.setText(name[position]);
	  	  classes.setTextAppearance(getActivity().getApplicationContext(),android.R.style.TextAppearance_Large);
	  	  classes.setTypeface(null, Typeface.BOLD);
	  	  return classes;
	  	  }
	  	  }	  
	 
	 public void setTitle(CharSequence title) {	     
	     
		 ((SettingsScreenActivity) getActivity()).getSupportActionBar().setTitle(title);
		 
	 }
}








    		
