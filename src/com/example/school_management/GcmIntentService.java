package com.example.school_management;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;
import com.example.school_management.MainActivity;
import com.example.school_management.R;
import com.google.android.gcm.GCMBaseIntentService;
import com.gr_infotech.fragments.Notifications_Fragment;
import com.gr_infotech.fragments.Send_Classnotice_Fragment;
import com.gr_infotech.pojo.Chat_setter_getter;
import com.gr_infotech.pojo.DatabaseHandler;
 
public class GcmIntentService extends GCMBaseIntentService {
 
	static SharedPreferences shared;	
	public static final String SENDER_ID = "391767134233";
	public static int flags=0;
	public static Editor editor;
	
 public GcmIntentService() {
  super(SENDER_ID);
  shared=getApplicationContext().getSharedPreferences("GCM",MODE_PRIVATE);
  
 }
 
 private static final String TAG = "===GCMIntentService===";
 
 @Override
 protected void onRegistered(Context arg0, String registrationId) {
  Log.e(TAG, "Device registered: regId = " + registrationId);
 }
 
 @Override
 protected void onUnregistered(Context arg0, String arg1) {
  Log.e(TAG, "unregistered = " + arg1);
 }
 
 @Override
 protected void onMessage(Context context, Intent intent) {
  Log.e(TAG, "new message= ");
  String message = intent.getExtras().getString("message");
  generateNotification(context, message);
 }
 
 @Override
 protected void onError(Context arg0, String errorId) {
  Log.e(TAG, "Received error: " + errorId);
 }
 
 @Override
 protected boolean onRecoverableError(Context context, String errorId) {
  return super.onRecoverableError(context, errorId);
 }
 
 /**
  * Issues a notification to inform the user that server has sent a message.
  */
 private static void generateNotification(Context context, String message) {
  int icon = R.drawable.ic_launcher;
  long when = System.currentTimeMillis();
  
  String title = context.getString(R.string.app_name);
  String notifytype,notifycontent,notifyclass,notifysection,notifysubject;
  String[] parts = message.split("~");
  notifytype = parts[0]; 
  notifycontent = parts[1]; 
  notifyclass= parts[2];
  notifysection= parts[3];
  notifysubject=parts[4];
  
  DatabaseHandler db = new DatabaseHandler(context);
  db.addData(new Chat_setter_getter("receive",notifycontent,notifytype,notifyclass,notifysection,notifysubject));
  flags=1;  
  db.close();
  
  
  
  editor= shared.edit();
   editor.clear();
 editor.putString("gcmintent","settings_screen");
 editor.putString("notifytype",notifytype);
 editor.putString("gcmintent_message",notifycontent);
 editor.commit();
  
 
  Intent notificationIntent = new Intent(context,SettingsScreenActivity.class);  
  PendingIntent pIntent = PendingIntent.getActivity(context, 0,notificationIntent, 0);
  Notification n  = new Notification.Builder(context)
  .setContentTitle(title)
  .setContentText(notifysubject+":"+notifycontent)
  .setSmallIcon(icon)
  .setContentIntent(pIntent) 
  .setAutoCancel(true).build();
  NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
  notificationManager.notify(0, n);  
 }
}












