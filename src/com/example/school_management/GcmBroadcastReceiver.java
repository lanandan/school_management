package com.example.school_management;

import android.app.Activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

public class GcmBroadcastReceiver extends WakefulBroadcastReceiver{

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		Log.i("test","its cmg in the broadcast receiver or not");
		  ComponentName comp = new ComponentName(context.getPackageName(),GcmIntentService.class.getName());
	        // Start the service, keeping the device awake while it is launching. 
	        startWakefulService(context, (intent.setComponent(comp)));
	        setResultCode(Activity.RESULT_OK);
	}

}
