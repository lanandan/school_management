package com.example.school_management;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import com.example.school_management.LoginActivity.Login_process;
import com.gr_infotech.utilities.Network_checking;
import android.support.v7.app.ActionBarActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Secondary_Mobile extends ActionBarActivity implements OnClickListener
{
	EditText name,mobile_no,email_id;
	CheckBox chkemail,chksms;
	Button saveadd;
	HttpClient httpclient;
	HttpPost httppost;
	ArrayList<NameValuePair> nameValuePairs;
	InputStream is;
	ProgressDialog pd;
	String message,line,result;
	SharedPreferences shared;
	    private static final String SH_UIDS = "Tripuid";	
		private static final String UID = "UniqueID";
		private static final String TYPE = "Type";
		private static final String PHNO = "phone";
		String uid,type,phno;
		TextView skipnow;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_secondary__mobile);
		name=(EditText)findViewById(R.id.edt_mobname);
		mobile_no=(EditText)findViewById(R.id.edt_mobno);
		email_id=(EditText)findViewById(R.id.Email_Id);
		chkemail=(CheckBox)findViewById(R.id.chk_email);
		chksms=(CheckBox)findViewById(R.id.chk_sms);		
		saveadd=(Button)findViewById(R.id.save_add);
		saveadd.setOnClickListener(this);
		
		skipnow=(TextView)findViewById(R.id.skipnow);
		skipnow.setOnClickListener(this);
		
		shared = getSharedPreferences(SH_UIDS,Context.MODE_PRIVATE);
			uid=(shared.getString(UID,null));
			type=(shared.getString(TYPE,null));
		    phno=(shared.getString(PHNO,null));
		   
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.secondary__mobile, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) 
	{
		if(v.getId()==R.id.save_add)
		{
			if(secondary_mobile_validation())
			{
				if(Network_checking.isNetworkAvailable(Secondary_Mobile.this))
				{
					Secondary_mobile_process secondary=new Secondary_mobile_process();
					secondary.execute();				
				}
				else
				{
					showToast("Please check your network connection");
				}
			}			
		}
		
		if(v.getId()==R.id.skipnow)
		{
			Intent i=new Intent(getApplicationContext(),AddChildActivity.class);	          	
			startActivity(i);	
			overridePendingTransition(R.anim.right_in, R.anim.left_out);	
			skipnow.setPaintFlags(skipnow.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
			
		}		
	}
	
	
	
	public boolean secondary_mobile_validation()
	{
			String mobileno,names,email;	
			mobileno = mobile_no.getText().toString().trim();
			names = name.getText().toString().trim();
			email=email_id.getText().toString().trim();			
			Drawable dr = getResources().getDrawable(R.drawable.star_boxed_full);            
			dr.setBounds(0, 0, dr.getIntrinsicWidth(), dr.getIntrinsicHeight());
			
									
			if (mobileno.equals("".trim()) || names.equals("".trim()))
			{
				if(mobile_no.getText().toString().trim().equalsIgnoreCase(""))
				{
					mobile_no.setError("Phone Number should be filled",dr);
				}
				if(name.getText().toString().trim().equalsIgnoreCase(""))
				{
					name.setError("Password should be filled",dr);
				}
				
				if(chkemail.isChecked())
				{
				if(email_id.getText().toString().trim().equalsIgnoreCase(""))
				{
					email_id.setError("Password should be filled",dr);
				}
				}
				
				return false;
			}
			else
			{
				if (mobileno.length() == 10)
				{
					if(chkemail.isChecked())
					{					
					if (isValidEmail(email_id.getText().toString()))
					{
						return true;
					}
					else
					{
						email_id.setText("");
						email_id.setError("Not a valid email id",dr);
						email_id.requestFocus();
						return false;
					}					
				}
					else
					{
						return true;
					}
				}
				else
				{
					showToast("Invalid Mobile Number");
					mobile_no.setText("");
					mobile_no.setError("Mobile Number Should be 10 digits",dr);
					mobile_no.requestFocus();
					return false;
				}
			}
	}
	
	private void showToast(String msg)
	{
		Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
	}	

	
	
	
	 private boolean isValidEmail(String email) {
			String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
					+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

			Pattern pattern = Pattern.compile(EMAIL_PATTERN);
			Matcher matcher = pattern.matcher(email);
			return matcher.matches();
		}	 
	
	
	 
	 public class Secondary_mobile_process extends AsyncTask<Void, Void, ArrayList<String>>
		{	
			int flag=0;
			
			
			@Override
			protected ArrayList<String> doInBackground(Void... params) {					
				
				if(checkingvaliduser())
				{
					flag=1;
				}
				else
				{
					flag=2;
				}
				return null;
			}		
			@Override
		    protected void onPreExecute() {	           
		            super.onPreExecute();                     
		            pd = new ProgressDialog(Secondary_Mobile.this,R.style.MyTheme1);
					pd.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progressbar_custom));
					pd.setCancelable(false);
					pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
					pd.show();           
		    }		
			@Override
		    protected void onPostExecute(ArrayList<String> contacts) {
		        super.onPostExecute(contacts);
		        pd.cancel();		        
		        if(flag==1)
		        {
		        	mobile_no.setText("");
		        	name.setText("");
		        	email_id.setText("");
		        	Toast.makeText(getApplicationContext(),"Added successfully", Toast.LENGTH_SHORT).show();		        	
		        }
		        if(flag==2)
		        {
		        	Toast.makeText(getApplicationContext(),message, Toast.LENGTH_SHORT).show();
		        }	        
		    }
				
			public boolean checkingvaliduser()
			{
				boolean returns = false;
				try
				{
					httpclient = new DefaultHttpClient();
					String httpid = "http://"+LoginActivity.URL+"/schoolmanagement/designs/webservice/secondary_data.php";
					httppost = new HttpPost(httpid);
					nameValuePairs= new ArrayList<NameValuePair>();
					nameValuePairs.add(new BasicNameValuePair("uid",uid));
					nameValuePairs.add(new BasicNameValuePair("type",type));									
					nameValuePairs.add(new BasicNameValuePair("mobile_no",mobile_no.getText().toString()));
					nameValuePairs.add(new BasicNameValuePair("name",name.getText().toString()));
					if(chkemail.isChecked())
					{
						nameValuePairs.add(new BasicNameValuePair("email",email_id.getText().toString()));	
					}
					else
					{
						nameValuePairs.add(new BasicNameValuePair("email","Nil"));
					}
										
					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
					HttpResponse response = httpclient.execute(httppost); 
					HttpEntity entity = response.getEntity();
					
					is = entity.getContent();			
						    BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
						    StringBuilder sb = new StringBuilder();
						    while ((line = reader.readLine()) != null)
				            {					    	
				                sb.append(line + "\n");
				               
				            }
				            is.close();
				            result = sb.toString();
				            Log.e("message", result);
				         JSONObject json_data = new JSONObject(result);
					     JSONObject data = json_data.getJSONObject("DATA");		         
					     String code = data.getString("CODE");
					     message =data.getString("MESSAGE");		         
				         if(code.equals("200"))
				         {
				        	  returns=true;
				         }
				         if(code.equals("500"))
				         {
				        	 returns=false;
				         }        
				                					
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				return returns;	
			}		
		}
}
