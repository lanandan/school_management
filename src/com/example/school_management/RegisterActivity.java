package com.example.school_management;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import com.example.school_management.LoginActivity.Login_process;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.gr_infotech.utilities.Network_checking;
import android.support.v7.app.ActionBarActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class RegisterActivity extends ActionBarActivity implements OnClickListener
{

	EditText name,mobile_no,passwd,answer;
	Button register,clear;
	Spinner type,sec_question;	
	String[] spinner_type = {"Select Type","Teacher","Parent"};
	String[] question={"Security Question","what is your school name?","What is your pet’s name?","What is your favorite game?","What month were you born?","What is your mother’s birthday?"};
	String webServiceInfo = "";
	TextView selecttype;
	
	ArrayAdapter<String> adapter_type,adapter_question;
	String strname,strmobile_no,strpwd,stranswer;
	HttpClient httpclient;
	HttpPost httppost;
	ArrayList<NameValuePair> nameValuePairs;
	InputStream is;
	String line,result;
	String uid,str_type;
	SharedPreferences shared;
	Typeface nobileMedium, nobileRegular, nobileBoldItalic, nobileMediumItalic;
	String sec_questions,types;
	Resources res;
	String message;
	ProgressDialog pd;
	RadioButton selected;
	
	private static final String SH_UIDS = "Tripuid";	
	private static final String UID = "UniqueID";
	private static final String TYPE = "Type";
	private static final String PHNO = "phone";
	
	RadioGroup rg;
	String selectedoperation;
	
	//Variables related to GCM
	
	GoogleCloudMessaging gcm;	
	String regid;
	//Sender ID=ProjectNumber from Google Developer console = Srishiva53@gmail.com account
	public static final String SENDER_ID = "391767134233";
	SharedPreferences gcm_share;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);	
		getSupportActionBar().hide();
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);		
		
		rg=(RadioGroup)findViewById(R.id.mobforsms);
		rg.setVisibility(View.INVISIBLE);
		
		nobileMedium = Typeface.createFromAsset(getAssets(), "fonts/Nobile-Medium.ttf");
		nobileRegular = Typeface.createFromAsset(getAssets(), "fonts/Nobile-Regular.ttf");
		nobileBoldItalic = Typeface.createFromAsset(getAssets(), "fonts/Nobile-BoldItalic.ttf");
		nobileMediumItalic = Typeface.createFromAsset(getAssets(), "fonts/Nobile-MediumItalic.ttf");
		
		name=(EditText)findViewById(R.id.regis_edt_name);
		mobile_no=(EditText)findViewById(R.id.regis_edt_mobileno);
		passwd=(EditText)findViewById(R.id.regs_edt_pwd);
		answer=(EditText)findViewById(R.id.regs_edt_ans);
		register=(Button)findViewById(R.id.regs_btn_register);
		register.setOnClickListener(this);
		selecttype=(TextView)findViewById(R.id.Secondary_mobile);
		selecttype.setVisibility(View.INVISIBLE);
		
		register.setTypeface(nobileBoldItalic);		
		name.setTypeface(nobileRegular);
		mobile_no.setTypeface(nobileRegular);
		answer.setTypeface(nobileRegular);
		
		
		
		type=(Spinner)findViewById(R.id.regs_select_type);
		
		type.setOnItemSelectedListener(new OnItemSelectedListener() {
		    @Override
		    public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
		        
		    	if (type.getSelectedItem().toString().equals("Parent"))
		    	{
		    		rg.setVisibility(View.VISIBLE);
		    		selecttype.setVisibility(View.VISIBLE);		    		
		    		int selectedId = rg.getCheckedRadioButtonId();
		    		selected = (RadioButton) findViewById(selectedId);
		    		selectedoperation=selected.getText()+"";
		    	
		    	}
		    	if (type.getSelectedItem().toString().equals("Teacher"))
		    	{
		    		rg.setVisibility(View.INVISIBLE);
		    		selecttype.setVisibility(View.INVISIBLE);		    		
		    		
		    	}
		    	
		    	
		    	
		    }

		    @Override
		    public void onNothingSelected(AdapterView<?> parentView) {
		        // your code here
		    }

		});
		
		
		sec_question=(Spinner)findViewById(R.id.regs_secquestion);	
		
		adapter_type = new ArrayAdapter(this,android.R.layout.simple_spinner_item,spinner_type);
		adapter_type.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		type.setAdapter(adapter_type);
		
		adapter_question = new ArrayAdapter(this,android.R.layout.simple_spinner_item,question);
		adapter_question.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sec_question.setAdapter(adapter_question);
				
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	@Override
	public void onBackPressed() 
	{
		//onDestroy();
		//moveTaskToBack(true);
		Intent i=new Intent(getApplicationContext(),LoginActivity.class);	          	
		startActivity(i);	
		overridePendingTransition(R.anim.right_in, R.anim.left_out);
		
	}
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
	}
	
	@Override
	protected void onResume() 
	{
		super.onResume();
		//setCustomTitle();
	}

	private void setCustomTitle() 
	{
		if (android.os.Build.VERSION.SDK_INT >= 14) 
		{
			// getActionBar().setDisplayShowTitleEnabled(false);
			// getActionBar().setSubtitle(getResources().getString(R.string.txt_subtitle));

			getActionBar().setTitle(getResources().getString(R.string.title_activity_add_child));
//			getActionBar().setLogo(R.drawable.schedule_trans);

			int actionBarTitle = Resources.getSystem().getIdentifier(
					"action_bar_title", "id", "android");
			TextView actionBarTitleView = (TextView) getWindow().findViewById(
					actionBarTitle);
			actionBarTitleView.setTextSize(20);			

			if (actionBarTitleView != null) {
				actionBarTitleView.setTypeface(nobileBoldItalic);
			}
		}
	}

	@Override
	public void onClick(View v) {
		
		
		if(v.getId()==R.id.regs_btn_register)
		{
			if(registrationvalidation())
			{
			
				if(Network_checking.isNetworkAvailable(RegisterActivity.this))
				{
					
					Registration_process register=new Registration_process();
					register.execute();	
					
					
				}
				else
				{
					showToast("Please check your network connection");
				}
			}	
		}
		
		
	}
	
	
	public void input_text_clear()
	{
		name.setText("");		
		mobile_no.setText("");
		passwd.setText("");
		answer.setText("");
		type.setSelection(0);
		sec_question.setSelection(0);		
	}
	
	public boolean registrationvalidation()
	{
		
		strname = name.getText().toString().trim();
		strmobile_no = mobile_no.getText().toString().trim();
		strpwd=passwd.getText().toString().trim();
		stranswer=answer.getText().toString().trim();
		sec_questions=sec_question.getSelectedItem().toString();
		types=type.getSelectedItem().toString();		
		
		
		if (strname.equals("".trim()) || strmobile_no.equals("".trim())||strpwd.equals("".trim())||stranswer.equals("".trim())||sec_questions.equals("Security Question")||types.equals("Select Type"))
		{
			Drawable dr = getResources().getDrawable(R.drawable.star_boxed_full); 
            dr.setBounds(0, 0, dr.getIntrinsicWidth(), dr.getIntrinsicHeight());
			
			if(name.getText().toString().trim().equalsIgnoreCase(""))
			{
				name.setError("Name field should be filled",dr);
				
			}
			if(mobile_no.getText().toString().trim().equalsIgnoreCase(""))
			{
				mobile_no.setError("Mobile No field should be filled",dr);
				
			}
			if(passwd.getText().toString().trim().equalsIgnoreCase(""))
			{
				passwd.setError("Password field should be filled",dr);
				
			}
			if(answer.getText().toString().trim().equalsIgnoreCase(""))
			{
				answer.setError("Answer field should be filled",dr);
				
			}
			if(sec_questions.equals("Security Question"))
			{
				((TextView)sec_question.getChildAt(0)).setError("Select Security Question",dr);
				
				//showToast("Security Question should be Selected");			
			}
	        if(types.equals("Select Type"))
			{
	        	
	        	((TextView)type.getChildAt(0)).setError("Select Type",dr);
				//showToast("Type should be selected for teacher or parent");
			}
			showToast("Please Fill all the fields");
			return false;
		}
		else
		{
			Drawable dr = getResources().getDrawable(R.drawable.star_boxed_full); 
            dr.setBounds(0, 0, dr.getIntrinsicWidth(), dr.getIntrinsicHeight());
			if (mobile_no.length() == 10)
			{
				if (strpwd.length() > 3)
					return true;
				else
				{
					showToast("Password Should be Minimum 4 Digits");
					passwd.setText("");		
					passwd.setError("Password Should be Minimum 4 Digits",dr);
					passwd.requestFocus();
					return false;
				}
			}				
			{
				showToast("Invalid Mobile Number");
				mobile_no.setText("");
				mobile_no.setError("Invalid Mobile Number",dr);
				mobile_no.requestFocus();
				return false;
			}
		}
		
		
	}
	
	private void showToast(String msg)
	{
		Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
	}	
	
	
	public class Registration_process extends AsyncTask<Void, Void, ArrayList<String>>
	{	
		int flag=0;
		
		
		@Override
		protected ArrayList<String> doInBackground(Void... params) {					
			
			if(checking_Registration_success())
			{
				flag=1;
			}
			else
			{
				flag=2;
			}
			return null;
		}		
		@Override
	    protected void onPreExecute() {
	            // TODO Auto-generated method stub
	            super.onPreExecute();                     
	            pd = new ProgressDialog(RegisterActivity.this,R.style.MyTheme1);
				pd.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progressbar_custom));
				pd.setCancelable(false);
				pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
				pd.show();	            
	            //pd = ProgressDialog.show(LoginActivity.this, "Logging in","Please Wait");
	    }		
		@Override
	    protected void onPostExecute(ArrayList<String> contacts) {
	        super.onPostExecute(contacts);
	        pd.cancel();		        
	        if(flag==1)
	        {
	        	if(type.getSelectedItem().toString().equals("Parent"))
				{
	        		
	        		if(selectedoperation.equals("Yes"))
		        	{
	        			Intent i=new Intent(getApplicationContext(),Secondary_Mobile.class);	          	
						startActivity(i);	
						overridePendingTransition(R.anim.right_in, R.anim.left_out);		 
	        		}
		        	else
		        	{
		        		Intent i=new Intent(getApplicationContext(),AddChildActivity.class);	          	
						startActivity(i);	
						overridePendingTransition(R.anim.right_in, R.anim.left_out);		        		
		        	}	        									
				}
				else
				{					
					Log.e("Setting Screen","Settings Screeen");
					Intent i=new Intent(getApplicationContext(),SettingsScreenActivity.class);	          	
					startActivity(i);	
					overridePendingTransition(R.anim.right_in, R.anim.left_out);							
				}
	        }
	        if(flag==2)
	        {
	       
	        	Toast.makeText(getApplicationContext(),message, Toast.LENGTH_SHORT).show();
	        }	        
	    }
	
	}
	
	
	
	public boolean checking_Registration_success()
	{
		boolean returns=false;
		try
		{
			
			
			
			//Getting GCM Id to send notification
			
			/*
			if(gcm==null)
			{
				gcm=GoogleCloudMessaging.getInstance(getApplicationContext());
			}
		    try {			
				regid=gcm.register(SENDER_ID);
				Log.e("Register ID",regid);
				gcm_share=getSharedPreferences("gcm",Context.MODE_PRIVATE);
				SharedPreferences.Editor editor = gcm_share.edit();
				editor.putString("RegId",regid);
				editor.commit();
				Log.i("Test","The value is saved in preference"+gcm_share.getString("RegId",""));
			}
		   catch (IOException e) {
			
				e.printStackTrace();
			}
		
		    */
		    
		    
		    
		    
			httpclient = new DefaultHttpClient();
			httppost = new HttpPost("http://"+LoginActivity.URL+"/schoolmanagement/designs/webservice/register.php");
			nameValuePairs= new ArrayList<NameValuePair>();			
			nameValuePairs.add(new BasicNameValuePair("Name",strname));
			nameValuePairs.add(new BasicNameValuePair("Mobile_no",strmobile_no));	
			nameValuePairs.add(new BasicNameValuePair("password",strpwd));
			nameValuePairs.add(new BasicNameValuePair("answer",stranswer));
			nameValuePairs.add(new BasicNameValuePair("type",type.getSelectedItem().toString()));
			nameValuePairs.add(new BasicNameValuePair("security_question",sec_question.getSelectedItem().toString()));					
			//nameValuePairs.add(new BasicNameValuePair("Gcm_id",regid));		
			nameValuePairs.add(new BasicNameValuePair("Gcm_id","500000001123444565"));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));			
			HttpResponse response = httpclient.execute(httppost);
			Log.e("entered","entered");
			HttpEntity entity = response.getEntity();
			Log.e("entered","entered");
			is = entity.getContent();	
			Log.e("entered","entered");
			
				    BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
				    StringBuilder sb = new StringBuilder();
				    while ((line = reader.readLine()) != null)
		            {
		                sb.append(line + "\n");
		                Log.e("entered",line);		                
		            }
		            is.close();		            
		            result = sb.toString();		
		            
		         JSONObject json_data = new JSONObject(result);
			     JSONObject data = json_data.getJSONObject("DATA");		         
			     String code = data.getString("CODE");
		         message =data.getString("MESSAGE");		         
		        
		         uid=data.getString("uid");
		         str_type=data.getString("type");		        
		         
		         if(code.equals("200"))
		         {		        	 
		        	 
		        	 if(uid.equals(""))
			         {
			        	 
			         }
			         else
			         {
			        	 shared=getSharedPreferences(SH_UIDS,Context.MODE_PRIVATE);
			           	 Editor editor = shared.edit();
			             editor.putString(UID,uid);
			             editor.putString(TYPE,str_type);	
			             editor.putString(PHNO,mobile_no.getText().toString());
			             editor.putString("Regid",regid);
			             editor.commit();
			         }	  
		        	  returns=true;
		         }
		         if(code.equals("500"))
		         {		        	 
		        	 returns=false;
		         }		         
		              					
		}
		catch(Exception e)
		{
			Log.e("Exception",e+"");
		}
		return returns;	
	}	
}

