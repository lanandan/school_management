package com.example.school_management;

import static com.example.school_management.CommonUtilities.SENDER_ID;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
 
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
 
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gcm.GCMRegistrar;
 

public class MainActivity extends Activity {
 
 private String TAG = "** GCMPushDEMOAndroid**";
 private TextView mDisplay;
 String regId = "Welcome";
 String webServiceInfo = "";
 @Override
 public void onCreate(Bundle savedInstanceState) {
  super.onCreate(savedInstanceState);
  setContentView(R.layout.activity_main);
  checkNotNull(SENDER_ID, "SENDER_ID");
  GCMRegistrar.checkDevice(this);
 // GCMRegistrar.checkManifest(this); 
  mDisplay = (TextView) findViewById(R.id.textView1);
 
  regId = GCMRegistrar.getRegistrationId(this);
 
  if (regId.equals("")) {
   GCMRegistrar.register(this, SENDER_ID);
  } else 
  {
	  Toast.makeText(this,"already have",Toast.LENGTH_LONG);
	  regId="already have";
	  Log.v(TAG, "Already registered");
   }
  /**
   * call asYnc Task
   */
  new sendIdOnOverServer().execute();
  
  
  
 }
 
 private void checkNotNull(Object reference, String name) {
  if (reference == null) {
   throw new NullPointerException(getString(R.string.error_config,
     name));
  }
 
 }
 
 @Override
 protected void onPause() {
  super.onPause();
  GCMRegistrar.unregister(this);
 }
 
 
 
 
 public class sendIdOnOverServer extends AsyncTask<Void, Void, ArrayList<String>> 
 {
 
  ProgressDialog pd = null;
 
  @Override
  protected void onPreExecute() {
   pd = ProgressDialog.show(MainActivity.this, "Please wait",
     "Loading please wait..", true);
   pd.setCancelable(true);
 
  }
 
 
  @Override
  protected void onPostExecute(ArrayList<String> contacts) {
   pd.cancel();
   mDisplay.setText("RegId=" +regId+"Web Service:"+webServiceInfo);
  }

@Override
protected ArrayList<String> doInBackground(Void... params) {
	
	 try {
		    HttpResponse response = null;
		    HttpParams httpParameters = new BasicHttpParams();
		    HttpClient client = new DefaultHttpClient(httpParameters);
		    String url = "http://demo.verifiedwork.com/vw_demo/mini_social_781/GCM.php"+"&regID="+regId;
		    Log.i("Send URL:", url);
		    HttpGet request = new HttpGet(url);
		 
		    response = client.execute(request);
		 
		    BufferedReader rd = new BufferedReader(new InputStreamReader(
		      response.getEntity().getContent()));
		 
		   
		    while ((webServiceInfo = rd.readLine()) != null) {
		     Log.d("****Status Log***", "Webservice: " + webServiceInfo);
		 
		    }
		   } catch (Exception e) {
		    e.printStackTrace();
		   }
	
	
	return null;
}
 
 }
 
}




