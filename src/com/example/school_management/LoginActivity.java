package com.example.school_management;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.gr_infotech.utilities.Network_checking;

public class LoginActivity extends ActionBarActivity implements OnClickListener
{
	Button login;
	EditText edt_login,edt_pwd;
	TextView forgot_pwd;
	CheckBox chremember;	
	Typeface nobileMedium, nobileRegular, nobileBoldItalic, nobileMediumItalic;
	String userName,password;
	SharedPreferences shpRemember,shared,shares,share_forgot_pwd;
	Editor ed;
	HttpClient httpclient;
	HttpPost httppost;
	ArrayList<NameValuePair> nameValuePairs;
	InputStream is;
	String line,result,uid,type,phno;	
	TextView register;
	ProgressDialog pd;
	public static String URL="192.168.1.117";
	private static final String SH_LOGIN = "TripConnectorLogin";	
	private static final String SH_UID = "sUserID";
	private static final String SH_PASSWORD = "sPassword";	
	//shared preference to store userid and password	
	private static final String SH_UIDS = "Tripuid";	
	private static final String UID = "UniqueID";
	private static final String TYPE = "Type";
	private static final String PHNO = "phone";	
    String message;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		getSupportActionBar().hide();
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);	
		
		forgot_pwd=(TextView)findViewById(R.id.login_txtForgotPassword);		
		forgot_pwd.setOnClickListener(this);
		register=(TextView)findViewById(R.id.btn_register);
		login=(Button)findViewById(R.id.login_btnLogin);		
		edt_login=(EditText)findViewById(R.id.edt_login);
		edt_pwd=(EditText)findViewById(R.id.login_etPassword);
		chremember=(CheckBox)findViewById(R.id.login_chRememberMe);
		login.setOnClickListener(this);		
		register.setOnClickListener(this);
		nobileMedium = Typeface.createFromAsset(getAssets(), "fonts/Nobile-Medium.ttf");
		nobileRegular = Typeface.createFromAsset(getAssets(), "fonts/Nobile-Regular.ttf");
		nobileBoldItalic = Typeface.createFromAsset(getAssets(), "fonts/Nobile-BoldItalic.ttf");
		nobileMediumItalic = Typeface.createFromAsset(getAssets(), "fonts/Nobile-MediumItalic.ttf");
		login.setTypeface(nobileBoldItalic);		
		edt_login.setTypeface(nobileRegular);
		edt_pwd.setTypeface(nobileRegular);
		chremember = (CheckBox) findViewById(R.id.login_chRememberMe);
		chremember.setTypeface(nobileMediumItalic);
		getSharedPrefRemember();		
	}

	private void getSharedPrefRemember()
	{
		shpRemember = getSharedPreferences(SH_LOGIN, MODE_PRIVATE);
		userName = shpRemember.getString(SH_UID, null);
		password = shpRemember.getString(SH_PASSWORD, null);
		if (userName == null || password == null)
		{
			Log.d("Remember Password", "No username and password in catch,,,");
		}
		else
		{			
			edt_login.setText(userName);
			edt_pwd.setText(password);
		}
	}
	
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onBackPressed() 
	{
//		onDestroy();
		moveTaskToBack(true);
		overridePendingTransition(R.anim.left_in, R.anim.right_out);
	}
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
	}
	
	@Override
	protected void onResume() 
	{
		super.onResume();
		//setCustomTitle();
	}

	private void setCustomTitle() 
	{
		if (android.os.Build.VERSION.SDK_INT >= 14) 
		{
			 getActionBar().setDisplayShowTitleEnabled(false);
			// getActionBar().setSubtitle(getResources().getString(R.string.txt_subtitle));

			getActionBar().setTitle(getResources().getString(R.string.title_activity_login));
			//getActionBar().setLogo(R.drawable.schedule_trans);

			int actionBarTitle = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
			TextView actionBarTitleView = (TextView) getWindow().findViewById(actionBarTitle);
			actionBarTitleView.setTextSize(20);			
			if (actionBarTitleView != null) {
				actionBarTitleView.setTypeface(nobileBoldItalic);
			}
		}
	}
	
	
	
	
	@Override
	public void onClick(View v) 
	{
		if(v.getId()==R.id.login_txtForgotPassword)
		{
			forgot_pwd.setPaintFlags(forgot_pwd.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
			if(edt_login.getText().equals("".trim())||edt_login.getText().toString().length() != 10)
			{		
					showToast("Invalid Mobile Number");
					edt_login.setText("");
					edt_login.requestFocus();				
			}
			else
			{	
				
				if(Network_checking.isNetworkAvailable(LoginActivity.this))
				{
				
				if(get_type())
				{
					Intent i=new Intent(getApplicationContext(),Forgot_pwd_activity.class);	          	
					startActivity(i);	
					overridePendingTransition(R.anim.right_in, R.anim.left_out);	
				}
				else
				{
					showToast(message);
				}
				}
				else
				{
					showToast("Please check your network connection");
				}
			}
			
		}
		
		
		if(v.getId()==R.id.btn_register)
		{
			register.setPaintFlags(forgot_pwd.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
			Intent i=new Intent(getApplicationContext(),RegisterActivity.class);	          	
			startActivity(i);	
			overridePendingTransition(R.anim.right_in, R.anim.left_out);
		}
		
		
		if(v.getId()==R.id.login_btnLogin)
		{
			
			if(loginValidation())
			{
			
				putSharedPrefRemember(userName, password);

				if(Network_checking.isNetworkAvailable(LoginActivity.this))
				{
					Login_process login=new Login_process();
					login.execute();				
				}
				else
				{
					showToast("Please check your network connection");
				}
			}			
		}					
	}
	
	public boolean get_type()
	{
		boolean returns=false;
		try
		{
			httpclient = new DefaultHttpClient();
			httppost = new HttpPost("http://"+URL+"/schoolmanagement/designs/webservice/forgot_pwd.php");
			nameValuePairs= new ArrayList<NameValuePair>();			
			nameValuePairs.add(new BasicNameValuePair("phoneno",edt_login.getText().toString().trim()));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost); 
			HttpEntity entity = response.getEntity();			
			Log.e("data",edt_login.getText().toString().trim()+","+edt_pwd.getText().toString().trim());
			is = entity.getContent();			
				    BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
				    StringBuilder sb = new StringBuilder();
				    while ((line = reader.readLine()) != null)
		            {
				    	
		                sb.append(line + "\n");
		               
		            }
		            is.close();
		            result = sb.toString();	        
		            Log.e("Result",result);
		         JSONObject json_data = new JSONObject(result);
			     JSONObject data = json_data.getJSONObject("DATA");		         
			     String code = data.getString("CODE");
		         message =data.getString("MESSAGE");       
		         
		         uid=data.getString("uid");
		         type=data.getString("type");
		         phno=edt_login.getText().toString();		        
		        
		         if(code.equals("200"))
		         {		        	 
		        	 if(uid.equals(""))
			         {
			        	 
			         }
			         else
			         {
			        	    Editor edit;
							share_forgot_pwd = getSharedPreferences("Mobile_No", MODE_PRIVATE);
							edit = share_forgot_pwd.edit();
							edit.clear();
							edit.putString("Phoneno",edt_login.getText().toString());
							edit.putString("type",type);
							edit.putString("uid",uid);						
							edit.commit();		        	 
			         }
		        	  returns=true;
		         }
		         if(code.equals("500"))
		         {
		        	 returns=false;
		         }        
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return returns;			
	}
	
	
	public void input_text_clear()
	{
		edt_login.setText("");
		edt_pwd.setText("");
	}
	
	public boolean loginValidation()
	{
				
			userName = edt_login.getText().toString().trim();
			password = edt_pwd.getText().toString().trim();
			
			Drawable dr = getResources().getDrawable(R.drawable.star_boxed_full); 
            
			dr.setBounds(0, 0, dr.getIntrinsicWidth(), dr.getIntrinsicHeight());
			if (userName.equals("".trim()) || password.equals("".trim()))
			{
				if(edt_login.getText().toString().trim().equalsIgnoreCase(""))
				{
					edt_login.setError("Phone Number should be filled",dr);
				}
				if(edt_pwd.getText().toString().trim().equalsIgnoreCase(""))
				{
					edt_pwd.setError("Password should be filled",dr);
				}
				return false;
			}
			else
			{
				if (userName.length() == 10)
				{
					if (password.length() > 3)
						return true;
					else
					{
						showToast("Password Should be Minimum 4 Digits");
						edt_pwd.setText("");
						edt_pwd.setError("Password Should be Minimum 4 Digits",dr);
						edt_pwd.requestFocus();
						return false;
					}
				}				
				{//String httpid = "http://smartrestaurant.verifiedwork.com/administrator/index.php/webservices/login_check";
					showToast("Invalid Mobile Number");
					edt_login.setText("");
					edt_login.setError("Mobile Number Should be 10 digits",dr);
					edt_login.requestFocus();
					return false;
				}
			}
		
	}
	
	private void showToast(String msg)
	{
		Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
	}	
	
	
	private void putSharedPrefRemember(String uname, String pwd)
	{		
		shpRemember = getSharedPreferences(SH_LOGIN, MODE_PRIVATE);
		ed = shpRemember.edit();		
		if(chremember.isChecked())
		{
			ed.putString(SH_UID, uname);
			ed.putString(SH_PASSWORD, pwd);			
		}
		else
		{
			ed.putString(SH_UID, "");
			ed.putString(SH_PASSWORD, "");
		}
		ed.commit();
	}
		
	public class Login_process extends AsyncTask<Void, Void, ArrayList<String>>
	{	
		int flag=0;
		String httpid = "http://smartrestaurant.verifiedwork.com/administrator/index.php/webservices/login_check";
		
		@Override
		protected ArrayList<String> doInBackground(Void... params) {					
			
			if(checkingvaliduser())
			{
				flag=1;
			}
			else
			{
				flag=2;
			}
			return null;
		}		
		@Override
	    protected void onPreExecute() {	           
	            super.onPreExecute();                     
	            pd = new ProgressDialog(LoginActivity.this,R.style.MyTheme1);
				pd.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progressbar_custom));
				pd.setCancelable(false);
				pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
				pd.show();           
	    }		
		@Override
	    protected void onPostExecute(ArrayList<String> contacts) {
	        super.onPostExecute(contacts);
	        pd.cancel();		        
	        if(flag==1)
	        {
	        	Intent i=new Intent(getApplicationContext(),SettingsScreenActivity.class);	          	
				startActivity(i);	
				overridePendingTransition(R.anim.right_in, R.anim.left_out);
	        }
	        if(flag==2)
	        {
	        	Toast.makeText(getApplicationContext(),message, Toast.LENGTH_SHORT).show();
	        }	        
	    }
			
		public boolean checkingvaliduser()
		{
			boolean returns = false;
			try
			{
				httpclient = new DefaultHttpClient();
				String httpid = "http://"+URL+"/schoolmanagement/designs/webservice/login.php";
				httppost = new HttpPost(httpid);
				nameValuePairs= new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("mobile_no",edt_login.getText().toString().trim()));
				nameValuePairs.add(new BasicNameValuePair("password",edt_pwd.getText().toString().trim()));				
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				HttpResponse response = httpclient.execute(httppost); 
				HttpEntity entity = response.getEntity();			
				Log.e("data",edt_login.getText().toString().trim()+","+edt_pwd.getText().toString().trim());
				is = entity.getContent();			
					    BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
					    StringBuilder sb = new StringBuilder();
					    while ((line = reader.readLine()) != null)
			            {					    	
			                sb.append(line + "\n");
			               
			            }
			            is.close();
			            result = sb.toString();
			            Log.e("message", result);
			         JSONObject json_data = new JSONObject(result);
				     JSONObject data = json_data.getJSONObject("DATA");		         
				     
				     String code = data.getString("CODE");
				     message =data.getString("MESSAGE");         
			         uid=data.getString("uid");
			         type=data.getString("type");
			         phno=edt_login.getText().toString();
			         
			         Log.e("phoneno",phno);
			         Log.e("Code",code);
			         Log.e("type",type);
			         Log.e("uid",uid);
			         Log.e("message",message);
			         
			         
			         if(code.equals("200"))
			         {
			        	  returns=true;
			         }
			         if(code.equals("500"))
			         {
			        	 returns=false;
			         }        
			         if(uid.equals(""))
			         {
			        	 
			         }
			         else
			         {
			        	 shared=getSharedPreferences(SH_UIDS,MODE_PRIVATE);
			           	 Editor editor = shared.edit();
			           	 editor.clear();
			             editor.putString(UID,uid);
			             editor.putString(TYPE,type);	
			             editor.putString(PHNO,phno);
			             editor.commit();
			         }			       					
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			return returns;	
		}		
	}	
}
