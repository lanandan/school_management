package com.example.school_management;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;



import com.example.school_management.LoginActivity.Login_process;
import com.gr_infotech.utilities.Network_checking;

import android.support.v7.app.ActionBarActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class AddChildActivity extends ActionBarActivity implements OnClickListener 
{

	Button done;
	EditText childname,childno;
	ImageButton addchild;
	Typeface nobileMedium, nobileRegular, nobileBoldItalic, nobileMediumItalic;
	String strchildname,strchildno;
	private static final String SH_UIDS = "Tripuid";	
	private static final String UID = "UniqueID";
	private static final String TYPE = "Type";
	private static final String PHNO = "phone";
	SharedPreferences shared;
	String uid,type;
	HttpClient httpclient;
	HttpPost httppost;
	ArrayList<NameValuePair> nameValuePairs;
	InputStream is;
	String line,result;
	String message;
	String code;
	ProgressDialog pd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_child);
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);			
		getSupportActionBar().hide();
		nobileMedium = Typeface.createFromAsset(getAssets(), "fonts/Nobile-Medium.ttf");
		nobileRegular = Typeface.createFromAsset(getAssets(), "fonts/Nobile-Regular.ttf");
		nobileBoldItalic = Typeface.createFromAsset(getAssets(), "fonts/Nobile-BoldItalic.ttf");
		nobileMediumItalic = Typeface.createFromAsset(getAssets(), "fonts/Nobile-MediumItalic.ttf");
		
		done=(Button)findViewById(R.id.btn_add_child_done);		
		childname=(EditText)findViewById(R.id.add_child_name);
		childno=(EditText)findViewById(R.id.add_child_no);		
		addchild=(ImageButton)findViewById(R.id.btn_add_child);
		addchild.setOnClickListener(this);
		done.setOnClickListener(this);		
		shared = getSharedPreferences(SH_UIDS,Context.MODE_PRIVATE);
		uid=(shared.getString(UID,null));
		type=(shared.getString(TYPE,null));
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_child, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	
	@Override
	public void onBackPressed() 
	{
//		onDestroy();
		moveTaskToBack(true);
		overridePendingTransition(R.anim.left_in, R.anim.right_out);
	}
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
	}
	
	@Override
	protected void onResume() 
	{
		super.onResume();
		//setCustomTitle();
	}

	private void setCustomTitle() 
	{
		if (android.os.Build.VERSION.SDK_INT >= 14) 
		{
			// getActionBar().setDisplayShowTitleEnabled(false);
			// getActionBar().setSubtitle(getResources().getString(R.string.txt_subtitle));

			getActionBar().setTitle(getResources().getString(R.string.title_activity_add_child));
//			getActionBar().setLogo(R.drawable.schedule_trans);

			int actionBarTitle = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
			TextView actionBarTitleView = (TextView) getWindow().findViewById(actionBarTitle);
			actionBarTitleView.setTextSize(20);			

			if (actionBarTitleView != null) {
				actionBarTitleView.setTypeface(nobileBoldItalic);
			}
		}
	}
	
	
	@Override
	public void onClick(View v) 
	{
		if(v.getId()==R.id.btn_add_child)
		{
			
			if(inputValidation())
			{
				if(Network_checking.isNetworkAvailable(AddChildActivity.this))
				{
					add_child_process add_child=new add_child_process();
					add_child.execute();	
					
					
				}
				else
				{
					showToast("Please check your network connection");
				}
				
			}			
			
		}
		if(v.getId()==R.id.btn_add_child_done)
		{
			Intent i=new Intent(getApplicationContext(),SettingsScreenActivity.class);	          	
			startActivity(i);	
			overridePendingTransition(R.anim.right_in, R.anim.left_out);		
		}		
	}
	
	public boolean inputValidation()
	{
		strchildname = childname.getText().toString().trim();
		strchildno = childno.getText().toString().trim();
		
		Drawable dr = getResources().getDrawable(R.drawable.star_boxed_full); 
        dr.setBounds(0, 0, dr.getIntrinsicWidth(), dr.getIntrinsicHeight());
	
        if (strchildname.equals("".trim()) || strchildno.equals("".trim()))
		{
        	if(childname.getText().toString().trim().equalsIgnoreCase(""))
        	{
        		childname.setError("Child Name should be filled",dr);
        	}
        	if(childno.getText().toString().trim().equalsIgnoreCase(""))
        	{
        		childno.setError("Child No should be filled",dr);
        	}        	
			showToast("Fill All Feilds");
			return false;
		}
		else
		{
		return true;
		}		
	}
	

	private void showToast(String msg)
	{
		Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
	}
	
		
	
	
	public class add_child_process extends AsyncTask<Void, Void, ArrayList<String>>
	{	
		int flag=0;
		
		
		@Override
		protected ArrayList<String> doInBackground(Void... params)
		{					
			
			if(checking_adding_child_success())
			{
				flag=1;
			}
			else
			{
				flag=2;
			}
			return null;
		}		
		@Override
	    protected void onPreExecute() {
	            // TODO Auto-generated method stub
	            super.onPreExecute();                     
	            pd = new ProgressDialog(AddChildActivity.this,R.style.MyTheme1);
				pd.setIndeterminateDrawable(getResources().getDrawable(R.drawable.progressbar_custom));
				pd.setCancelable(false);
				pd.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
				pd.show();	            
	            //pd = ProgressDialog.show(LoginActivity.this, "Logging in","Please Wait");
	    }		
		@Override
	    protected void onPostExecute(ArrayList<String> contacts) {
	        super.onPostExecute(contacts);
	        pd.cancel();		        
	      
	        if(flag==1)
	        {
	        	showToast(message);
	        }
	        if(flag==2)
	        {
	        	childno.setText("");
				childname.setText("");				
	        	Toast.makeText(getApplicationContext(),message, Toast.LENGTH_SHORT).show();
	        }	        
	    }
	

		
		public boolean checking_adding_child_success()
		{
			boolean returns=false;
			try
			{
				httpclient = new DefaultHttpClient();
				httppost = new HttpPost("http://"+LoginActivity.URL+"/schoolmanagement/designs/webservice/add_child.php");
				nameValuePairs= new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("childname",strchildname));
				nameValuePairs.add(new BasicNameValuePair("childno",strchildno));				
				nameValuePairs.add(new BasicNameValuePair("type","Parent"));
				nameValuePairs.add(new BasicNameValuePair("uid",uid));			
				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				HttpResponse response = httpclient.execute(httppost); 
				HttpEntity entity = response.getEntity();
				is = entity.getContent();			
				
					    BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
					    StringBuilder sb = new StringBuilder();
					    while ((line = reader.readLine()) != null)
			            {
			                sb.append(line + "\n");
			            }
			            is.close();
			            result = sb.toString();	            
				     JSONObject json_data = new JSONObject(result);
				     Log.e("Results",json_data+"");
				     JSONObject data = json_data.getJSONObject("DATA");		         
				     code = data.getString("CODE");
			         message =data.getString("MESSAGE");		
			         
			         Log.e("Message",message);
			         //uid and type to store in sessions using shared preference
			         
			         if(code.equals("200"))
			         {	        	 
			        	 showToast(message);
			        	  returns=true;
			         }
			         if(code.equals("500"))
			         {
			        	 returns=false;
			         }
			         if(code.equals("700"))
			         {
			        	 returns=false;
			         }
			         
			         if(code.equals("501"))
			         {
			        	 returns=false;
			         }
			         
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			return returns;		
		}
	}
}
