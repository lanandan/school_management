package com.example.school_management;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import com.gr_infotech.adapter.CustomList;
import com.gr_infotech.fragments.Addchild_fragment;
import com.gr_infotech.fragments.Contact_us_Fragment;
import com.gr_infotech.fragments.Edit_account_fragment;
import com.gr_infotech.fragments.Home_Work_Fragment;
import com.gr_infotech.fragments.Home_Works_Fragment;
import com.gr_infotech.fragments.Notifications_Fragment;
import com.gr_infotech.fragments.Send_Classnotice_Fragment;
import com.gr_infotech.fragments.Show_ContactsList_Fragment;
import com.gr_infotech.fragments.Subject_fragment;
import com.gr_infotech.utilities.Logout_Common;
import com.gr_infotech.utilities.Network_checking;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class SettingsScreenActivity extends ActionBarActivity 
{
	private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private DrawerLayout mDrawerLayout;
    HttpClient httpclient;
    HttpPost httppost;
    ArrayList<NameValuePair> nameValuePairs;
    InputStream is;
    static int flag=0;  
    TextView list;
    String[] menu;
    ListView dList;
    private ActionBarDrawerToggle mDrawerToggle;
    SharedPreferences shared,shared_first_name;
    String uid,type,line,result,phno;
    Integer[] imageId; 
    private static final String SH_UIDS = "Tripuid";	
	private static final String UID = "UniqueID";
	private static final String TYPE = "Type";
	private static final String PHNO = "phone";
	public static String dynamic_header_name[],dynamic_header_class[],dynamic_header_sec[],dynamic_header_no[];	
   String code="";
   String message="";
 
   
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings_screen);		
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);			
		list=(TextView)findViewById(R.id.txt);
		 mTitle = mDrawerTitle = getSupportActionBar().getTitle();   
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
	    shared = getSharedPreferences(SH_UIDS,Context.MODE_PRIVATE);
		uid=(shared.getString(UID,null));
		type=(shared.getString(TYPE,null));
	    phno=(shared.getString(PHNO,null));
	   
	    if(type.equals("Teacher"))
		{
			menu = new String[]{"","View","Teacher","Edit Account","Contact Us","Logout"};
		

		     imageId = new Integer[]{
			     		R.drawable.ic_launcher,
			     		1,
			     		1,
			     		1,
			     		1,
			     		R.drawable.icon_logout
			     		};		
		}
		if(type.equals("Parent"))
		{
			menu = new String[]{"","View","Parent","Edit Account","Add Child","Contact Us","Logout"};	
			 imageId = new Integer[]{
			     		R.drawable.ic_launcher,
			     		1,
			     		1,
			     		1,
			     		1,
			     		1,
			     		R.drawable.icon_logout
			     		};		
		}
	     
	     CustomList adapter = new CustomList(this,menu,imageId);
	     dList = (ListView) findViewById(R.id.left_drawer);
	     mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
	     
	     mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,R.drawable.ic_drawer, R.string.drawer_open, R.string.drawer_close) {
	    	 
	    	 
	    	 public void onDrawerClosed(View view) {
	                getSupportActionBar().setTitle(mTitle);
	                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu
	            }
	             
	            public void onDrawerOpened(View drawerView) {
	            	getSupportActionBar().setTitle("Settings Screen");
	                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu
	            }	       
	    	 
	     };
	     
	     mDrawerLayout.setDrawerListener(mDrawerToggle);	   
	     
	     dList.setOnItemClickListener(new OnItemClickListener(){
	     	
	     	public void onItemClick(AdapterView<?> arg0, View v, int position, long id) {         
	 	    	try
	 	    	{
	 	    		
	 	    		displayView(position);
	 	    		
	 	    		
	 	    	}
	 	    	catch(Exception e)
	 	    	{
	 	    		Log.e("Exception Entered",e+""+position);
	 	    		
	 	    	}
	     	 }
	     
	     });
	     
	     dList.setAdapter(adapter);
	     dList.setSelector(android.R.color.holo_orange_dark);
	     if(LoadGalleryname().equals("200"))
	     {
	    	 if (savedInstanceState == null)
			  {        	 
	    		 getSupportActionBar().setTitle("Settings Screen");
		    	 Fragment fragment=new Head_setting_screen_fragment();
				  FragmentManager fragmentManager = getFragmentManager();
			      fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
			      
			  }
		     else
		     {
				  Log.d("savedInstanceState "," Inside If >>> If -- Else "+savedInstanceState);
			 } 
	     }
	     
	     else
	     {    
	    	 if(type.equals("Parent") && message.equals("Data Not Available"))
	 		 {
	    		 Toast.makeText(getApplicationContext(),""+message,Toast.LENGTH_LONG).show();
	    		 
		    	 Intent i=new Intent(getApplicationContext(),AddChildActivity.class);	          	
				 startActivity(i);	
				 overridePendingTransition(R.anim.right_in, R.anim.left_out);
           	 }
	    	 else
	    	 {
	    	 Toast.makeText(getApplicationContext(),""+message,Toast.LENGTH_LONG).show();
	    	 Intent i=new Intent(getApplicationContext(),LoginActivity.class);	          	
			 startActivity(i);	
			 overridePendingTransition(R.anim.right_in, R.anim.left_out);
			 }
	    	 
	     }
	}
	
	
	 protected void onStart(){
		 
		 super.onStart();
		 
		if(GcmIntentService.flags==1)
		{
		 shared = getSharedPreferences("GCM",Context.MODE_PRIVATE);
		 String menuFragment=(shared.getString("gcmintent",null));		 	  
		    if (menuFragment != null)
		    {
		    	if (menuFragment.equals("settings_screen"))
		    	{
		    		FragmentManager fragmentManager = getFragmentManager();
		    		 String notifytype=(shared.getString("notifytype",null));
		    		 if(notifytype.equals("Send Class Notice".trim()))
		    		 {
		    			 Fragment fragment=new Send_Classnotice_Fragment();		    			
		    		    fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
		    	     }
		    		 if(notifytype.equals("Home Work".trim()))
		    		 {
		    			 Fragment fragment=new Home_Work_Fragment();		    			
		    		    fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
		    	     }
		    		 if(notifytype.equals("Home Works".trim()))
		    		 {	    			 
		    			// Note: this is generalised home work 
		    			/*
		    			Fragment fragment=new Home_Works_Fragment();
		    		    fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
		    	    	*/		    			 
		    			 Fragment fragment=new Subject_fragment();
			    		 fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
		    		 }
		    		 if(notifytype.equals("Notification".trim()))
		    		 {
		    			 Fragment fragment=new Notifications_Fragment();
		    		    fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
		    	     }	    		 
	    	
	    		}
		    }
		 }
	 }
	 



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.settings_screen, menu);
	
		return super.onCreateOptionsMenu(menu);
	
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		
		
		if (item != null && item.getItemId() == android.R.id.home) {
            if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                mDrawerLayout.closeDrawer(Gravity.LEFT);
            } else {
                mDrawerLayout.openDrawer(Gravity.LEFT);
            }
            
		 }

		
		switch (item.getItemId()) {
		
		case R.id.create_new_group:			
			Fragment fragment = new Show_ContactsList_Fragment();
			FragmentManager fragmentManager = getFragmentManager();
			fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
			return true;
		
		default:
			return super.onOptionsItemSelected(item);
		}

		//return true;		
		
	}
	
	
	@Override
	public void onBackPressed() 
	{
//		onDestroy();
		moveTaskToBack(true);
		overridePendingTransition(R.anim.left_in, R.anim.right_out);
	}
	
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
	}
	
	@Override
	protected void onResume() 
	{
		
		 super.onResume();
		 
		 
		 if(GcmIntentService.flags==1)
			{
			 shared = getSharedPreferences("GCM",Context.MODE_PRIVATE);
			 String menuFragment=(shared.getString("gcmintent",null));	 
			 	//String menuFragment=getIntent().getStringExtra("gcmintent");		  
			    if (menuFragment != null)
			    {
			    	if (menuFragment.equals("settings_screen"))
			    	{
			    		FragmentManager fragmentManager = getFragmentManager();
			    		 String notifytype=(shared.getString("notifytype",null));
			    		 if(notifytype.equals("Send Class Notice".trim()))
			    		 {
			    			 Fragment fragment=new Send_Classnotice_Fragment();		    			
			    		    fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
			    	     }
			    		 if(notifytype.equals("Home Work".trim()))
			    		 {
			    			 Fragment fragment=new Home_Work_Fragment();		    			
			    		    fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
			    	     }
			    		 if(notifytype.equals("Home Works".trim()))
			    		 {
			    			 /*Fragment fragment=new Home_Works_Fragment();
			    		    fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();*/
			    		    
			    		    Fragment fragment=new Subject_fragment();
				    		 fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
			    	     }
			    		 if(notifytype.equals("Notification".trim()))
			    		 {
			    			 Fragment fragment=new Notifications_Fragment();
			    		    fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
			    	     }	    		 
		    	
		    		}
			    }
			 }		 
	
	}

	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
	    super.onPostCreate(savedInstanceState);
	    mDrawerToggle.syncState();
	}
	
	private void displayView(int position) {
		  
		  Log.d("Inside displayView Func :",""+position);
		  
	      Fragment fragment = null;
	      mDrawerLayout.closeDrawer(dList);

	      if(type.equals("Teacher"))
	      {
	    	  switch(position)
	    	  {
	    	  case 3:
	    		  fragment=new Edit_account_fragment();
	    		  break;
	    	  
	    	  case 4:
	    		  fragment=new Contact_us_Fragment();
	    		  break;
	    	  case 5:     
	    		
	    		  if (Network_checking.isNetworkAvailable(SettingsScreenActivity.this))
	  			{	
	  				Logout_Common.setLogout(SettingsScreenActivity.this);
	  			}
	  			else
	  			{
	  				showToast("Check Internet Connection");		
	  			}
	  			break; 		  
	    		  
	        	  //fragment = new Notifications_Fragment();
	             
	    	  default:
	        	  fragment=new Head_setting_screen_fragment();
	              break;
	    	  }
	      }
	      if(type.equals("Parent"))
	      {
	    	  switch(position)
	    	  {
	    	  
	    	  case 3:
	    		  fragment=new Edit_account_fragment();
	    		  break;
	    	  case 4:
	    		  fragment=new Addchild_fragment();
	    		  break;	    		  
	    	  case 5:
	    		  fragment=new Contact_us_Fragment();
	    		  break;
	    		  
	    	  case 6:
	    		  if (Network_checking.isNetworkAvailable(SettingsScreenActivity.this))
		  			{	
		  				Logout_Common.setLogout(SettingsScreenActivity.this);
		  			}
		  			else
		  			{
		  				showToast("Check Internet Connection");		
		  			}
		  			break;
	    	  
	    	  default:
	        	  fragment=new Head_setting_screen_fragment();
	              break;	    	  
	    	  }
	      }	    
	      if (fragment != null) 
	      {
	          FragmentManager fragmentManager = getFragmentManager();
	          fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();
	          Log.e("Entered","Entered");
	          dList.setItemChecked(position, true);
	          dList.setSelection(position);
	          Log.e("Entered","Entered");	       
	          
	          if(position==0||position==1||position==2)
	          {
	        	  setTitle("Settings Screen");
	          }	          
	          else
	          {
	        	  setTitle(menu[position]);  
	          }
		     
	      } else {
	          Log.e("MainActivity", "Error in creating fragment");
	      }
	      
	  }
	
	 public String LoadGalleryname()
	 {	   
		 try
		  {
			// String header_name_id="",header_code_id="",get_header_name="",get_header_code="";
			 String header_name = null,header_class = null,header_sec = null,header_no=null;
			 String get_name = null,get_class = null,get_sec = null,get_no=null;
			 
			 if(type.equals("Teacher"))
			   {
				 httppost = new HttpPost("http://"+LoginActivity.URL+"/schoolmanagement/designs/webservice/setting2.php");
				   header_name="TEACHER CLASS";				   	
				   header_sec="TEACHER SECTION";				   
				   get_class="CLASS";
				   get_sec="SECTION";			   
				   try
					{		
					httpclient = new DefaultHttpClient();				
					nameValuePairs= new ArrayList<NameValuePair>();					
					nameValuePairs.add(new BasicNameValuePair("type",type));
					nameValuePairs.add(new BasicNameValuePair("phno",phno));
					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
					HttpResponse response = httpclient.execute(httppost); 
					HttpEntity entity = response.getEntity();					
					is = entity.getContent();
					
						    BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
						    StringBuilder sb = new StringBuilder();				    
						    while ((line = reader.readLine()) != null)
				            {
				                sb.append(line + "\n");
				            }
				            is.close();
				            result = sb.toString();
				            
						JSONObject json_data = new JSONObject(result);					    	     
				        JSONObject data = json_data.getJSONObject("DATA");
				        code = data.getString("CODE");
				        message =data.getString("MESSAGE");
				        
				        Log.e("code",code);
				        
				        if(code.equals("200"))
				        {
				        					        	
				        	 JSONArray classes = (JSONArray)data.getJSONArray("TEACHER CLASS");
				        	 JSONArray section = (JSONArray)data.getJSONArray("TEACHER SECTION");
				        	 
				        	 dynamic_header_name=new String[classes.length()];
				        	 dynamic_header_sec=new String[section.length()];
				        	
				        	 int i=0;
				        	 for(i=0;i<classes.length();i++)
				       	  {
				       		  		        		  
				        		 dynamic_header_name[i]=classes.getString(i).toString();
				        		 dynamic_header_sec[i]=section.getString(i).toString();
				       	  }
				        	
				       
				      shared_first_name=getSharedPreferences("shared_first_name",Context.MODE_PRIVATE);
				       Editor editor = shared_first_name.edit();
				       editor.clear();
				       editor.putString("firsname",dynamic_header_name[0]);				       
				       editor.putString("firstsec",dynamic_header_sec[0]);
				       editor.commit();             
				       }
				        if(code.equals("500"))
				        {				        	
				        	/*Intent i=new Intent(getApplicationContext(),LoginActivity.class);          	
							startActivity(i);	
							overridePendingTransition(R.anim.right_in, R.anim.left_out);*/				        	
				        }
				        
			       }
					catch(Exception e)
					{
						Log.e("Exception",e+"");
					}
			   }
			 
			   if(type.equals("Parent"))
			   {
				   httppost = new HttpPost("http://"+LoginActivity.URL+"/schoolmanagement/designs/webservice/setting1.php");
				   header_name="STUDENT NAME";
				   header_class="STUDENT CLASS";	
				   header_sec="STUDENT SECTION";
				   header_no="STUDENT NO";				   
				   get_name="NAME";
				   get_class="CLASS";
				   get_sec="SECTION";
				   get_no="NO";
				   
				   try
					{		
					httpclient = new DefaultHttpClient();				
					nameValuePairs= new ArrayList<NameValuePair>();
					nameValuePairs.add(new BasicNameValuePair("uid",uid));
					nameValuePairs.add(new BasicNameValuePair("type",type));
					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
					HttpResponse response = httpclient.execute(httppost); 
					HttpEntity entity = response.getEntity();					
					is = entity.getContent();
					
						    BufferedReader reader = new BufferedReader(new InputStreamReader(is,"iso-8859-1"),8);
						    StringBuilder sb = new StringBuilder();				    
						    while ((line = reader.readLine()) != null)
				            {
				                sb.append(line + "\n");
				            }
				            is.close();
				            result = sb.toString();
				    	//generic
						JSONObject json_data = new JSONObject(result);
					    Log.e("Json Data", json_data+"");	     
				        JSONObject data = json_data.getJSONObject("DATA"); // get data object
				        code = data.getString("CODE"); // get the name from data.
				        message =data.getString("MESSAGE");
				        
				        if(code.equals("200"))
				        {
				        	 JSONArray stud_no = (JSONArray)data.getJSONArray("STUDENT NO");
				        	 JSONArray stud_name = (JSONArray)data.getJSONArray("STUDENT NAME");
				        	 JSONArray stud_class = (JSONArray)data.getJSONArray("STUDENT CLASS");
				        	 JSONArray stud_section = (JSONArray)data.getJSONArray("STUDENT SECTION");
				           	 dynamic_header_name=new String[stud_name.length()];				        
						     dynamic_header_class=new String[stud_class.length()];
						     dynamic_header_sec=new String[stud_section.length()];
						     dynamic_header_no=new String[stud_no.length()];
				        	 
						     int i=0;
				        	 for(i=0;i<stud_no.length();i++)
				       	  	{
				        		 dynamic_header_name[i]=stud_name.getString(i).toString();
				        		 dynamic_header_no[i]=stud_no.getString(i).toString();
				        		 dynamic_header_class[i]=stud_class.getString(i).toString();
				        		 dynamic_header_sec[i]=stud_section.getString(i).toString();
				        	}
				        	   shared_first_name=getSharedPreferences("shared_first_name",Context.MODE_PRIVATE);
						       Editor editor = shared_first_name.edit();
						       editor.clear();
						       editor.putString("firsname",dynamic_header_name[0]);		
						       editor.putString("firstcode",dynamic_header_class[0]);
						       editor.putString("firstsec",dynamic_header_sec[0]);
						       editor.putString("firstno",dynamic_header_no[0]);
						       editor.commit();     	        
				       }
				       
				        if(code.equals("500"))
				        {
				        	/*Intent i=new Intent(getApplicationContext(),LoginActivity.class);          	
							startActivity(i);	
							overridePendingTransition(R.anim.right_in, R.anim.left_out);*/	
				        }
			       }
					catch(Exception e)
					{
						Log.e("Exception",e+"");
					}
			   }				
				
		  }
		  catch(Exception e)
		  {
				Log.e("Exception",e+"");
		  }
		 return code;
	}
	 
	 private void showToast(String msg)
		{
			Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
		}	
	 
	 @Override
	 public void setTitle(CharSequence title) {
	     mTitle = title;
	     getActionBar().setTitle(mTitle);
	 }	 
}